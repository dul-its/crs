requires "Dancer2" => "1.1.2";
requires "Dancer2::Plugin::Ajax";
requires "Dancer2::Plugin::Database";
requires "Dancer2::Plugin::Deferred";
requires "Dancer2::Plugin::DBIC";
requires "Dancer2::Plugin::Redis";
requires "Dancer2::Serializer::JSON";
requires "Dancer2::Session::Memcached";
requires "DateTime";
requires "DateTime::Set";
requires "ExtUtils::Embed";
requires "Fatal";
requires "Module::Build::Tiny";
requires "Number::Format";
requires "Plack";
requires "Plack::Middleware::CrossOrigin";
requires "Plack::Test";
requires "Redis";
requires "Set::Infinite";
requires "TAP::Harness::Env";
requires "Task::Plack";
requires "XML::LibXML";
requires "XML::SAX";
requires "XML::Simple";
requires "XML::XML2JSON";
requires "JSON";
requires "Test::JSON";
requires "Net::Amazon::DynamoDB";


recommends "YAML"             => "0";
recommends "URL::Encode::XS"  => "0";
recommends "CGI::Deurl::XS"   => "0";
recommends "HTTP::Parser::XS" => "0";

on "test" => sub {
    requires "Test::More"            => "0";
    requires "HTTP::Request::Common" => "0";
};
