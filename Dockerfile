FROM image-mirror-prod-registry.cloud.duke.edu/library/perl:latest

RUN apt-get update && \
  apt-get -y install curl libcurl4 vim

ENV APP_HOME=/srv/web/crs

#RUN mkdir $APP_HOME && \
#	pwd && \
#	ls 

WORKDIR $APP_HOME
ADD ./cpanfile .
RUN cpanm -f -v --notest --no-interactive --installdeps .

COPY ./ $APP_HOME 

#RUN cp ./stunnel.conf /etc/stunnel/stunnel.conf && \
#	mkdir /etc/stunnel/ssl && \
#	cp ./environments/ssl/* /etc/stunnel/ssl/

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY entrypoint.sh /usr/local/bin/execpod.sh

RUN chmod a+rw /srv/web/crs

RUN touch crs.pid
RUN chmod a+rw crs.pid

EXPOSE ${CRS_PORT:-4000}

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["start"]
#CMD ["/usr/local/bin/starman","--daemonize","-l","0.0.0.0:4000","-E","production","--workers","5","--pid","/var/run/crs.pid","-I","/srv/web/crs","bin/app.psgi"]
