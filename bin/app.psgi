#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Plack::Builder;

use CRS;
use CRS::api;

builder {
	enable 'CrossOrigin', origins => '*';
	mount '/' => CRS->to_app;
	mount '/api' => CRS::api->to_app;
};
