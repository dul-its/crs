package CRS;

use strict;
use warnings;
use Carp;
use Data::Dumper;
# use MIME::Base64;

use Dancer2;
use Dancer2::Plugin::DUL::Alma;
use Dancer2::Plugin::Redis;
use Dancer2::Plugin::Ajax;
use CRS;
use CRS::request;
use CRS::myaccount;

#use CRS::api;

#use WWW::Curl::Easy;
use URI::Escape;

# use CRS::account

no warnings 'uninitialized';

our $VERSION = '0.1';

# ARE WE STILL USING THIS, OR "NO"?
# Let's review soon...
my $redis = undef;
sub redis_on_connect {

	print STDERR "CRS (STDERR): connected to Redis database!", "\n";
	print STDOUT "CRS (STDOUT): connected to Redis database!", "\n";
}

set layout => 'main';

hook before => sub {
	# consume shibboleth authentication data
	debug sprintf "hook before: request_path = [%s]", request->path;

	my $app = shift;
	if (request->path =~ /^logout/) {
		$app->destroy_session;
		debug "session destroyed";
	}

	# when available, capture the userid from either "uid" or "HTTP_UID",
	# located in the environment vars
	# debug Dumper(request->env);
	var captured_netid => request->env->{'uid'} || request->env->{'HTTP_UID'} || request->env->{'HTTP_DUDUKEID'} || undef;
	var captured_dukeid => request->env->{'HTTP_DUDUKEID'} || undef; # test id: 1000283566

	# debug Dumper request->env;

	my $c = config->{ui};
	# debug Dumper($c);

	# initialize alert messaging
	var alert_messages => [];

	# 'user' session variable could have been set in POST /authenticate route action
	#debug "hook:before";
	#debug Dumper session('user');
	#	error Dumper( keys %{request->env} );

	if ( !session('user') ) {
		# if uid is present, store it in a session
		my $netid = vars->{'captured_netid'};
		my $dukeid = vars->{'captured_dukeid'};
		debug "hook:before == netid: " . $netid . " dukeid: " . $dukeid;
		if ($netid eq '(null)') {
			if ($dukeid ne '(null)') {
				$netid = $dukeid;
			} else {
				# $netid = undef;
				# bad shib workaround 7/23/2024
				my $eppn = request->env->{'HTTP_EPPN'};
				# debug "hook:before == bad shib workaround eppn: " . $eppn;
				if ($eppn  ne '(null)') {
					$eppn =~ s/\@duke\.edu//;
					$netid = $eppn;
				} else {
					$netid = undef;
				}
			}
		}
		debug "hook:before == user id: " . $netid;
		if (defined $netid) {
			session user => $netid;
			debug "hook:before == user session variable set to " . $netid . "!";
			
		} else {
			session user => undef;
		}
	}

};

hook after => sub {
	session alert_message => undef;
	session error_message => undef;
};

hook before_template_render => sub {
	my $tokens = shift;
	if (!defined $tokens->{item_title}) {
		$tokens->{item_title} = 'Catalog Title Here';
	}

	if (vars->{page_title}) {
		$tokens->{page_title} = vars->{page_title} unless defined $tokens->{page_title};
		if (vars->{page_title_class}) {
			$tokens->{page_title_class} = vars->{page_title_class};
		}
	}
	$tokens->{show_search_bar} = 0 unless defined($tokens->{show_search_bar});
	$tokens->{alert_messages} = vars->{alert_messages};
	
	if (session('recent_searches')) {
		my $recents = session('recent_searches');
	#	debug Dumper ($recents);
		if ($recents) {
			$tokens->{recent_searches} = $recents;
		}
	}

	# debug Dumper ($tokens);
	# debug Dumper (session('alert_message'));
};

# this resets the path routing since 'request' defines its own prefix
prefix undef;

# default 'home' route
# Scenario #1:
# - if previously authenticated, go to My Accounts
#
# OTHERWISE...
# Display search (or discovery) form with some light directions (or help)
get '/' => sub {
	session showlastBtn => 1;
	#my $g = redis_get('hi');
	if (session('user')) {
		forward '/account';
	}
	template 'index', { show_search_bar => 0 };
};

get '/userid' => sub {
	if (session('user')) {
		template 'userid', { userid => session('user') };
	} else {
		redirect '/authenticate';
	}
};

get '/logout' => sub {
	# now make sure to destroy the Shibboleth session
	# redirect if a user has authenticated with SSO
	if (session('authShib')) {
		app->destroy_session;
		debug "session destroyed and SSO logout";
		redirect '/Shibboleth.sso/Logout?return=https://shib.oit.duke.edu/cgi-bin/logout.pl'
	} else {
		app->destroy_session;
		debug "session destroyed";
		redirect 'https://library.duke.edu'
	}

	# the below file uses a simulated click
#	send_file '/logout.htm';
};

get '/login' => sub {
	redirect '/authenticate';
};

# login step  shib for netid
# this route is present only as a stub.
# ideally, the web server's configuration will handle this route
get '/authenticate' => sub {
	my $query = query_parameters;
	my $req = request;
	
	# redirect if a user has authenticated with SSO
	# debug Dumper $req->env;
	#
	# the netid, when present in the headers, is kept here:
	# vars->{captured_netid}
	my $netid = vars->{'captured_netid'};
	my $dukeid = vars->{'captured_dukeid'};
	debug "get /authenticate netid: " . $netid . " dukeid: " . $dukeid;
	if ($netid eq '(null)') {
		if ($dukeid ne '(null)') {
			$netid = $dukeid;
		} else {
			# $netid = undef;
			# bad shib workaround 7/23/2024
			my $eppn = request->env->{'HTTP_EPPN'};
			# debug "hook:before == bad shib workaround eppn: " . $eppn;
			if ($eppn  ne '(null)') {
				$eppn =~ s/\@duke\.edu//;
				$netid = $eppn;
			} else {
				$netid = undef;
			}
		}
	}
	if (defined $netid) {
		# my $netid = vars->{captured_netid};
		session user => $netid;
		debug "hook:before == user session variable set to " . $netid . "!";

		session authShib => 1;

		session displayName => $req->env->{displayName} || $req->env->{HTTP_DISPLAYNAME};
		session recent_searches => [];

		debug "get /authenticate - got user id = " . $netid . " session user: " . session('user');
		# debug "get /authenticate - got user id = " . vars->{captured_netid} . " session user: " . session('user');
		my $sysid = $query->get('sysid');
		debug "sysid q: " . $sysid . " s: " . session('lastSysid');
		# AK-393
		# if (!defined $sysid) {
		# 	$sysid = session('lastSysid');
        # }
		# if the 'action' query value is 'confirm', then
		# redirect to the /confime route
		if ($query->get('action') eq 'confirm_request') {
			my %data = get_item_info( $sysid, session('user') );
			my $barcode = $query->get('barcode');
			my $item_title = $data{bibInfo}->{title} . $data{bibInfo}->{subTitle};
			my $reckey = $data{$barcode}{recKey};
			my $userHomeLib = $data{userInfo}{homeLib};

			my $uri = sprintf("/request/confirm/%s?reckey=%s&pid=%s&title=%s&userHomeLib=%s", $sysid, $reckey, $data{userInfo}{borId}, $item_title, $userHomeLib);
			redirect $uri;
		}
		
		redirect '/item/' . $sysid if $sysid;

		# otherwise, redirect to the account page
		redirect '/account';
	}

	# my $env = request->env;
	#error Dumper request->env;
	#error Dumper request->headers;

	template 'authenticate', { q => $query };
};

post '/authenticate' => sub {
	# put value of 'auth_netid' in the session ('user')..
	# get the value of 'sysid' from the form...
	# ignore the password
	# redirect to /request/:sysid

	my $user = body_parameters->get('auth_netid');
	debug "post /authenticate - got user id = " . $user . " session user: " . session('user');
	# TODO add check for absence of this value

	# my $data = bor_key($user);
	# session user => $data->{bor_id};

	redirect '/item/' . body_parameters->get('sysid');
};

get '/impersonate/:userid' => sub {
	my $userid = route_parameters->get('userid');
	my $valid_impersonators = config->{users}->{impersonators};
	my $session_user = session('user');

	debug "userid: $userid";
	debug Dumper $valid_impersonators;
	if ( grep( $userid, @$valid_impersonators ) ) {
		debug "get '/impersonate [valid_impersonators] session variable set to impersonate " . $userid . "!";
		session impersonateuser => $userid;
	}
	# for user testing
	#if (session('user') =~ /jaf41|en25|khn717|dlc32|seanaery|md169|ceg16/) {
	#	session impersonateuser => $userid;
	#	debug "get '/impersonate session variable set to impersonate" . $userid . "!";
	#}
	redirect '/account'
};

# search route
# -
# Process the 'q' query parameter, parsing out the system id
# or producing a 404 response when we can't determine the system id
post '/search' => sub {
	my $sysid = body_parameters->get('q');

	my $recents = [];
	if (session('recent_searches')) {
		$recents = session('recent_searches');
	}
	push @{$recents}, $sysid;
	session recent_searches => $recents;

	# anticipate a full catalog search URL (because Derrek is lazy)
	$sysid =~ s/^(http(s)?:\/\/)?search\.library\.duke\.edu(:[\d]{2,})?\/search\?id=//;
	$sysid =~ s/^(http(s)?:\/\/)?library\.duke\.edu(:[\d]{2,})?\/catalog\/search\/recordid\///;
	$sysid =~ s/^(http(s)?:\/\/)?find-dev\.library\.duke\.edu(:[\d]{2,})?\/catalog\///;
	$sysid =~ s/duke//gi;
	$sysid = sprintf('%09s', $sysid);

	# hand off to the 'request' route
	redirect '/item/' . $sysid;
};

#get '/circstatus/:sysid' => sub {
#	my $sysid = route_parameters->get('sysid');
#	my $circstatus = circstatus($sysid);
#	return encode_json( $circstatus );
#};

get '/circstatusitm/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');
	my $c = circstatusitm($sysid);
	return encode_json( $c );
};

get '/circstatushold/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');
	return encode_json( circstatushold($sysid) );
};

get '/item/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');

	session lastSysid => $sysid;
	session showlastBtn => 0;
	my $userid = session('user');
	if (session('impersonateuser')) {
	    $userid = session('impersonateuser');
	}
	my %data = get_item_info( $sysid, $userid );
	# debug %data;

	# userState Y=> has id, D=> no id, but has SCL/ARCH, N=> no id, but should, E=> has error
	# if ($data{userInfo}{userState} eq 'N' && !session('user')) {
	# 	# redirected to a route that forces authentication (shibboleth or otherwise)
	# 	redirect '/authenticate?sysid=' . $sysid;
	# }

	# the system needs to get the bor(rower)_id and store it in the session

	# my @holdings = ();
	my $userInfo;
	my $bibInfo;
	my $libraries;
	my $item_title = "";
	my $part_of_mmsid = "";
	my $relatedRecsJson = "";


	foreach my $lv1 (sort keys %data) {
		if ($lv1 eq 'bibInfo') {
			$bibInfo = $data{$lv1};
			$item_title = $bibInfo->{title};
			$part_of_mmsid = $bibInfo->{part_of_mmsid};
			$relatedRecsJson = $bibInfo->{relatedRecs};
			next;
		}
		if ($lv1 eq 'userInfo') {
			$userInfo = $data{$lv1};
			next;
		}
		if ($lv1 eq 'libraries') {
			$libraries = $data{$lv1};
			next;
		}
		if ($lv1 eq 'testing') {
			next;
		}
	}
	# debug "part_of_mmsid " . Dumper($part_of_mmsid);
	my @part_of_mmsid_arr = @{decode_json($part_of_mmsid)};
	my $partTitle = "";
	my @part_of = ();
	my $partIndex = 1;
	foreach (@part_of_mmsid_arr) {
		my %part_of_hash = ();
		my $part_of_id = $_;
		my $urlLDPart = sprintf("https://open-na.hosted.exlibrisgroup.com/alma/01DUKE_INST/bibs/%s", $part_of_id);
		# debug 'bib part json-ld: ' . $urlLDPart;
		my $alma_response_part = `curl -sS -X GET "${urlLDPart}"`;
		my $bibPartLinkedDataRef = decode_json $alma_response_part;
		# debug "bibPartLinkedData " . Dumper($bibPartLinkedDataRef);
		my %bibPartLinkedData = %$bibPartLinkedDataRef;
		$part_of_hash{mmsid} = $part_of_id;
		$part_of_hash{title} = $bibPartLinkedData{title};
		$part_of_hash{index} = $partIndex;
		push @part_of, \%part_of_hash;
		$partIndex++;
	}
	# related records
	debug 'relatedRecsJson: ' . $relatedRecsJson;
	my @relatedRecs = decode_json($relatedRecsJson);

	my $docType = 'monograph';
	if ($bibInfo->{bibLevel} eq 'c') {
		$docType = 'manuscript';
	}
	# my $aeon = sprintf("rft.description=%s&rft.title=%s&oclc=%s&rtf.barcode=%s&rft.volume=%s&rft.callnum=%s&rft.opac=%s",
	#	$data{bibInfo}{description}, $item_title, $data{bibInfo}{oclc}, $data{barcode}, $data{$lv1}{description}, $data{$lv1}{callNumber}, $data{$lv1}{opacNote});

	my $aeonQueryBib = sprintf("rfe_dat=%s&genre=%s&rft.title=%s&rft.au=%s&rft.description=%s&rft.oclc=%s&rft.access=%s&rft.date=%s&rft.pub=%s"
		, 'aleph:' . $sysid
		, $docType
		, uri_escape_utf8($data{bibInfo}{title})
		, uri_escape_utf8($data{bibInfo}{author})
		, uri_escape_utf8($data{bibInfo}{description})
		, uri_escape_utf8($data{bibInfo}{oclc})
		, uri_escape_utf8($data{bibInfo}{roAccess})
		, uri_escape_utf8($data{bibInfo}{pubDate})
		, uri_escape_utf8($data{bibInfo}{imprint})
	);

	#
	my $illiadSrv = "https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDD/illiad.dll";

	if ($userInfo->{ilr} eq 'MCL') {
		$illiadSrv = "https://illiad.mclibrary.duke.edu/illiad.dll";
	} elsif ($userInfo->{ilr} eq 'FORD') {
		$illiadSrv = "https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDB/illiad.dll";
	} elsif ($userInfo->{ilr} eq 'LAW') {
		$illiadSrv = "https://duke-illiad-oclc-org.proxy.lib.duke.edu/illiad/NDL/illiad.dll";
	}

	my $isns = "";
	# foreach my $isn (@{$data{bibInfo}{isn}}) {
	# 	$isns .= $isn . ',';
	# }
	# $isns =~ s/,+$//;
	$isns = @{$data{bibInfo}{isn}}[0];

	my $illiadLinkBib  = sprintf("%s?Action=10&form=20&Value=GenericRequestTRLNLoan&CitedPages=CRS&ESPNumber=%s&ISSN=%s&IlliadForm=Logon&LoanTitle=%s&LoanAuthor=%s&LoanPublisher=%s&Location=%s"
		, $illiadSrv
		, uri_escape_utf8($data{bibInfo}{oclc})
		, uri_escape_utf8($isns)
		, uri_escape_utf8($data{bibInfo}{title})
		, uri_escape_utf8($data{bibInfo}{author})
		, uri_escape_utf8($data{bibInfo}{imprint})
		, uri_escape_utf8("https://requests.library.duke.edu/item/" . $sysid)
	);

	my $illiadScanUrl  = sprintf("%s?Action=10&form=23&Value=GenericRequestTRLNLoan&CitedPages=CRS&PhotoJournalTitle=%s&ESPNumber=%s&ISSN=%s&IlliadForm=Logon&LoanTitle=%s&LoanAuthor=%s&LoanPublisher=%s&Location=%s"
		, $illiadSrv
		, uri_escape_utf8($item_title)
		, uri_escape_utf8($data{bibInfo}{oclc})
		, uri_escape_utf8($isns)
		, uri_escape_utf8($data{bibInfo}{title})
		, uri_escape_utf8($data{bibInfo}{author})
		, uri_escape_utf8($data{bibInfo}{imprint})
		, uri_escape_utf8("https://requests.library.duke.edu/item/" . $sysid)
	);
	
	# for APIs down
	my $apiDownMsg = "";
	my $illiadLinkApiDown  = "";
    # for APIs down
	if ($userInfo->{userState} eq 'E' && $bibInfo->{error}) {
		$apiDownMsg = "Requesting Items Is Unavailable";
		$illiadLinkApiDown  = sprintf("%s?Action=10&form=20&Value=GenericRequestTRLNLoan&CitedPages=CRS&IlliadForm=Logon&Notes=%s"
		, $illiadSrv
		, uri_escape_utf8("CRS Request for: https://requests.library.duke.edu/item/" . $sysid . " while Alma was not available.")
		);
	}
	if (config->{maintenance_mode} && $bibInfo->{error}) {
		$apiDownMsg = "Requesting Items Is Unavailable";
		$illiadLinkApiDown  = sprintf("%s?Action=10&form=20&Value=GenericRequestTRLNLoan&CitedPages=CRS&IlliadForm=Logon&Notes=%s"
		, $illiadSrv
		, uri_escape_utf8("CRS Request for: https://requests.library.duke.edu/item/" . $sysid . " while Alma was not available.")
		);
	}

	my $findUrl = sprintf("https://find.library.duke.edu/catalog/DUKE%s", $sysid);

	my $totalItems = $bibInfo->{totalItems};
	my $holdingsJson = $bibInfo->{holdings};
	my $holdingsRef = decode_json $holdingsJson;
	# debug "Holdings: " . Dumper($holdingsRef);
	my %holdings = %$holdingsRef;
	my $collCount = 0;
	foreach my $libCode (sort keys %holdings) {
		# debug "libCode: " . $libCode . " libName: " . $holdings{$libCode}{libName} . " collName: " . $holdings{$libCode}{collName} . 
		# " count: " . $holdings{$libCode}{count} . " holdings: ". Dumper($holdings{$libCode}{holdings});
		$collCount++
	}
	# debug "Total Items: " . $totalItems . " collCount: " . $collCount;

	my $forDetail = {
		sysid => $sysid,
		userid => $userid,
		multiVol => $bibInfo->{multiVol},
		borStatus => $userInfo->{borStatusCode},
		userState => $userInfo->{userState},
		homeLib => $userInfo->{homeLib},
		aeonQueryBib => $aeonQueryBib,
		illiadLinkBib => $illiadLinkBib,
		illiadScanUrl => $illiadScanUrl,
		isStaff => $userInfo->{isStaff},
		# title => uri_escape_utf8($data{bibInfo}{title}),
		# mmsid => $data{bibInfo}{mmsid},
		# part_of_mmsid => $data{bibInfo}{part_of_mmsid},
		ilr => $userInfo->{ilr},
		digitization => $userInfo->{digitization},
		paging => $userInfo->{paging}
	};
	
	my $cItmCnt = 0;
	#debug Dumper session;
	debug "Libraries " . $libraries;
	template 'item', {
		bibInfo => $bibInfo,
		item_title => $item_title,
		userInfo => $userInfo,
		illiadScanUrl => $illiadScanUrl,
		forDetail => encode_json( $forDetail ),
		findUrl => $findUrl,
		partOf => \@part_of,
		apiDownMsg => $apiDownMsg,
		illiadLinkApiDown => $illiadLinkApiDown,
		cItmCnt => $cItmCnt,
		holdings => \%holdings,
		relatedRecs => @relatedRecs,
		libraries => $libraries,
	};
};

post '/item-detail' => sub {
	my $parms = body_parameters;
	my $sysid = $parms->{sysid};
	my $userid = $parms->{userid};
	my $multiVol = $parms->{multiVol};
	my $borStatus = $parms->{borStatus};
	my $userState = $parms->{userState};
	my $homeLib = $parms->{homeLib};
	my $aeonQueryBib = $parms->{aeonQueryBib};
	my $illiadLinkBib = $parms->{illiadLinkBib};
	my $illiadScanUrl = $parms->{illiadScanUrl};
	my $isStaff = $parms->{isStaff};
	my $uriTitle = $parms->{title};
	my $mmsid = $parms->{mmsid};
	my $ilr = $parms->{ilr};
	my $digitization = $parms->{digitization};
	my $paging = $parms->{paging};
	my $cItmCnt = $parms->{cItmCnt};
	debug "POST item-detail sysid: " . $sysid . " userid: " . $userid . " MMSID: " . $mmsid  . " cItmCnt: " . $cItmCnt;
	# debug "item-detail ilr: " . $ilr . " digitization: " . $digitization;
	my %data = get_item_detail($sysid, $mmsid, $userid, $multiVol, $borStatus, $userState, $homeLib, $aeonQueryBib, $illiadLinkBib, $digitization, $paging, $cItmCnt);

	my @holdings = ();
	my $recInfo;
	my $multi_vol = "";
	my $avail_count = 0;
	my $unavail_count = 0;
	my $on_order_count = 0;
	my $show_ill = 0;
	# my $show_recall = 1;
	my $show_lsc_guest = 1;
	my $show_single_scan = 0;

#	debug "CRS ILLIAD URL " . $illiadLinkBib ;

	foreach my $lv1 (sort keys %data) {
		if ($lv1 eq 'recInfo') {
			$recInfo = $data{$lv1};
			$multi_vol = $recInfo->{multiVol};
			$userState = $recInfo->{userState};
			next;
		}
		if ($lv1 eq 'testing') {
			next;
		}
		# $data{$lv1}{sysid} = $sysid;
		# $data{barcode} = $lv1;
		#	$data{doc_number} = $sysid;
		if ($data{$lv1}{requestType} =~ m/none|alma-recall/) {
			$unavail_count++;
		} elsif ($data{$lv1}{requestType} =~ m/map/) {
			if ($data{$lv1}{tmpItemStatus} !~ m/02|03|07|08|09|10|11|12|13|14|15/) {
				$avail_count++;
			} else {
				$unavail_count++;
			}
		} elsif ($data{$lv1}{requestType} =~ m/aeon/) {
			$data{$lv1}{aeon_query} = sprintf("%s&rft.site=%s&rft.barcode=%s&rft.volume=%s&rft.callnum=%s&rft.collcode=%s"
				, $aeonQueryBib
				, $data{$lv1}{subLibCode}
				, uri_escape_utf8($data{$lv1}{barcode})
				, uri_escape_utf8($data{$lv1}{description})
				, uri_escape_utf8($data{$lv1}{callNumber})
				, $data{$lv1}{collCode}

			);
		# 	$avail_count++; should SCL be counted? what if it is only SCL
			# $unavail_count++;
		} else {
			$avail_count++;
		}

		if ($data{$lv1}{digitization} eq 'Y') {
			$data{$lv1}{scanUrl} = sprintf("%s&PhotoItemEdition=%s&Notes=Barcode%%3A%%20%s"
				, $illiadScanUrl
				, uri_escape_utf8($data{$lv1}{description})
				, uri_escape_utf8($data{$lv1}{barcode})
			);
		}

		if ($data{$lv1}{availStatus} =~ m/On Order/) {
			$on_order_count++;
		}

		my $subLibName = $data{$lv1}{subLibName};
		if ($subLibName =~ m/\'/) {
			$subLibName =~ s/\'/&#39;/g;
			# debug "subLibName " . $subLibName;
			$data{$lv1}{subLibName} = $subLibName;
		}

		push @holdings, $data{$lv1};
	}

	# indicator to present library guests with the option
	# to request item from LSC via Qualtrics form (from main library website)
	$show_lsc_guest = $data{recInfo}{itmCntLSC};

	if ($userState eq 'N') {
		debug "post /item-detail - userState: " . $userState . " session user: " . session('user');
		# template 'login1', {
		# 	sysid => $sysid,
		# 	show_lsc_guest => $show_lsc_guest
		# }, { layout => undef };
	} else {
		debug "avail_count: " . $avail_count . " on_order_count: " . $on_order_count . " unavail_count: " . $unavail_count;
		if ($avail_count eq 0 &&
			$multi_vol ne 'Y' &&
			$data{recInfo}{blocked} eq 'N')
		{
			my $ilrCnt = $unavail_count - $on_order_count;
			debug "ilrCnt: " . $ilrCnt;
			if ($ilr && $ilrCnt > 0) {
				$show_ill = 1;
			} 
		}
		if (config->{maintenance_mode}) {
			if ($ilr) {
				$show_ill = 1;
			} 
		}

		my %retData = ();
		$retData{holdings} = \@holdings;
		$retData{recInfo} = $recInfo;
		$retData{sysid} = $sysid;
		$retData{userid} = $userid;
		$retData{show_ill} = $show_ill;
		$retData{show_lsc_guest} = $show_lsc_guest;
		$retData{multi_vol} = $multi_vol;
		# $retData{show_recall} = $show_recall;
		$retData{homeLib} = $homeLib;
		$retData{isStaff} = $isStaff;
		$retData{illiadScanUrl} = $illiadScanUrl;

		# debug "retData: " . encode_json(\%retData);
		return encode_json(\%retData);
	}

};

get '/search' => sub {
	template 'index', { show_search_bar => 0 };
};

get '/test-request' => sub {
	template 'testreq', {};
};

ajax ['get'] =>  '/verify-api' => sub {
	my $curl_test = `which curl`;
	chomp $curl_test;
	if(!$curl_test) {
		return encode_json { 'reply-text' => 'cURL is not available on the host server', 'reply-code' => 9999 };
	}

	my $rest_api_url = config->{aleph_resturi};
	my $response = `curl --connect-timeout 2 -i ${rest_api_url}`;
	if ($response =~ /Connection timed out/) {
		return encode_json { 'reply-text' => 'Unable to reach Aleph API server', 'reply-code' => 9998 };
	}

	encode_json { 'reply-text' => 'OK' };

	#my $curl = WWW::Curl::Easy->new;

	#$curl->setopt(CURLOPT_HEADER, 1);
	#$curl->setopt(CURLOPT_URL, $rest_api_url);
	#my $response_body;
	#$curl->setopt(CURLOPT_WRITEDATA, \$response_body);

	#my $retcode = $curl->perform;
	#debug sprintf "retcode = [%s]", $retcode;

	#if ($retcode == 0) {
	#	return encode_json( { 'reply-text' => 'OK' } );
	#}
	#encode_json( { 'reply-text' => $curl->errbuf, 'reply-code' => $curl->strerror($retcode) } );
};

get '/updateholdshelfs' => sub {
	# for updating hold shelfs for pickup or delivery location
	if (session('user') =~ /jaf41|en25/) {
		debug "get '/updateholdshelfs";
		my @data = get_hold_shelfs();
	}
};

true;
