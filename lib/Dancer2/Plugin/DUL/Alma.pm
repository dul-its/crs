package Dancer2::Plugin::DUL::Alma;

# ABSTRACT: model used to interact with Alma APIs
#

use strict;
use warnings;
use Carp;
use DateTime;
use Moo;
use Dancer2::Plugin;
use Dancer2::Plugin::DUL::DscLogging;
use Data::Dumper;
use File::Basename;	# to get name of working directory
use Number::Format qw(:subs);
use XML::LibXML;
use XML::LibXML::XPathContext;
use XML::Simple;
use XML::XML2JSON;
use JSON;
use Test::JSON;
use Encode 'encode';
# use MIME::Base64;
# require LWP::UserAgent;
# use Redis;
# use Dancer2::FileUtils 'read_file_content';

no warnings 'uninitialized';

plugin_keywords qw/bor_info bor_fines bor_loans bor_holds renew_item cancel_hold_request get_item_info get_item_detail make_hold_request get_hold_shelfs circstatusitm circstatushold/;



register 'bor_info' => sub {
	my ($dsl, $bor_id) = @_;
	my $config = $dsl->app->config;
    my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};
    # $dsl->debug( "*** bor_info function ***" );
    # $dsl->debug( Dumper( [$api_uri, $api_key] ));
	my $maintenance_mode = $config->{maintenance_mode};

	# init var
	my $return = {};
	my $bor_info = {};

    my $urlUser = $api_uri . sprintf("users/%s?user_id_type=all_unique&view=full&expand=loans,requests,fees", $bor_id);
    # $dsl->debug('bor_info url: ' . $urlUser);
    my $alma_response = `curl -sS --write-out "HTTPSTATUS:%{http_code}" -X GET "${urlUser}" -H "Authorization: apikey ${api_key}"`;
	my $httpStatus = 0;
	if ($alma_response =~ s/HTTPSTATUS:(?<status>\d{3})$//) {
	    $httpStatus = $+{status};
	}
	if ($httpStatus == 200) {
		# $dsl->debug('bor_info alma_response status: ' . $httpStatus);
		# $dsl->debug('bor_info alma_response: ' . $alma_response);
		my $domUser = XML::LibXML->load_xml(string => $alma_response);
		if ($domUser->getElementsByTagName('user')) {
			$bor_info->{bor_name} = $domUser->findvalue('//user/full_name');
			foreach my $emailNode ($domUser->findnodes('//user/contact_info/emails/email')) {
				if ($emailNode->findvalue('./@preferred') eq 'true') {
					$bor_info->{bor_email} = $emailNode->findvalue('./email_address');
				}
			}
			foreach my $phoneNode ($domUser->findnodes('//user/contact_info/phones/phone')) {
				if ($phoneNode->findvalue('./@preferred') eq 'true') {
					$bor_info->{bor_phone} = $phoneNode->findvalue('./phone_number');
				}
			}
			foreach my $addrNode ($domUser->findnodes('//user/contact_info/addresses/address')) {
				if ($addrNode->findvalue('./@preferred') eq 'true') {
					my $addrStr = $addrNode->findvalue('./line1');
					my $line2 = $addrNode->findvalue('./line2');
					my $line3 = $addrNode->findvalue('./line3');
					my $line4 = $addrNode->findvalue('./line4');
					my $line5 = $addrNode->findvalue('./line5');
					my $city = $addrNode->findvalue('./city');
					my $state = $addrNode->findvalue('./state_province');
					my $zip = $addrNode->findvalue('./postal_code');
					my $country = $addrNode->findvalue('./country');
					if (length($line2) > 0) {
						$addrStr.= ", ". $line2;
					}
					if (length($line3) > 0) {
						$addrStr.= ", ". $line3;
					}
					if (length($line4) > 0) {
						$addrStr.= ", ". $line4;
					}
					if (length($line5) > 0) {
						$addrStr.= ", ". $line5;
					}
					if (length($city) > 0) {
						$addrStr.= ", ". $city;
					}
					if (length($state) > 0) {
						$addrStr.= ", ". $state;
					}
					if (length($zip) > 0) {
						if (length($state) > 0) {
							$addrStr.= " ". $zip;
						} else {
							$addrStr.= ", ". $zip;
						}
					}
					if (length($country) > 0) {
						$addrStr.= ", ". $country;
					}
					$bor_info->{bor_address} = $addrStr;
				}
			}
			$bor_info->{bor_status} = $domUser->findvalue('//user/user_group/@desc');
			$bor_info->{bor_status_code} = $domUser->findvalue('//user/user_group');
			foreach my $statNode ($domUser->findnodes('//user/user_statistics/user_statistic')) {
				if ($statNode->findvalue('./category_type') eq 'PATRONTYPE') {
					$bor_info->{bor_patron_type} = $statNode->findvalue('./statistic_category/@desc');
				}
			}
			$bor_info->{bor_exp_date} = $domUser->findvalue('//user/expiry_date');
			$bor_info->{total_requests} = $domUser->findvalue('//user/requests');
			$bor_info->{requests_link} = $domUser->findvalue('//user/requests/@link');
			$bor_info->{total_loans} = $domUser->findvalue('//user/loans');
			$bor_info->{loans_link} = $domUser->findvalue('//user/loans/@link');
			$bor_info->{total_cash} = $domUser->findvalue('//user/fees');
			$bor_info->{fees_link} = $domUser->findvalue('//user/fees/@link');

			foreach my $noteNode ($domUser->findnodes('//user/user_notes/user_note')) {
				if ($noteNode->findvalue('./note_type') eq 'LIBRARY') {
					my $noteText = $noteNode->findvalue('./note_text');
					if ($noteText =~ /^HOME_LIBRARY:/) {
						$noteText =~ s/HOME_LIBRARY: //;
						$bor_info->{bor_home_library} = $noteText;
					}
				}
			}
			# $dsl->debug( Dumper( $bor_info ) );
		} else {
			$dsl->debug('bor_info alma_response: ' . $alma_response);
			$bor_info->{error} = "Patron information is not available for ID: " . $bor_id;
			if (not $maintenance_mode) {
				my %errData = ();
				$errData{user_id} = $bor_id;
				$errData{response} = $alma_response;
			
				my $jsonErr = to_json (\%errData);
				$dsl->debug("bor_info  error data: " . $jsonErr);
				dscLog("Failed to get patron info.", $jsonErr, 14);
			}
		}
	} else {
		$dsl->debug('bor_info alma_response: ' . $alma_response);
		$bor_info->{error} = "Patron information is not available for ID: " . $bor_id;
		if (not $maintenance_mode) {
			my %errData = ();
			$errData{user_id} = $bor_id;
			$errData{httpStatus} = $httpStatus;
			$errData{response} = $alma_response;
		
			my $jsonErr = to_json (\%errData);
			$dsl->debug("bor_info  error data: " . $jsonErr);
			dscLog("Failed to get patron info.", $jsonErr, 14);
		}
	}
    $bor_info;
};

register 'bor_fines' => sub {
	my ($dsl, $bor_id) = @_;
	my $config = $dsl->app->config;
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

	my $fine_info = {};
	my $fines_items = [];
	my $fines_summary = {};
	my $total_fines = {};

    my @fineStatus = ("ACTIVE","INDISPUTE","EXPORTED");
    foreach (@fineStatus) {
        my $urlFines = $api_uri . sprintf("users/%s/fees?user_id_type=all_unique&status=%s", $bor_id, $_);
        # $dsl->debug('bor_fines urlFines: ' . $urlFines);
    
        my $alma_response = `curl -sS -X GET "${urlFines}" -H "Authorization: apikey ${api_key}"`;
        # $dsl->debug('bor_fines alma_response: ' . $alma_response);
        my $domFines = XML::LibXML->load_xml(string => $alma_response);
        foreach my $fineNode ($domFines->findnodes('//fees/fee')) {
            # $dsl->debug('fineNode: ' . $fineNode->toString());
        	my $f = {};
            $f->{status} = $fineNode->findvalue('./status/@desc');
        	$f->{sublibrary} = $fineNode->findvalue('./owner/@desc');
        	$f->{title} = $fineNode->findvalue('./title');
        	my $tdate = $fineNode->findvalue('./creation_time');
        	$f->{transaction_date} = sprintf("%s/%s/%s", substr($tdate, 5, 2), substr($tdate, 8, 2), substr($tdate, 0, 4) );
        	$f->{transaction_desc} = $fineNode->findvalue('./type/@desc');
        	my $fine_net_sum = $fineNode->findvalue('./balance');
        	# if ($fine_net_sum =~ m/^\((.*)\)$/) {
        	# 	$fine_net_sum = '-' . $1;
        	# }
        	$f->{fine_net_sum} = format_number $fine_net_sum, 2, 2;
        	# $f->{fine_status} = $fineNode->findvalue('./z31-status');
            if ($f->{transaction_desc} eq 'Credit') {
                $f->{credit_debit} = 'Credit';
            } else {
                $f->{credit_debit} = 'Debit';
            }
        	if ($f->{fine_net_sum} ne '0.00') {
        		push @{$fines_items}, $f;
        	}
        }
    }

	$fine_info->{fines} = $fines_items;
	$fine_info;
};

register 'bor_loans' => sub {
    my ($dsl, $bor_id, $cItmCnt, $renewableCnt) = @_;
    my $config = $dsl->app->config;
    my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

	my $loan_info = {};
	my @loanItems = ();

    my $recCount = 0;
	my $hasMore = 1;

	my $urlLoan = $api_uri . sprintf("users/%s/loans?user_id_type=all_unique&limit=100&offset=%s&order_by=id&direction=ASC&loan_status=Active&expand=renewable", $bor_id, $cItmCnt);
	# $dsl->debug('bor_loans url: ' . $urlLoan );

	my $alma_response = `curl -sS -X GET "${urlLoan}" -H "Authorization: apikey ${api_key}"`;
	# $dsl->debug('bor_loans alma_response: ' . $alma_response);
	my $domLoan = XML::LibXML->load_xml(string => $alma_response);

	$recCount = $domLoan->findvalue('//item_loans/@total_record_count');
	$dsl->debug( "rec count " . $recCount );
	foreach my $loanNode ($domLoan->findnodes('//item_loans/item_loan')) {
		my $l = {};
		my $renewable = $loanNode->findvalue('./renewable');
		if ($renewable eq 'true') {
			$l->{renewals} = 'Y';
			$renewableCnt++
		} else {
			$l->{renewals} = 'N';
		}
		# item renew needs loan id
		$l->{rec_key} = $loanNode->findvalue('./loan_id');
		$l->{sysid} = $loanNode->findvalue('./mms_id');

		$l->{sublibrary} = $loanNode->findvalue('./library/@desc');
		$l->{title} = $loanNode->findvalue('./title');
		my $authorStr = $loanNode->findvalue('./author');
		$authorStr =~ s/ https:.*$//;
		$l->{author} = $authorStr;
		$l->{year} = $loanNode->findvalue('./publication_year');
		$l->{call_no} = $loanNode->findvalue('./call_number');
		$l->{barcode} = $loanNode->findvalue('./item_barcode');
		my $ddate = substr($loanNode->findvalue('./due_date'), 0, 19);
		my $format = DateTime::Format::Strptime->new(pattern   => "%FT%T", strict    => 1, time_zone => "UTC", on_error  => "croak");
		my $dt = $format->parse_datetime($ddate);
		$dt->set_time_zone("America/New_York");
		$l->{due_date} = $dt->mdy('/');
		push @loanItems, $l;
	}

	my $loanCnt = scalar @loanItems;
	$cItmCnt = $cItmCnt + $loanCnt;
	$dsl->debug("loan count: " . $cItmCnt . " total: " . $recCount . " url: " . $urlLoan);
	if ($cItmCnt == $recCount ) {
		$hasMore = 0;
	}


    $loan_info->{loan_items} = \@loanItems;
	$loan_info->{renewable_cnt} = $renewableCnt;
	$loan_info->{hasMore} = $hasMore;
	$loan_info->{cItmCnt} = $cItmCnt;
	# $dsl->debug( Dumper( $loan_info ) );
    $loan_info
};

register 'bor_holds' => sub {
	my ($dsl, $bor_id) = @_;
	my $config = $dsl->app->config;
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

	# my $urlHold = $api_uri . sprintf("users/%s/requests?user_id_type=all_unique&limit=100&offset=0&status=active", $bor_id);
    my $urlHold = $api_uri . sprintf("users/%s/requests?user_id_type=all_unique&limit=100&offset=0&status=active&apikey=%s", $bor_id, $api_key);
    # $dsl->debug('bor_holds url: ' . $urlHold );
    # my $alma_response = `curl -X GET "${urlHold}" -H "Authorization: apikey: ${api_key}"`;
    my $alma_response = `curl -sS -X GET "${urlHold}"`;
    # $dsl->debug('bor_holds alma_response: ' . $alma_response);
    my $domHold = XML::LibXML->load_xml(string => $alma_response);
	my $hold_info ={};
	my @holdItems = ();

    foreach my $holdNode ($domHold->findnodes('//user_requests/user_request'))  {
        my $h = {};

        # if ($key eq 'delete') {
        #     $h->{cancelable} = $val;
        # }
        $h->{rec_key} = $holdNode->findvalue('./request_id');
        my $authorStr = $holdNode->findvalue('./author');
		$authorStr =~ s/ https:.*$//;
		# $authorStr = encode('utf8', $authorStr);
		# $dsl->debug('bor_holds author: ' . $authorStr);
		$h->{author} = $authorStr;
		my $titleStr = $holdNode->findvalue('./title');
		# $dsl->debug('bor_holds title: ' . $titleStr);
		$h->{title} = $titleStr;
    #	$h->{year} = $holdNode->findvalue('z13/z13-year');
        my $holdStatus = $holdNode->findvalue('./request_status');
        # TODO should this also use task_name or a combo
        # if ($holdStatus eq 'In Process') {
        #     my $endHoldDate = $holdNode->findvalue('./expiry_date');
        #     $h->{hold_status} = sprintf('On hold until %s/%s', substr($endHoldDate, 5, 2), substr($endHoldDate, 8, 2));
        # } else {
            $h->{hold_status} = $holdStatus;
        # }
        my $rdate = $holdNode->findvalue('./request_date');
        $h->{hold_recall_date} = sprintf("%s/%s/%s", substr($rdate, 5, 2), substr($rdate, 8, 2), substr($rdate, 0, 4) );
    #	$h->{end_request_date} = $holdNode->findvalue('./expiry_date');
        $h->{pickup_location} = $holdNode->findvalue('./pickup_location');
        $h->{sublibrary} = $holdNode->findvalue('./managed_by_library');

        push @holdItems, $h;
    }
	
	$hold_info->{hold_items} = [@holdItems];
	# $dsl->debug( Dumper( $hold_info ) );
	$hold_info;
};

register 'renew_item' => sub {
	my $dsl = shift;
	my %args = @_;
    # rec_key is loan_id
	my $rec_key = $args{rec_key};
	my $bor_id = $args{bor_id};

	my $config = $dsl->app->config;
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

    # my $renewUrl = $api_uri . sprintf("users/%s/loans/%s?user_id_type=all_unique&op=renew", $bor_id, $rec_key);
    my $renewUrl = $api_uri . sprintf("users/%s/loans/%s?user_id_type=all_unique&op=renew&apikey=%s", $bor_id, $rec_key, $api_key);
	# $dsl->debug($renewUrl);
    # my $alma_response = `curl -X POST "${renewUrl}" -H "Authorization: apikey: ${api_key}"`;
    my $alma_response = `curl -sS -X POST "${renewUrl}"`;

    # $dsl->debug('renew_item alma_response: ' . $alma_response);
    my $dom = XML::LibXML->load_xml(string => $alma_response);
    my $xpc = XML::LibXML::XPathContext->new($dom);
    $xpc->registerNs('el',  'http://com/exlibris/urm/general/xmlbeans');
	my $error = "";
	my $reply = "";

    foreach my $errNode ($xpc->findnodes('//el:web_service_result/el:errorList/el:error'))  {
        # $dsl->debug('errNode: ' . $errNode);
        my $errCode = '';
        my $errMsg = '';
        foreach my $childNode ($errNode->childNodes()) {
            my $name = $childNode->nodeName;
            if ($name eq 'errorCode') {
                $errCode = $childNode->to_literal;
            }
            if ($name eq 'errorMessage') {
                $errMsg = $childNode->to_literal;
            }
        }
        # $dsl->debug('errCode: ' . $errCode . ' errMsg: ' . $errMsg);
        if ($errCode eq '401822') {
            $error = $errMsg;
            $error =~ s/\s\d*\s-//;
        } else {
            $error = $errMsg;
        }
    }
    $dsl->debug('Error msg: ' . $error);

    $reply = $dom->findvalue('//item_loan/last_renew_status/@desc');


	return { reply => $reply, error => $error };
};

register 'cancel_hold_request' => sub {
	my $dsl = shift;
	my %args = @_;
    # rec_key is request_id
	my $rec_key = $args{rec_key};
    my $bor_id = $args{bor_id};

	my $config = $dsl->app->config;
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

    my $reqUrl = $api_uri . sprintf("users/%s/requests/%s?reason=CancelledAtPatronRequest&apikey=%s", $bor_id, $rec_key, $api_key);
    # $dsl->debug($reqUrl);
    my $alma_response = `curl -sS -I -X DELETE "${reqUrl}"`;
    my $status = substr($alma_response, 9, 3);
    # $dsl->debug("HTTP code: " . $status);
    my $error = "";
	my $reply = "";
    if ($status eq '204') {
        $reply = "Cancelled request";
    } else {
        $error = "Error cancelling request"
    }
	
	
	return { reply => $reply, error => $error };
};

register 'get_item_info'  => sub {
	# in the event you need to interact with the Dancer2 dsl (or app)
	my $dsl = shift;

	my $config = $dsl->app->config;
    my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};
    # my $json_ld_url = $config->{alma_json_ld};
	my $maintenance_mode = $config->{maintenance_mode};
	# test down api
	if (0) {
		$api_uri = "https://api-na-bad.hosted.exlibrisgroup.com/almaws/v1/";
	}

	# TODO debug
	my $debug = 0;
	my %data = ();
	my $sysid = "";
	my $userid = "";
	($sysid, $userid) = @_;

	if (!defined $userid) {
		$userid = "";
	}

    # Change sysid to MMS ID
    my $mmsid = "";
    if (length($sysid) eq '9') {
        $mmsid = sprintf("99%s0108501", $sysid);
    } else {
        $mmsid = $sysid;
    }
	$dsl->debug( "get_item_info START " . $sysid . " USER: " . $userid . " MMS ID: " . $mmsid);
    $data{bibInfo}{mmsid} =  $mmsid;
	my $hasSCL = 0;
	my $homeLib = "NONE";

	# get page info:
    if (length($userid) > 0) {
        my $blocked = 'N';
        my $blockedMsg = "";

        my $cashLimit = "";
        my $fineBalance = "";
        my $fineSign = "";

        my $loanNodes = "";
        my $holdNodes = "";

        my $urlUser = $api_uri . sprintf("users/%s?user_id_type=all_unique&view=full&expand=loans,requests,fees", $userid);
        # $dsl->debug('get_item_info user url: ' . $urlUser);
        my $alma_response = `curl -sS --write-out "HTTPSTATUS:%{http_code}" -X GET "${urlUser}" -H "Authorization: apikey ${api_key}"`;
		my $httpStatus = 0;
		if ($alma_response =~ s/HTTPSTATUS:(?<status>\d{3})$//) {
			$httpStatus = $+{status};
		}
		if ($httpStatus == 200) {
			my $domUser = XML::LibXML->load_xml(string => $alma_response);
			# $dsl->debug('get_item_info user alma_response: ' . $alma_response);
			if ($domUser->getElementsByTagName('user')) {
				my $rawExpDate = $domUser->findvalue('//user/expiry_date');
				my $expDate = DateTime->new(year => substr($rawExpDate, 0, 4)
					, month => substr($rawExpDate, 5, 2)
					, day => substr($rawExpDate, 8, 2) );
				my $expEpoch = $expDate->epoch;
				my $currEpoch = time();
				if ($currEpoch > $expEpoch) {
					$blocked = 'Y';
					$blockedMsg = "Your library account has expired. Please check with your <a href=\"https://library.duke.edu/about/contact\">local Duke library</a> for assistance.";
				}
				$data{userInfo}{borExpDate} = substr($rawExpDate, 0, 10);
				$data{userInfo}{borStatusCode} = $domUser->findvalue('//user/user_group');
				if ($data{userInfo}{borStatusCode} !~ m/\d\d/ || $data{userInfo}{borStatusCode} eq "22") {
					$data{userInfo}{borStatusCode} = "22";
					$blocked = 'Y';
					$blockedMsg = "Your account has a invalid or missing Patron Status; please check with the circulation desk.";
				}

				foreach my $statNode ($domUser->findnodes('//user/user_statistics/user_statistic')) {
					if ($statNode->findvalue('./category_type') eq 'PATRONTYPE') {
						$data{userInfo}{borType} = $statNode->findvalue('./statistic_category/@desc');
					}
					if ($data{userInfo}{borType} =~ m/^Staff - Library Staff/) {
						$data{userInfo}{isStaff} = 'Y';
					} else {
						$data{userInfo}{isStaff} = 'N';
					}
					if ($statNode->findvalue('./category_type') eq 'ELIGIBLEFOR') {
						my $eligibleFor = $statNode->findvalue('./statistic_category');
						# $dsl->debug('eligibleFor: ' . $eligibleFor);
						if ($eligibleFor =~ m/^ILR/) {
							$eligibleFor =~ s/ILR//;
							$data{userInfo}{ilr} = $eligibleFor;
						}
						if ($eligibleFor =~ m/^SCAN/) {
							$eligibleFor =~ s/SCAN//;
							$data{userInfo}{digitization} = $eligibleFor;
						}
						if ($eligibleFor =~ m/HONORSUGRAD|ITEMPAGING|ITEMPAGINGMARINE/) {
							$data{userInfo}{paging} = 'Y';
						}
					}
				}
				foreach my $noteNode ($domUser->findnodes('//user/user_notes/user_note')) {
					if ($noteNode->findvalue('./note_type') eq 'LIBRARY') {
						my $noteText = $noteNode->findvalue('./note_text');
						if ($noteText =~ /^HOME_LIBRARY:/) {
							$noteText =~ s/HOME_LIBRARY: //;
							$data{userInfo}{homeLib} = $noteText;
						}
					}
				}
			
				# patron blocks
				if ($blocked eq 'N') {
					foreach my $noteNode ($domUser->findnodes('//user/user_blocks/user_block')) {
						if ($noteNode->findvalue('./block_status') eq 'ACTIVE') {
							$blocked = 'Y';
							$blockedMsg = "Your account has blocks; please check with the circulation desk.";
						}
					}
				}

				# if ($blocked eq 'N') {
				#     my $cashLimit = 0;
				#     # get cash limit from rules
				# 	my $dukFines = $domUser->findvalue('//user/fees');
				# 	if ($dukFines >= $cashLimit) {
				# 		$blocked = 'Y';
				# 		$blockedMsg = "This request cannot be processed due to current fines on your library account.  Please check your <a href=\"/account\">library account</a> for details or contact <a href=\"mailto:perkins-requests\@duke.edu\">perkins-requests\@duke.edu</a> or your <a href=\"https://library.duke.edu/libraries\">local library desk</a> for assistance.";
				# 	}
				# }

				$data{userInfo}{blocked} = $blocked;
				$data{userInfo}{blockedMsg} = $blockedMsg;

				# userState Y=> has id, D=> no id, but has SCL/ARCH, N=> no id, but should, E=> has error
				$data{userInfo}{userState} = 'Y';
			} else {
				# $dsl->debug('get_item_info user alma_response: ' . $alma_response);
				$data{userInfo}{userState} = 'E';
				if (not $maintenance_mode) {
					my %errData = ();
					$errData{mmsid} = $mmsid;
					$errData{user_id} = $userid;
					$errData{response} = $alma_response;
					
					my $jsonErr = to_json (\%errData);
					$dsl->debug("get_item_info userInfo error data: " . $jsonErr);
					dscLog("Failed to get patron info.", $jsonErr, 14);
				}
			}
		} else {
            # $dsl->debug('get_item_info user alma_response: ' . $alma_response);
			$data{userInfo}{userState} = 'E';
			if (not $maintenance_mode) {
				my %errData = ();
				$errData{mmsid} = $mmsid;
				$errData{user_id} = $userid;
				$errData{httpStatus} = $httpStatus;
				$errData{response} = $alma_response;
				
				my $jsonErr = to_json (\%errData);
				$dsl->debug("get_item_info userInfo error data: " . $jsonErr);
				dscLog("Failed to get patron info.", $jsonErr, 14);
			}
		}
    }

    # my $urlBib = $json_ld_url . sprintf("bibs/%s", $mmsid);
    # my $urlBib = $api_uri . sprintf("bibs/%s?view=full&expand=p_avail", $mmsid);
	my $urlBib = sprintf("https://na05.alma.exlibrisgroup.com/view/sru/01DUKE_INST?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.mms_id=%s", $mmsid);
    # $dsl->debug( "urlBib " . $urlBib);
    # my $alma_response = `curl -sS --write-out "HTTPSTATUS:%{http_code}" -X GET "${urlBib}" -H "Authorization: apikey ${api_key}" -H "accept: application/json"`;
	my $alma_response = `curl -sS --write-out "HTTPSTATUS:%{http_code}" -X GET "${urlBib}"`;
	my $httpStatus = 0;
	if ($alma_response =~ s/HTTPSTATUS:(?<status>\d{3})$//) {
		$httpStatus = $+{status};
	}
	if ($httpStatus == 200) {
		# $dsl->debug( "SRU bib_response " . $alma_response);
		my $domBib = XML::LibXML->load_xml(string => $alma_response);
		my $xpc = XML::LibXML::XPathContext->new($domBib);
		$xpc->registerNs('s',  'http://www.loc.gov/zing/srw/');
		$xpc->registerNs('m',  'http://www.loc.gov/MARC21/slim');
    	my $recCount = $xpc->findvalue('//s:searchRetrieveResponse/s:numberOfRecords');
        # $dsl->debug( "rec count " . $recCount );
		if ($recCount == 1) {
			foreach my $recNode ($xpc->findnodes('//s:searchRetrieveResponse/s:records/s:record/s:recordData/m:record')) {
				my $domb = XML::LibXML::XPathContext->new($recNode);
				$domb->registerNs('m',  'http://www.loc.gov/MARC21/slim');
				# my  = $recordsNode->find('./record/recordData/record');
				my $leader = $domb->findvalue('./m:leader');
				$data{bibInfo}{bibLevel} = substr($leader, 7 ,1);
				$data{bibInfo}{tmpLeader} =  $leader;
				if (substr($leader, 6 ,1) =~ m/a/) {
					if ($data{bibInfo}{bibLevel} =~ m/b|s/) {
						$data{bibInfo}{multiVol} =  "Y";
					} else {
						$data{bibInfo}{multiVol} =  "N";
					}
				} elsif (substr($leader, 6 ,1) =~ m/p/) {
					if ($data{bibInfo}{bibLevel} =~ m/c/) {
						$data{bibInfo}{multiVol} =  "Y";
					} else {
						$data{bibInfo}{multiVol} =  "U";
					}
				} else {
					$data{bibInfo}{multiVol} =  "U";
				}
				$data{bibInfo}{title} = $domb->findvalue('./m:datafield[@tag="245"]/m:subfield[@code="a"]');
				$data{bibInfo}{subTitle} = $domb->findvalue('./m:datafield[@tag="245"]/m:subfield[@code="b"]');
					#	$data{bibInfo}{sor} = $domb->findvalue('./m:datafield[@tag="245"]/m:subfield[@code="c"]');
				$data{bibInfo}{author} = $domb->findvalue('./m:datafield[@tag="100"]/m:subfield[@code="a"]');
				if (!$data{bibInfo}{author}) {
					$data{bibInfo}{author} = $domb->findvalue('./m:datafield[@tag="110"]/m:subfield[@code="a"]');
				}
				if (!$data{bibInfo}{author}) {
					foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="700"]')) {
						my $dfxp = XML::LibXML::XPathContext->new($dfnode);
						$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
						if (!$data{bibInfo}{author}) {
							$data{bibInfo}{author} = $dfxp->findvalue('./m:subfield[@code="a"]') . ' ' . $dfxp->findvalue('./m:subfield[@code="e"]');
						} else {
							$data{bibInfo}{author} = $data{bibInfo}{author} . ' - ' . $dfxp->findvalue('./m:subfield[@code="a"]') . ' ' . $dfxp->findvalue('./m:subfield[@code="e"]');
						}
					}
				}
				if (!$data{bibInfo}{author}) {
					foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="710"]')) {
						my $dfxp = XML::LibXML::XPathContext->new($dfnode);
						$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
						if (!$data{bibInfo}{author}) {
							$data{bibInfo}{author} = $dfxp->findvalue('./m:subfield[@code="a"]') . ' ' . $dfxp->findvalue('./m:subfield[@code="e"]');
						} else {
							$data{bibInfo}{author} = $data{bibInfo}{author} . ' - ' . $dfxp->findvalue('./m:subfield[@code="a"]') . ' ' . $dfxp->findvalue('./m:subfield[@code="e"]');
						}
					}
				}
				my $imprint = $domb->findvalue('./m:datafield[@tag="264"]/m:subfield[@code="a"]');
				my $imprint_b = $domb->findvalue('./m:datafield[@tag="264"]/m:subfield[@code="b"]');
				if (length($imprint_b)) {
					$imprint .= " " . $imprint_b;
				}
				$data{bibInfo}{imprint} = $imprint;
				$data{bibInfo}{pubDate} = $domb->findvalue('./m:datafield[@tag="264"]/m:subfield[@code="c"]');

				#     $data{bibInfo}{oclc} = $domb->findvalue('./m:datafield[@tag="035"]/m:subfield[@code="a"]');
				foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="035"]')) {
					my $dfxp = XML::LibXML::XPathContext->new($dfnode);
					$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
					my $m035 = $dfxp->findvalue('./m:subfield[@code="a"]');
					if ($m035 =~ /\(NcD\)\d{9}DUK01/) {
						$data{bibInfo}{aleph_id} = $m035;
					} else {
						$data{bibInfo}{oclc} = $m035;
					}
				}

				# we may multiple isbn values
				my @isn = ();
				foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="020"]')) {
					my $dfxp = XML::LibXML::XPathContext->new($dfnode);
					$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
					push @isn, $dfxp->findvalue('./m:subfield[@code="a"]');
				}
				$data{bibInfo}{isn} = \@isn;
				$data{bibInfo}{description} = $domb->findvalue('./m:datafield[@tag="300"]/m:subfield[@code="a"]');
				$data{bibInfo}{roAccess} = $domb->findvalue('./m:datafield[@tag="506"]/m:subfield[@code="a"]');
				# MARC 21 – 773, 774, 777, 780, 785, 786, 800, 810, 811, and 830
				my $relatedIssn = $domb->findvalue('./m:datafield[@tag="830"]/m:subfield[@code="x"]');
				$relatedIssn =~ s/[^0-9\-]//g;
				# $dsl->debug("related records issn " . $relatedIssn);
				my %locations = ();
				my %libraries= ();
				my $totalItmCnt = 0;
				my @part_of_mmsid = ();
				my @relatedRecs = ();
				foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="AVA"]')) {
					my $dfxp = XML::LibXML::XPathContext->new($dfnode);
					$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
					my $hold_mmsid = $dfxp->findvalue('./m:subfield[@code="0"]');
					if ($hold_mmsid ne $mmsid) {
						# TODO related records check
						my $isRelated = 0;
						my $urlLDPart = sprintf("https://open-na.hosted.exlibrisgroup.com/alma/01DUKE_INST/bibs/%s", $hold_mmsid);
						my $alma_response_part = `curl -sS -X GET "${urlLDPart}"`;
						my $bibPartLinkedDataRef = decode_json $alma_response_part;
						$dsl->debug("related records check " . Dumper($bibPartLinkedDataRef));
						my %bibPartLinkedData = %$bibPartLinkedDataRef;
						my $issn = "";
						my $identifiers = $bibPartLinkedData{identifier};
						$dsl->debug("identifiers type " . ref $identifiers);
						if (ref $identifiers eq "ARRAY") {
							foreach my $identifier (@{$identifiers}) {
								$dsl->debug("identifier ". Dumper($identifier));
								my %indent = %{$identifier};
								my $idType = $indent{'@type'};
								$dsl->debug("identifier type " . $idType);
								if ($idType eq "bibo:issn") {
									$issn = $indent{value};
									if ($issn eq $relatedIssn) {
										$isRelated = 1;
									}
								}
							}
						}
						if (ref $identifiers eq "HASH") {
							$dsl->debug("identifier ". Dumper($identifiers));
							my %indent = %{$identifiers};
							my $idType = $indent{'@type'};
							$dsl->debug("identifier type " . $idType);
							if ($idType eq "bibo:issn") {
								$issn = $indent{value};
								if ($issn eq $relatedIssn) {
									$isRelated = 1;
								}
							}
						}

						if ($isRelated) {
							my %relRec = ( mmsid => $hold_mmsid, title => $bibPartLinkedData{title}, issn => $issn);
							push @relatedRecs, \%relRec; # TODO AK-225
						} else {
							push @part_of_mmsid, $hold_mmsid;
						}
					}
					my %holding = ();
					my $holdId = $dfxp->findvalue('./m:subfield[@code="8"]');
					if (length($holdId) > 0) {
						$holding{id} = $holdId;
						$holding{tempLoc} = 'N';
					} else {
						$holding{tempLoc} = 'Y';
					}
					$holding{location} = $dfxp->findvalue('./m:subfield[@code="d"]');
					$holding{status} = $dfxp->findvalue('./m:subfield[@code="e"]');
					my $count = 1;
					$count = $dfxp->findvalue('./m:subfield[@code="f"]');
					$holding{count} = $count;
					$totalItmCnt += $count;
					
					my $libcode = $dfxp->findvalue('./m:subfield[@code="b"]');
					if ($libcode =~ m/SCL|ARCH/) {
						$hasSCL = 1;
					}
					my $libName = $dfxp->findvalue('./m:subfield[@code="q"]');
					$libName =~ s/\'/&apos;/g;
					if (!exists $libraries{$libcode}) {
						$libraries{$libcode}{name} = $libName;
					}
					my $collcode = $dfxp->findvalue('./m:subfield[@code="j"]');
					my $locCode = sprintf('%s-%s', $libcode, $collcode);
					if (exists $locations{$locCode}) {
						push @{$locations{$locCode}{holdings}}, \%holding;
						$locations{$locCode}{count} = $locations{$locCode}{count} + $count;
					} else {
						$locations{$locCode}{libName} = $libName;
						$locations{$locCode}{collName} = $dfxp->findvalue('./m:subfield[@code="c"]');
						$locations{$locCode}{count} = $count;
						my @holdings = ();
						push @holdings, \%holding;
						$locations{$locCode}{holdings} = \@holdings;
					}
				}
				my $relatedRecsJson = encode_json(\@relatedRecs);
				# $dsl->debug("Related records ". $relatedRecsJson);
				foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="AVE"]')) {
					my $dfxp = XML::LibXML::XPathContext->new($dfnode);
					$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
					my $hold_mmsid = $dfxp->findvalue('./m:subfield[@code="0"]');
					if ($hold_mmsid ne $mmsid) {
						$dsl->debug("Has related Electronic rec " . $hold_mmsid);
					} else {
						$dsl->debug("This is an Electronic record");
						# AVE $$8 -> Portfolio ID
						# AVE $$c -> Collection ID
						# AVE $$m -> Collection Name
						my $url = $domb->findvalue('./m:datafield[@tag="856"]/m:subfield[@code="u"]');
						if (length($url) > 0) {
							$data{bibInfo}{directLink} = $url; # TODO AK-225
						} else {
							if ($domb->findvalue('./m:datafield[@tag="024"]/m:subfield[@code="2"]') eq "doi") {
								my $doi = $domb->findvalue('./m:datafield[@tag="024"]/m:subfield[@code="a"]');
								my $summonLink = sprintf("https://duke.summon.serialssolutions.com/search?q=(DOI:('%s'))", $doi);
								$data{bibInfo}{summonLink} = $summonLink; # TODO AK-225
							}
							if (!defined $data{bibInfo}{summonLink}) {
								foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="035"]')) {
									my $dfxp = XML::LibXML::XPathContext->new($dfnode);
									$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
									my $m035 = $dfxp->findvalue('./m:subfield[@code="a"]');
									if ($m035 =~ /^\(SSID\)/) {
										my $ssid = substr($m035, 6);
										my $summonLink = sprintf("https://duke.summon.serialssolutions.com/search?q=%s", $ssid);
										$data{bibInfo}{summonLink} = $summonLink; # TODO AK-225
									} 
								}
							}
							if (!defined $data{bibInfo}{summonLink}) {
								foreach my $dfnode ($domb->findnodes('./m:datafield[@tag="035"]')) {
									my $dfxp = XML::LibXML::XPathContext->new($dfnode);
									$dfxp->registerNs('m',  'http://www.loc.gov/MARC21/slim');
									my $m035 = $dfxp->findvalue('./m:subfield[@code="a"]');
									if ($m035 =~ /^\(OCoLC\)/) {
										my $oclc = substr($m035, 7);
										my $summonLink = sprintf("https://duke.summon.serialssolutions.com/search?q=(OCLC:(%s))", $oclc);
										$data{bibInfo}{summonLink} = $summonLink; # TODO AK-225
									} 
								}
							}
						}
					}
				}
				$data{bibInfo}{part_of_mmsid} = encode_json(\@part_of_mmsid);
				$data{bibInfo}{relatedRecs} = $relatedRecsJson;
				$data{bibInfo}{holdings} = encode_json(\%locations);
				$data{bibInfo}{totalItems} = $totalItmCnt;
				$dsl->debug("libraries ". encode_json(\%libraries));
				$data{libraries} = encode_json(\%libraries);

				# userState Y=> has id, D=> no id, but has SCL/m:ARCH, N=> no id, but should, E=> has error
				if (!defined $data{userInfo}{userState}) {
					if ($hasSCL) {
						$data{userInfo}{userState} = 'D';
					} else {
						$data{userInfo}{userState} = 'N';
					}
				}
				# for testing
				if ($debug) {
					foreach my $lv1 (sort keys %data) {
						for my $lv2 (sort keys %{$data{$lv1}}) {
							if ($lv2 =~ /^tmp/) {
								delete $data{$lv1}{$lv2};
							}
						}
					}
				}
			}
		} else {
			# $dsl->debug( "bib_response not one rec " . $alma_response);
			$data{bibInfo}{error} = "Bibliographic information cannot be found.";
			if (not $maintenance_mode) {
				my %errData = ();
				$errData{mmsid} = $mmsid;
				$errData{user_id} = $userid;
				$errData{response} = $alma_response;
				my $jsonErr = to_json (\%errData);
				$dsl->debug("get_item_info bibData error data: " . $jsonErr);
				dscLog($data{bibInfo}{error}, $jsonErr, 3);
			}
		}
	} else {
		# $dsl->debug( "bib_response failed " . $alma_response);
		$data{bibInfo}{error} = "Bibliographic information cannot be found.";
		if (not $maintenance_mode) {
			my %errData = ();
			$errData{mmsid} = $mmsid;
			$errData{user_id} = $userid;
			$errData{response} = $alma_response;
			$errData{httpStatus} = $httpStatus;
			my $jsonErr = to_json (\%errData);
			$dsl->debug("get_item_info bibData error data: " . $jsonErr);
			dscLog($data{bibInfo}{error}, $jsonErr, 3);
		}
	}

	# $dsl->debug( "get_item_info END " . Dumper(%data));
	return %data;
};

register 'get_item_detail'  => sub {
	# in the event you need to interact with the Dancer2 dsl (or app)
	my $dsl = shift;

	my $config = $dsl->app->config;
	# $dsl->debug( Dumper ( $config ) );
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};
	my $maintenance_mode = $config->{maintenance_mode};

	my $debug = 0;
	my %data = ();

	my $sysid = "";
    my $mmsid = "";
	my $userid = "";
	my $multiVol = "";
	my $borStatus = "";
	my $userState = "";
	my $homeLib = "";
	my $aeonQueryBib = "";
	my $illiadLinkBib = "";
	# my $part_of_mmsid = "";
	my $userDigitization = "";
	my $userPaging = "";
	my $cItmCnt = 0;

	($sysid, $mmsid, $userid, $multiVol, $borStatus, $userState, $homeLib, $aeonQueryBib, $illiadLinkBib, $userDigitization, $userPaging, $cItmCnt) = @_;

	$dsl->debug( "get_item_detail START " . $sysid . " userid: " . $userid . " MMSID: " . $mmsid . " cItmCnt: " . $cItmCnt);
	# $dsl->debug( "multiVol: " . $multiVol . " borStatus: " . $borStatus . " userState: " . $userState . " homeLib: " . $homeLib);
	# $dsl->debug("ILLIAD URL " . $illiadLinkBib);
	# $dsl->debug("AEON URL " . $aeonQueryBib);
	# $dsl->debug("userDigitization: " . $userDigitization . " userPaging: " . $userPaging);
	# $dsl->debug("maintenance_mode: " . $maintenance_mode);

    $data{recInfo}{mmsid} = $mmsid;
	$data{recInfo}{userState} = $userState;
	$data{recInfo}{multiVol} = $multiVol;

	if (!defined $userid) {
	#	$userid = "circOnly";
		$userid = "";
	}

	
	# my $domr = XML::LibXML->load_xml(string => $ruleFile);

	my $hasSCL = 0;

	# Number of ALEPH items
	$data{recInfo}{itmCntAlma} =  0;

	# Number of AEON items
	$data{recInfo}{itmCntAeon} =  0;

	# Number of LSC items
	$data{recInfo}{itmCntLSC} = 0;

	$data{recInfo}{itmCntOther} =  0;
	$data{recInfo}{itmCntDesc} =  0;
	$data{recInfo}{itmCntMap} =  0;
	$data{recInfo}{blocked} = 'N';

	# TODO make loop for large number of items
	my $hasMore = 1;
	my $largeSet = 0;

	# https://api-na.hosted.exlibrisgroup.com/almaws/v1/bibs/990000123450108501/holdings/ALL/items?
	# limit=100&offset=0&expand=due_date_policy%2C%20due_date&user_id=jaf41&order_by=description&direction=asc&view=brief
	# &apikey=l8xx1f3348cf76e844bfa652ecc37742f7e6
	# TODO AK-270, Also change two recInfo.cItmCnt <= 30 in item-detail.tt
	my $urlItem = $api_uri . sprintf("bibs/%s/holdings/ALL/items?limit=30&offset=%s&expand=due_date_policy,due_date&user_id=%s&order_by=enum_a,enum_b,chron_i,description,receive_date&direction=asc&view=brief", $mmsid, $cItmCnt, $userid);
	# if (length($part_of_mmsid)) {
	# 	$dsl->debug("part_of_mmsid: " . $part_of_mmsid);
	# 	$urlItem = $api_uri . sprintf("bibs/%s/holdings/ALL/items?limit=100&offset=%s&expand=due_date_policy,due_date&user_id=%s&order_by=description&direction=asc&view=brief", $part_of_mmsid, $cItmCnt, $userid);
	# }
	# $dsl->debug( "urlItem: " . $urlItem);
	# get item info
	my $alma_response = `curl -sS -X GET "${urlItem}" -H "Authorization: apikey ${api_key}" -H "accept: application/json"`;
	# $dsl->debug( "items_response json " . $alma_response);
	my $itemDataRef = decode_json $alma_response;
	# $dsl->debug( "itemData " . Dumper($itemDataRef));
	my %itemData = %$itemDataRef;
	my $totalRecCnt = $itemData{total_record_count};
	if ($totalRecCnt > 50) {
		$largeSet = 1;
		# $dsl->debug( "largeSet has " . $totalRecCnt);
	}
	if ($totalRecCnt > 0) {
		my $item = $itemData{item};
		my $itmPerHoldCnt = scalar @{$item};
		$cItmCnt = $cItmCnt + $itmPerHoldCnt;
		# $dsl->debug("rec count: " . $cItmCnt . " total: " . $totalRecCnt . " url: " . $urlItem);
		if ($cItmCnt == $totalRecCnt ) {
			$hasMore = 0;
		}
		my $sortCnt = 0;
		foreach my $itmNode (@{$item}) {
			my $holding_data = %{$itmNode}{holding_data};
			my $item_data = %{$itmNode}{item_data};
			# $dsl->debug( "holding_data " . Dumper($holding_data));
			# $dsl->debug( "item_data " . Dumper($item_data));
			my $arrivalDate = %{$item_data}{arrival_date};
			my $expArrivalDate = %{$item_data}{expected_arrival_date};
			my $description = %{$item_data}{description};
			# $dsl->debug( "description: " . $description . " expArrivalDate: " . $expArrivalDate . " arrivalDate: " . $arrivalDate);
			my $enum_a = %{$item_data}{enumeration_a};
			my $enum_b = %{$item_data}{enumeration_b};
			my $enum_c = %{$item_data}{enumeration_c};
			my $chron_i = %{$item_data}{chronology_i};
			my $chron_j = %{$item_data}{chronology_j};
			my $chron_k = %{$item_data}{chronology_k};
			if ($enum_a eq "") {
				$enum_a = "0";
			}
			if ($enum_b eq "") {
				$enum_b = "0";
			}
			if ($enum_c eq "") {
				$enum_c = "0";
			}
			$enum_a =~ s/\D//g;
			$enum_b =~ s/\D//g;
			$enum_c =~ s/\D//g;
			# $dsl->debug("enum_a: " . $enum_a . " enum_b: " . $enum_b . " enum_c: " . $enum_c);
			my $enumSortKey = sprintf("%06s%04s%04s", $enum_a, $enum_b, $enum_c);
			if ($chron_i eq "") {
				$chron_i = "0";
			}
			if ($chron_j eq "") {
				$chron_j = "0";
			}
			if ($chron_k eq "") {
				$chron_k = "0";
			}
			$chron_i =~ s/\D//g;
			$chron_j =~ s/\D//g;
			$chron_k =~ s/\D//g;
			# $dsl->debug("chron_i: " . $chron_i . " chron_j: " . $chron_j . " chron_k: " . $chron_k);
			my $chronSortKey = sprintf("%06s%04s%04s", $chron_i, $chron_j, $chron_j);

			my $sortKey = substr($arrivalDate, 0, -1) . "-" . sprintf("%02s", $sortCnt);
			if ($enumSortKey > 0) {
				$sortKey = $enumSortKey . "-" . sprintf("%02s", $sortCnt);
			} elsif ($chronSortKey > 0) {
				$sortKey = $chronSortKey . "-" . sprintf("%02s", $sortCnt);
			} elsif (length($description)) {
				$sortKey = $description . "-" . sprintf("%02s", $sortCnt);
			}
			$sortCnt++;
			# $dsl->debug("sortKey: " . $sortKey . " enumSortKey: " . $enumSortKey . " chronSortKey: " . $chronSortKey);
			$data{$sortKey}{recKey} = %{$item_data}{pid};
			$data{$sortKey}{holdId} = %{$holding_data}{holding_id};
			$data{$sortKey}{barcode} = %{$item_data}{barcode};
			my $temp_library = %{$holding_data}{temp_library};
			if ($temp_library->{value}) {
				$data{$sortKey}{subLibCode} = $temp_library->{value};
				$data{$sortKey}{subLibName} = $temp_library->{desc};
			} else {
				my $library = %{$item_data}{library};
				$data{$sortKey}{subLibCode} = $library->{value};
				$data{$sortKey}{subLibName} = $library->{desc};
			}
			my $temp_location = %{$holding_data}{temp_location};
			if ($temp_location->{value}) {
				$data{$sortKey}{collCode} = $temp_location->{value};
				$data{$sortKey}{collName} = $temp_location->{desc};
			} else {
				my $location = %{$item_data}{location};
				$data{$sortKey}{collCode} = $location->{value};
				$data{$sortKey}{collName} = $location->{desc};
			}
			# 'base_status' => {'desc' => 'Item in place','value' => '1'}
			$data{$sortKey}{tmp_base_status} = %{$item_data}{base_status};
			# 'policy' => {'value' => '01','desc' => 'Standard Loan'}
			$data{$sortKey}{tmp_policy} = %{$item_data}{policy};
			# 'due_date_policy' => 'End of Academic Year 2024'
			$data{$sortKey}{tmp_due_date_policy} = %{$item_data}{due_date_policy};
			$data{$sortKey}{description}  = $description;
			my $callNumber = %{$holding_data}{call_number};
			if ($data{$sortKey}{subLibCode} =~ m/SCL|ARCH/) {
				$hasSCL = 1;
				$data{$sortKey}{callNumber}  = $callNumber;
			} else {
				$data{$sortKey}{callNumber}  = $callNumber . ' ' . %{$item_data}{description};
			}
			if ($callNumber =~ m/^[A-Z]{1,3}[0-9]{1,4}\.?([0-9]{1,2})?\ ?\.?[A-Z]{1}[0-9]{1,}.?\ ?([A-Z]{1,2}[1-9]{1,})?\ ?([0-9]{1,})?/) {
				$data{$sortKey}{mapCallNumber}  = $callNumber;
			} else {
				$data{$sortKey}{mapCallNumber}  = "";
			}
			
			$data{$sortKey}{tmpEnumeration_a} = %{$item_data}{enumeration_a};
			$data{$sortKey}{tmp_awaiting_reshelving} = %{$item_data}{awaiting_reshelving};
			my $process_type = %{$item_data}{process_type};
			$data{$sortKey}{tmp_process_type} = $process_type->{value};
			if (length($data{$sortKey}{description})) {
				$data{recInfo}{itmCntDesc}++;
			}
			# $dsl->debug( "holding_data holding_suppress_from_publishing " . %{$holding_data}{holding_suppress_from_publishing});
			if (%{$holding_data}{holding_suppress_from_publishing} eq 'true') {
				$data{$sortKey}{display}  = 'N';
			} else {
				$data{$sortKey}{display}  = 'Y';
			}
		}
	} else {
		$hasMore = 0;
	}
	$data{recInfo}{totalRecCnt} = $totalRecCnt;
	$data{recInfo}{cItmCnt} =  $cItmCnt;
	$data{recInfo}{hasMore} =  $hasMore;

	# $dsl->debug( "data " . Dumper(%data));

	foreach my $lv1 (sort keys %data) {
		my $sublib = '';
		my $due_date_policy = '';
		my $base_status_code = '';
		my $base_status_desc = '';
		my $policy_code = '';
		my $policy_desc = '';
		my $getReqOptions = 'Y';
		my $hold = 'N';
		my $digitization = 'N';

		if ( $lv1 eq 'recInfo') {
			next;
		}
		# 	# monographic set?
		if (length($data{$lv1}{tmpEnumeration_a}) > 0 && $data{recInfo}{multiVol} eq 'N') {
			$data{recInfo}{multiVol} = 'Y';
		}

		if ($data{recInfo}{multiVol}  =~ m/N|U/) {
			$data{recInfo}{illiadLink} = $illiadLinkBib;
		}

		for my $lv2 (sort keys %{$data{$lv1}}) {
			if ($lv2 eq 'subLibCode') {
				$sublib = $data{$lv1}{$lv2};
			}
			if ($lv2 eq 'tmp_base_status') {
				my $base_status = $data{$lv1}{$lv2};
				$base_status_code = $base_status->{value};
				$base_status_desc = $base_status->{desc};
            }
			if ($lv2 eq 'tmp_due_date_policy') {
				$due_date_policy = $data{$lv1}{$lv2};
			}
			if ($lv2 eq 'tmp_policy') {
				my $policy = $data{$lv1}{$lv2};
				$policy_code = $policy->{value};
				$policy_desc = $policy->{desc};
			}
		}

		if ($data{$lv1}{'subLibCode'} =~ m/LSC/) {
			$data{recInfo}{itmCntLSC}++;
		} 
		if ($data{$lv1}{'subLibCode'} =~ m/SCL|ARCH/) {
			# $dsl->debug( "policy info " . $data{$lv1}{subLibCode} . " " . $data{$lv1}{barcode} . " getReqOpt: " . $getReqOptions . 
			# " base_status_code: " . $base_status_code . " (" . $base_status_desc .") policy_code: " . $policy_code ." (" . $policy_desc . 
			# ") due_date_policy: " . $due_date_policy . " tmp_process_type: " . $data{$lv1}{'tmp_process_type'});
			if ($policy_code =~ m/02/) {
				$data{$lv1}{'availStatus'} = 'Access Restricted';
				$data{$lv1}{'requestType'} = 'none';
				$data{$lv1}{'itemAvail'} = 'N';
			} elsif ($data{$lv1}{'tmp_process_type'} eq 'ACQ') {
				$data{$lv1}{'availStatus'} = 'On Order';
				$data{$lv1}{'requestType'} = 'none';
				$data{$lv1}{'itemAvail'} = 'N';
			} elsif ($data{$lv1}{'tmp_process_type'} eq 'TECHNICAL') {
				$data{$lv1}{'availStatus'} = 'Unavailable';
				$data{$lv1}{'requestType'} = 'none';
				$data{$lv1}{'itemAvail'} = 'N';
			} elsif ($data{$lv1}{'tmp_process_type'} eq 'WORK_ORDER_DEPARTMENT') {
				$data{$lv1}{'availStatus'} = 'In Process';
				$data{$lv1}{'requestType'} = 'none';
				$data{$lv1}{'itemAvail'} = 'N';
			} elsif ($data{$lv1}{'tmp_process_type'} eq 'MISSING') {
				$data{$lv1}{'availStatus'} = 'Lost';
				$data{$lv1}{'requestType'} = 'none';
				$data{$lv1}{'itemAvail'} = 'N';
			} else {
				$data{$lv1}{'availStatus'} = 'Available - Library Use Only';
				if ($maintenance_mode) {
					$data{$lv1}{'requestType'} = 'none';
				} else {
					$data{$lv1}{'requestType'} = 'aeon';
				}
				$data{recInfo}{itmCntAeon}++;
				$data{$lv1}{'itemAvail'} = 'Y';
			}
			$getReqOptions = 'N';
		} elsif ($largeSet) {
			if ($base_status_code eq 1) {
				if ($data{$lv1}{subLibCode} eq 'LILLY') {
					$data{$lv1}{'availStatus'} = 'Unavailable during Renovation';
					$data{$lv1}{'requestType'} = 'none';
					$data{$lv1}{'itemAvail'} = 'N';
					$getReqOptions = 'N';
				} else {
					if ($borStatus =~ m/01|02|03|04|06|09|10|18|24|25|27|28|44/) {
						$data{$lv1}{'availStatus'} = 'Available';
						$data{$lv1}{'requestType'} = 'alma-request';
					} elsif ($userPaging eq 'Y') {
						$data{$lv1}{'availStatus'} = 'Available';
						$data{$lv1}{'requestType'} = 'alma-request';
					} else {
						$data{$lv1}{'availStatus'} = 'Available';
						$data{$lv1}{'requestType'} = 'map';
					}
					$getReqOptions = 'N';
					if ($userDigitization =~ m/DUL|MCL|FORD|LAW/) {
						$digitization = 'Y';

					}
					$data{$lv1}{'digitization'} = $digitization;
					if ($data{$lv1}{'tmp_awaiting_reshelving'} ) {
						$data{$lv1}{'availStatus'} = 'Reshelving';
					}
					$data{$lv1}{'itemAvail'} = 'Y';
				}
			}
			if ($policy_code =~ m/02|03/ && $base_status_code eq 1) {
				$data{$lv1}{'availStatus'} = 'Available - Library Use Only';
				$data{$lv1}{'requestType'} = 'none';
				$getReqOptions = 'N';
				$data{$lv1}{'itemAvail'} = 'Y';
			}
			if ($policy_code =~ m/08/ && $base_status_code eq 1) {
				$data{$lv1}{'availStatus'} = 'Available - Reading Room Use Only';
				$data{$lv1}{'requestType'} = 'none';
				$getReqOptions = 'N';
				$data{$lv1}{'itemAvail'} = 'Y';
			}
		} else {
			if ($policy_code =~ m/02|03/ && $base_status_code eq 1) {
				$data{$lv1}{'availStatus'} = 'Available - Library Use Only';
				$data{$lv1}{'requestType'} = 'none';
				$getReqOptions = 'N';
				$data{$lv1}{'itemAvail'} = 'Y';
			}
			if ($policy_code =~ m/08/ && $base_status_code eq 1) {
				$data{$lv1}{'availStatus'} = 'Available - Reading Room Use Only';
				$data{$lv1}{'requestType'} = 'none';
				$getReqOptions = 'N';
				$data{$lv1}{'itemAvail'} = 'Y';
			}
			if ($userState eq 'D') {
				$data{$lv1}{'requestType'} = 'login';
				if ($base_status_code eq 1) {
					$data{$lv1}{'availStatus'} = 'Available';
					$data{$lv1}{'itemAvail'} = 'Y';
					if ($data{$lv1}{'tmp_awaiting_reshelving'} ) {
						$data{$lv1}{'availStatus'} = 'Reshelving';
					}
				}
				if ($base_status_code eq 0) {
					$data{$lv1}{'availStatus'} = 'Checked Out';
					if ($data{$lv1}{'tmp_process_type'} eq 'HOLDSHELF') {
						$data{$lv1}{'availStatus'} = 'On hold';
					}
					if ($data{$lv1}{'tmp_process_type'} =~ m/TRANSIT|TRANSIT_TO_REMOTE_STORAGE/) {
						$data{$lv1}{'availStatus'} = 'In Transit';
					}
					if ($data{$lv1}{'tmp_process_type'} =~ m/LOST_LOAN|LOST_AND_PAID/) {
						$data{$lv1}{'availStatus'} = 'Lost';
					}
					if ($data{$lv1}{'tmp_process_type'} eq 'CLAIM_RETURNED_LOAN') {
						$data{$lv1}{'availStatus'} = 'Unavailable';
					}
					if ($data{$lv1}{'tmp_process_type'} eq 'ACQ') {
						$data{$lv1}{'availStatus'} = 'On Order';
					}
					if ($data{$lv1}{'tmp_process_type'} =~ m/WORK_ORDER_DEPARTMENT|TECHNICAL/) {
						if ($data{$lv1}{subLibCode} eq 'LILLY') {
							$data{$lv1}{'availStatus'} = 'Unavailable during Renovation';
							$data{$lv1}{'requestType'} = 'none';
						} else {
							$data{$lv1}{'availStatus'} = 'In Process';
						}
					}
					$data{$lv1}{'itemAvail'} = 'N';
				}
				$getReqOptions = 'N';
			}
			$data{recInfo}{itmCntAlma}++;
		}
		
		if ($data{$lv1}{display} eq 'N') {
			$getReqOptions = 'N';
		}
		if ($maintenance_mode) {
			if ($data{$lv1}{'subLibCode'} =~ m/SCL|ARCH/) {
				$data{$lv1}{'requestType'} = 'none';
			} else {
				if ($base_status_code eq 1) {
					$data{$lv1}{'availStatus'} = 'Available';
					$data{$lv1}{'requestType'} = 'map';
					$getReqOptions = 'N';
				}
			}
		}
		# $dsl->debug( "policy info " . $data{$lv1}{subLibCode} . " " . $data{$lv1}{barcode} . " getReqOpt: " . $getReqOptions . 
		# 	" base_status_code: " . $base_status_code . " (" . $base_status_desc .") policy_code: " . $policy_code ." (" . $policy_desc . 
		# 	") due_date_policy: " . $due_date_policy . " tmp_process_type: " . $data{$lv1}{'tmp_process_type'});
        if ($getReqOptions eq 'Y') {
			# https://api-na.hosted.exlibrisgroup.com/almaws/v1/bibs/990000123450108501/holdings/2277836270008501/items/2377836260008501/request-options?user_id=jaf41"
			my $urlReqOptions = $api_uri . sprintf("bibs/%s/holdings/%s/items/%s/request-options?user_id=%s", $mmsid, $data{$lv1}{holdId}, $data{$lv1}{recKey}, $userid);
			# if (length($part_of_mmsid)) {
			# 	$urlReqOptions = $api_uri . sprintf("bibs/%s/holdings/%s/items/%s/request-options?user_id=%s", $part_of_mmsid, $data{$lv1}{holdId}, $data{$lv1}{recKey}, $userid);
			# }
            # $dsl->debug($urlReqOptions);
            # get item info
            my $alma_response = `curl -sS -X GET "${urlReqOptions}" -H "Authorization: apikey ${api_key}" -H "accept: application/json"`;
            # $dsl->debug( "ReqOptions json " . $alma_response);
            my $reqOptionsDataRef = decode_json $alma_response;
            # $dsl->debug( "ReqOptions " . Dumper($reqOptionsDataRef));
			my %reqOptionsData = %$reqOptionsDataRef;
			if (exists $reqOptionsData{request_option}) {
				my $reqOpt = $reqOptionsData{request_option};
				foreach my $request (@{$reqOpt}) {
					my $reqType = %{$request}{type};
					if ($reqType->{value} eq 'HOLD') {
						$hold = 'Y';
                    }
					if ($reqType->{value} eq 'DIGITIZATION') {
						$digitization = 'Y';
                    }
				}
			}
			# $dsl->debug( "request types " . $data{$lv1}{subLibCode} . " " . $data{$lv1}{barcode} . " hold: " . $hold . " digitization: " . $digitization);
			
			$data{$lv1}{'digitization'} = $digitization;
        
			
			# if $getReqOptions eq 'Y' continue logic here, if $getReqOptions eq 'N' put the logic above.

			# used to create rules: https://gitlab.oit.duke.edu/aleph/home/-/blob/23_prod/home-slash-aleph/crs/create_rules.pl?ref_type=heads
			# $data{$sortKey}{display}  = 'Y';

			# availStatus => displays Item Status 
			# 'Available','Available - Library Use Only','Ask at Circulation Desk','Lost/Missing','Not Available','On Order',
			# 'Being Repaired','suppressed','In Transit','On Exhibit','Checked Out','Access Restricted'

			# itemAvail = 'Y' => base_status_code 1, 'N' => base_status_code 0

			# requestType = "none"; enum: 'aeon', 'alma-recall', 'alma-request', 'ill', 'map', 'login' or msg 'You have this on loan.', 'You have a hold on this.'

			if ($base_status_code eq 1) {
				if ($data{$lv1}{subLibCode} eq 'LILLY') {
					$data{$lv1}{'availStatus'} = 'Unavailable during Renovation';
					$data{$lv1}{'requestType'} = 'none';
					$data{$lv1}{'itemAvail'} = 'N';
				} else {
					if ($hold eq 'Y') {
						$data{$lv1}{'availStatus'} = 'Available';
						$data{$lv1}{'requestType'} = 'alma-request';
						$data{$lv1}{'itemAvail'} = 'Y';
					}
					if ($hold eq 'N') {
						$data{$lv1}{'availStatus'} = 'Available';
						$data{$lv1}{'requestType'} = 'map';
						$data{$lv1}{'itemAvail'} = 'Y';
					}
					if ($data{$lv1}{'tmp_awaiting_reshelving'} ) {
						$data{$lv1}{'availStatus'} = 'Reshelving';
						$data{$lv1}{'itemAvail'} = 'Y';
					}
				}
			}
			if ($base_status_code eq 0) {
				if ($hold eq 'Y') {
					$data{$lv1}{'availStatus'} = 'Checked Out';
					if ($maintenance_mode) {
						$data{$lv1}{'requestType'} = 'none';
					} else {
						$data{$lv1}{'requestType'} = 'alma-recall';
					}
				}
				if ($hold eq 'N') {
					# $dsl->debug("requestType none line 928");
					$data{$lv1}{'availStatus'} = 'Checked Out';
					$data{$lv1}{'requestType'} = 'none';
				}
				if ($data{$lv1}{'tmp_process_type'} eq 'HOLDSHELF') {
					$data{$lv1}{'availStatus'} = 'On hold';
				}
				if ($data{$lv1}{'tmp_process_type'} =~ m/TRANSIT|TRANSIT_TO_REMOTE_STORAGE/) {
					$data{$lv1}{'availStatus'} = 'In Transit';
				}
				if ($data{$lv1}{'tmp_process_type'} =~ m/LOST_LOAN|LOST_AND_PAID/) {
					$data{$lv1}{'availStatus'} = 'Lost';
				}
				if ($data{$lv1}{'tmp_process_type'} eq 'CLAIM_RETURNED_LOAN') {
					$data{$lv1}{'availStatus'} = 'Unavailable';
				}
				if ($data{$lv1}{'tmp_process_type'} eq 'ACQ') {
					$data{$lv1}{'availStatus'} = 'On Order';
				}
				if ($data{$lv1}{'tmp_process_type'} =~ m/WORK_ORDER_DEPARTMENT|TECHNICAL/) {
					if ($data{$lv1}{subLibCode} eq 'LILLY') {
						$data{$lv1}{'availStatus'} = 'Unavailable during Renovation';
						$data{$lv1}{'requestType'} = 'none';
					} else {
						$data{$lv1}{'availStatus'} = 'In Process';
					}
				}
				$data{$lv1}{'itemAvail'} = 'N';
			}
		}
		

		# 	if ($data{$lv1}{'requestType'} eq 'ill') {
		# 		# TODO add item data

		# 		$data{$lv1}{'illiadLink'} = sprintf("%s&LoanEdition=%s", $illiadLinkBib, $data{$lv1}{'description'} );
		# 	}

		# 	if ($debug) {
		# 		$data{'testing'}{$lv1} = $testResponse;
		# 	}
	} # end foreach my $lv1 (sort keys %data)

	# # userState Y=> has id, D=> no id, but has SCL/ARCH, N=> no id, but should
	# if (!$userid) {
	# 	if ($hasSCL) {
	# 		$data{recInfo}{userState} = 'D';
	# 	} else {
	# 		$data{recInfo}{userState} = 'N';
	# 	}
	# }

	$data{recInfo}{itmCntDspTotal} = $data{recInfo}{itmCntAeon} + $data{recInfo}{itmCntOther} + $data{recInfo}{itmCntAlma};

	if ($data{recInfo}{multiVol} eq "Y") {
		my %enumCnt = ();
		my %enumDesc = ();
		# sort by lib/enum a and rekey
		foreach my $lv1 (sort keys %data) {
			my $enum = "";
			my $sortLib = 'D';
			if ($data{$lv1}{'subLibCode'} =~ m/SCL|ARCH/) {
				$sortLib = 'R';
			} elsif ($data{$lv1}{'subLibCode'} =~ m/DKU/) {
				$sortLib = 'K';
			}
			$enum = $data{$lv1}{'tmpEnumeration_a'};
			if ($enum =~ m/^\d+$/) {
				if (!exists($enumCnt{$sortLib}{$enum})) {
					$enumCnt{$sortLib}{$enum} = 0;
				} else {
					$enumCnt{$sortLib}{$enum} = ++$enumCnt{$sortLib}{$enum};
				}
				my $enumKey = sprintf("%s%08d", $sortLib, $enum);
				if (!exists($enumDesc{$enumKey})) {
					$enumDesc{$enumKey}{'first'} = $data{$lv1}{'description'};
					if ($data{$lv1}{'requestType'} =~ m/alma-request/) {
						$enumDesc{$enumKey}{'alephAvail'} = 1;
					} else {
						$enumDesc{$enumKey}{'alephAvail'} = 0;
					}
				} else {
					if ($enumDesc{$enumKey}{'first'} ne $data{$lv1}{'description'}) {
						$enumDesc{$enumKey}{'diff'} = 1;
					}
					if ($data{$lv1}{'requestType'} =~ m/alma-request/) {
						$enumDesc{$enumKey}{'alephAvail'} = ++$enumDesc{$enumKey}{'alephAvail'};
					}
				}
				my $newKey = sprintf("%s%04d", $enumKey, $enumCnt{$sortLib}{$enum});
				$data{$newKey} = delete $data{$lv1};
			}
		}
		# $dsl->debug(Dumper(%enumDesc));

		# check for multiple copies
		foreach my $lv1 (sort keys %data) {
			my $enum = substr($lv1, 1, 8);
			if ($enum =~ m/^\d+$/) {
				$enum =~ s/^0*//;
			 	if ($enumCnt{substr($lv1, 0, 1)}{$enum} > 0) {
			 		if (!exists($enumDesc{substr($lv1, 0, 9)}{'diff'})) {
			 			my $avail = $enumDesc{substr($lv1, 0, 9)}{'alephAvail'};
						$data{$lv1}{'tmpMultiCopy'} = $avail;
						if ($avail > 0 && $data{$lv1}{'requestType'} =~ m/alma-recall|ill/) {
							# $dsl->debug("requestType none line 1010");
							$data{$lv1}{'requestType'} = 'none';
						}
					}
			 	}
			}
		}
	} else {
		my $physicallyAvailableItems = $data{recInfo}{itmCntAlma} + $data{recInfo}{itmCntMap};
		foreach my $lv1 (sort keys %data) {
			# Turn off recalls if there are available copies.
			if ($physicallyAvailableItems > 1 && $data{$lv1}{requestType} && $data{$lv1}{requestType} eq 'alma-recall') {
				# $dsl->debug("requestType none line 1022");
				$data{$lv1}{requestType} = 'none';
			}

			# Turn per-item scan requests into per-record requests.
			if (
				($physicallyAvailableItems == 0) &&
				($data{$lv1}{digitization} eq 'Y') &&
				($data{$lv1}{requestType} && $data{$lv1}{requestType} =~ m/alma-recall|none|ill/)
			) {
					$data{$lv1}{digitization} = 'N';
					$data{recInfo}{digitization} = 'Y';
			}
		}
	}
	if ($data{recInfo}{itmCntDesc} > 0) {
		$data{recInfo}{reqType} = 'item';
	} elsif ($data{recInfo}{itmCntDspTotal} == 1) {
		$data{recInfo}{reqType} = 'item';
	} else {
		$data{recInfo}{reqType} = 'title';
	}

	# for testing
	if ($debug) {
		foreach my $lv1 (sort keys %data) {
			for my $lv2 (sort keys %{$data{$lv1}}) {
				if ($lv2 =~ /^tmp/) {
					delete $data{$lv1}{$lv2};
				}
			}
		}
	}

	# $dsl->debug( "get_item_detail END recInfo " . Dumper(%data));
	return %data;
};

register 'make_hold_request' => sub {
	my $dsl = shift;
	my $config = $dsl->app->config;

	# $dsl->debug( Dumper ( $config ) );
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

	my ($bor_id, $reqType, $mmsid, $item_id, $postJson, $pickup_location) = @_;

	my $urlReq = '';
	if ($reqType eq 'title') {
		$urlReq = $api_uri . sprintf("users/%s/requests?mms_id=%s", $bor_id, $mmsid);
	}
	if ($reqType eq 'item') {
		$urlReq = $api_uri . sprintf("users/%s/requests?item_pid=%s", $bor_id, $item_id);
	}
	my $curlCmd = sprintf("curl -sS -X POST \"%s\" -H \"Authorization: apikey %s\" -H \"accept: application/json\" -H \"Content-Type: application/json\" -d '%s'", $urlReq, $api_key, $postJson);
	$dsl->debug($curlCmd);
	# curl -X POST "https://api-na.hosted.exlibrisgroup.com/almaws/v1/users/jaf41/requests?user_id_type=all_unique&mms_id=990000123450108501&allow_same_request=false&apikey=l8xx1f3348cf76e844bfa652ecc37742f7e6" 
	# -H "accept: application/json" -H "Content-Type: application/json" 
	# -d {"pickup_location_type":"LIBRARY","pickup_location_library":"PERKN","mms_id":"990000123450108501","request_type":"HOLD"}
	# my $alma_response = `curl -sS -X POST "${urlReq}" -H "Authorization: apikey ${api_key}" -H "accept: application/json"  -d '${\$postJson}'`;
	my $alma_response = `${curlCmd}`;
	$dsl->debug( "make_hold_request json " . $alma_response);
	my $reqDataRef = decode_json $alma_response;
	$dsl->debug( "reqData " . Dumper($reqDataRef));
	my %reqData = %$reqDataRef;
	my $return = {};
	# log error
	if ($reqData{errorList}) {
		my $error = $reqData{errorList}{error}[0];
		my $errMsg = $error->{errorMessage};
		$dsl->debug("make_hold_request error msg: " . $errMsg);
		my %errData = ();
		$return->{error} = 'Y';
		$errData{reqtype} = $reqType;
		$errData{mmsid} = $mmsid;
		$errData{item_id} = $item_id;
		$errData{user_id} = $bor_id;
		$errData{pickup_location} = $pickup_location;
		$errData{response} = $alma_response;
		my $jsonErr = to_json (\%errData);
		$dsl->debug("make_hold_request error data: " . $jsonErr);
		dscLog($errMsg, $jsonErr, 14);
	} else {
		$return->{error} = 'N';
		$return->{pickup_location} = $reqData{pickup_location};
		$return->{status} = $reqData{request_status};
	}

	
	$return;
};

register 'get_hold_shelfs' => sub {
	my $dsl = shift;
	my $config = $dsl->app->config;
	my $api_uri = $config->{alma_rest_uri};
    my $api_key = $config->{alma_api_key};

    my $urlLib = $api_uri . "conf/libraries";
    # $dsl->debug( "urlLib " . $urlLib);
    my $alma_response = `curl -sS -X GET "${urlLib}" -H "Authorization: apikey ${api_key}" -H "accept: application/json"`;
    # $dsl->debug( "bib_response json " . $alma_response);
    my $libDataRef = decode_json $alma_response;
    # $dsl->debug( "libData " . Dumper($libDataRef));
	my %libData = %$libDataRef;

	my @libArr = ();
	
	my $library =  $libData{library};
	foreach my $libNode (@{$library}) {
		my %libObj = ();
		$libObj{code} = %{$libNode}{code};
		$libObj{name} = %{$libNode}{name};
		my $campus = %{$libNode}{campus};
		my $campusCode = $campus->{value};
		if ($campusCode eq 'GENERALPICKUPLOCATIONS') {
			# $dsl->debug( "libCode: " . $libObj{code} . " libName: " . $libObj{name});
			push @libArr, \%libObj;
		}
	}
	# $dsl->debug( Dumper(@libArr));
	return @libArr;
};

register 'circstatusitm' => sub {
	my $dsl = shift;

	# TODO debug
	my $debug = 0;
	my ($sysid) = @_;

	# since we're calling 'get_item_detail' from outside of Dancer2's
	# route handler, we need to pass the $dsl instance as the first parameter

	# Change sysid to MMS ID
    my $mmsid = "";
    if (length($sysid) eq '9') {
        $mmsid = sprintf("99%s0108501", $sysid);
    } else {
        $mmsid = $sysid;
    }
	my %circData = ();
	my %data = get_item_detail( $dsl, $sysid, $mmsid); # ,'circOnly'

	foreach my $lv1 (sort keys %data) {
		if ($lv1 =~ /recInfo/) {

		} else {
			my $bc = "";
			my $as = "";
			for my $lv2 (sort keys %{$data{$lv1}}) {
				if ($lv2 =~ /barcode/) {
					$bc = $data{$lv1}{$lv2};
				}
				if ($lv2 =~ /availStatus/) {
					$as = $data{$lv1}{$lv2};
				}
			}
			if ($bc ne "" && $as ne "") {
				$circData{$bc}{label} = $as;
				if ($as =~ /^Available|^Available - Library Use Only|Reshelving|In Transit/i) {
					$circData{$bc}{available} = 'yes';
				} elsif ($as =~ /Lost|Checked Out|Unavailable|Unavailable during Renovation|Access Restricted|On Order|In Process|On hold/i) {
					$circData{$bc}{available} = 'no';
				} else {
					$circData{$bc}{available} = 'other';
				}
			}
		}
	}

	return \%circData;
};

register 'circstatushold' => sub {
	my $dsl = shift;

	# TODO debug
	my $debug = 0;
	my ($sysid) = @_;

	# since we're calling 'get_item_detail' from outside of Dancer2's
	# route handler, we need to pass the $dsl instance as the first parameter

	my %permLoc = ();
	my %tempLoc = ();
	my %tmpLocBC = ();
	my %data = get_item_info( $dsl, $sysid); # ,'circOnly'
	$dsl->debug( Dumper(%data));
	if (not defined($data{bibInfo}->{error})) {
		$dsl->debug("circstatushold json: " . $data{bibInfo}->{holdings});
		my $locRef = decode_json $data{bibInfo}->{holdings};
		my %loc = %$locRef;
		# $dsl->debug( Dumper(%loc));
		foreach my $locCode (sort keys %loc) {
			foreach my $holding (@{$loc{$locCode}{holdings}}) {
				my $avail = $holding->{status};
				if ($holding->{tempLoc} eq 'Y') {
					if ($locCode =~ m/^LILLY/) {
						$tempLoc{$locCode}{label} = 'Unavailable during renovation.';
						$tempLoc{$locCode}{available} = 'no';
					} else {
						if ($avail =~ m/check_holdings/) {
							$tempLoc{$locCode}{label} = 'Check Holdings.';
						} else {
							$tempLoc{$locCode}{label} = ucfirst($avail) . '.';
						}
						if ($avail =~ /^available/i) {
							$tempLoc{$locCode}{available} = 'yes';
						} elsif ($avail =~ /unavailable/i) {
							$tempLoc{$locCode}{available} = 'no';
						} else {
							$tempLoc{$locCode}{available} = 'other';
						}
					}
				} else {
					my $holdId = $holding->{id};
					if ($locCode =~ m/^LILLY/) {
						$permLoc{$holdId}{label} = 'Unavailable during renovation.';
						$permLoc{$holdId}{available} = 'no';
					} else {
						if ($avail =~ m/check_holdings/) {
							$permLoc{$holdId}{label} = 'Check Holdings.';
						} else {
							$permLoc{$holdId}{label} = ucfirst($avail) . '.';
						}
						if ($avail =~ /^available/i) {
							$permLoc{$holdId}{available} = 'yes';
						} elsif ($avail =~ /unavailable/i) {
							$permLoc{$holdId}{available} = 'no';
						} else {
							$permLoc{$holdId}{available} = 'other';
						}
					}
				}
			}
		}
		if (keys %tempLoc > 0) {
			# $dsl->debug(Dumper(%tempLoc));
			# Change sysid to MMS ID
			my $mmsid = "";
			if (length($sysid) eq '9') {
				$mmsid = sprintf("99%s0108501", $sysid);
			} else {
				$mmsid = $sysid;
			}

			my %itmData = get_item_detail( $dsl, $sysid, $mmsid); # ,'circOnly'
			foreach my $lv1 (sort keys %itmData) {
				if ( $lv1 eq 'recInfo') {
					next;
				}
				my %itm = %{$itmData{$lv1}};
				my $locCode = sprintf("%s-%s", $itm{subLibCode}, $itm{collCode});
				my $tl = $tempLoc{$locCode};
				if (defined($tl)) {
                    # $dsl->debug(Dumper(%itm));
					my $barcode = $itm{barcode};
					$tmpLocBC{$barcode}{available} = $tempLoc{$locCode}{available};
					$tmpLocBC{$barcode}{label} = $tempLoc{$locCode}{label};
					$tmpLocBC{$barcode}{holdId} = $itm{holdId};
					$tmpLocBC{$barcode}{libCode} = $itm{subLibCode};
					$tmpLocBC{$barcode}{libName} = $itm{subLibName};
					$tmpLocBC{$barcode}{collCode} = $itm{collCode};
					$tmpLocBC{$barcode}{collName} = $itm{collName};
                }
			}
		}
	}
	my %circData = ();
	$circData{permLoc} = \%permLoc;
	$circData{tempLoc} = \%tmpLocBC;
	return \%circData;
};


register_plugin;

1;
