package Dancer2::Plugin::DUL::DscLogging;

use strict;
use warnings;
use DateTime;
use Dancer2::Plugin;
use Data::Dumper;
use JSON;
use Net::Amazon::DynamoDB;

plugin_keywords qw/dscLog/;

register 'dscLog' => sub {
	my ($dsl, $message, $data, $days) = @_;

    my $access_key = 'AKIA3UBBEMDLA737AJE3';
    my $secret_key = '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';
    # my $host = 'dynamodb.us-east-1.amazonaws.com';
    my $tableName = 'alma-integrations-logging';
    my $appName = 'CRS';

    my $ddb = Net::Amazon::DynamoDB->new(
        access_key => $access_key,
        secret_key => $secret_key,
        tables     => {
            $tableName => {
                hash_key   => 'app',
                range_key  => 'timestamp',
                attributes => {
                    app       => 'S',
                    timestamp => 'S',
                    message   => 'S',
                    data      => 'S',
                    ttl       => 'N',
                }
            }
        }
    );

    $ddb->exists_table( $tableName) || $ddb->create_table( $tableName, 1, 1 );

    my $timestamp = DateTime->now->strftime('%Y-%m-%dT%H:%M:%S');
    my $epoc = time();
    $ddb->put_item( $tableName => {
        app         => $appName,
        timestamp       => $timestamp,
        message => $message,
        data => $data,
        ttl  => $epoc + ($days * 24 * 60 * 60),
    } ) or die $ddb->error;
};

register_plugin;

1;