package CRS::api;

use strict;
use warnings;

use Dancer2;
use Dancer2::Plugin::DUL::Alma;
#use CRS::api;

set serializer => 'JSON'; # Dancer2::Serializer::JSON

# prefix '/api';

get '/circstatusitm/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');
	return circstatusitm($sysid);
};

get '/circstatushold/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');
	return circstatushold($sysid);
};
