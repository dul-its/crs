package CRS::myaccount;

use strict;
use warnings;
use Carp;
use Data::Dumper;

use Dancer2 appname => 'CRS';
use Dancer2::Plugin::DUL::Alma;
use Dancer2::Plugin::Ajax;
use JSON qw//;

prefix '/myaccount';

get '' => sub {
	if (!session('user')) {
		info "detected expired session";
		redirect "/authenticate";
	}
	forward '/account';
};

prefix '/account';

get '' => sub {
	if (!session('user')) {
		info "detected expired session";
		#redirect '/authenticate';
		redirect '/';
	}

	debug "[/account] Attempting to get borrower session info...";
	my $bor_id = session('user');
	if (session('impersonateuser')) {
	    $bor_id = session('impersonateuser');
	}
	my $o = bor_info($bor_id);
	debug "###### [/account] Done getting borrower info";
	
	var page_title => 'My Library Account';
	session showlastBtn => 1;

	template 'myaccount', $o;
};

post '/cancel-hold-requests' => sub {
	my $user = session('user');
	if (!$user) {
		info "detected expired session";
		redirect '/authenticate';
	}
	if (session('impersonateuser')) {
	    $user = session('impersonateuser');
	}
	debug '/cancel-hold-requests recKeys: ' . body_parameters->get('rec_keys');
	my @rec_keys = split(/,/, body_parameters->get('rec_keys'));
	my $responses = [];
	my $replyCnt = 0;

	foreach my $rec_key (@rec_keys) {
		debug '/cancel-hold-requests rec_key: ' . $rec_key;
		my $r = cancel_hold_request(rec_key =>$rec_key, bor_id => $user);
		$replyCnt++ if $r->{reply};
		push @{ $responses }, { type => 'error', text => $r->{error} } if $r->{error};
	}
	my $msg = $replyCnt . " request(s) canceled.";
	push @{ $responses }, { type => 'reply', text => $msg } if $replyCnt > 0;
	encode_json( { responses => $responses } );
};

post '/renew-items' => sub {
	my $user = session('user');
	if (!$user) {
		info "detected expired session";
		redirect '/authenticate';
	}
	if (session('impersonateuser')) {
	    $user = session('impersonateuser');
	}
	debug '/renew-items recKeys: ' . body_parameters->get('rec_keys');
	my @rec_keys = split(/,/, body_parameters->get('rec_keys'));
	my $responses = [];
	my $replyCnt = 0;

	foreach (@rec_keys) {
		my $r = renew_item( rec_key => $_, bor_id => $user );
		# debug '/renew-items error: ' . $r->{error} ;
		$replyCnt++ if $r->{reply};
		push @{ $responses }, { type => 'error', text => $r->{error} } if $r->{error};
	}
	my $msg = $replyCnt . " item(s) renewed.";
	push @{ $responses }, { type => 'reply', text => $msg } if $replyCnt > 0;
	encode_json( { responses => $responses } );
};

get '/fines' => sub {
	if (!session('user')) {
		info "detected expired session";
		redirect '/';
	}
	my $total = 0;
	$total = query_parameters->get('total');
	my $bor_id = session('user');
	if (session('impersonateuser')) {
	    $bor_id = session('impersonateuser');
	}
	my $o = bor_fines($bor_id);
	my @fines = @{$o->{fines}};
	my %retData = ();
	$retData{fines} = \@fines;
	$retData{total_fines} = $total;
	return JSON::to_json(\%retData);
	# template 'fines', { fines => $o->{fines}, fines_summary => $o->{fines_summary}, total_fines => $total };
};

get '/loans' => sub {
	if (!session('user')) {
		info "detected expired session";
		redirect '/';
	}

	my $bor_id = session('user');
	if (session('impersonateuser')) {
	    $bor_id = session('impersonateuser');
	}
	
	my $cItmCnt = query_parameters->get('cItmCnt');
	my $renewableCnt = query_parameters->get('renewableCnt');
	debug 'start loans cItmCnt: ' . $cItmCnt . ' renewableCnt: ' . $renewableCnt;
	my $o = bor_loans($bor_id, $cItmCnt, $renewableCnt);
	# debug "loans: " . Dumper($o);
	my @loan_items = @{$o->{loan_items}};
	$cItmCnt = $o->{cItmCnt};
	$renewableCnt = $o->{renewable_cnt};
	my $hasMore = $o->{hasMore};
    debug 'before template cItmCnt: ' . $cItmCnt . ' renewableCnt: ' . $renewableCnt . 'hasMore: ' . $hasMore;
	my %retData = ();
	$retData{loanItems} = \@loan_items;
	$retData{renewableCnt} = $renewableCnt;
	$retData{hasMore} = $hasMore;
	$retData{userid} = $bor_id;
	$retData{cItmCnt} = $cItmCnt;
	return JSON::to_json(\%retData);
};

get '/holds' => sub {
	if (!session('user')) {
		info "detected expired session";
		redirect '/';
	}

	my $bor_id = session('user');
	if (session('impersonateuser')) {
	    $bor_id = session('impersonateuser');
	}
	my $o = bor_holds($bor_id);
	my @hold_items = @{$o->{hold_items}};
	my %retData = ();
	$retData{holds} = \@hold_items;
	return JSON::to_json(\%retData);;
	# template 'holds', { hold_items => $o->{hold_items}, bor_id => $bor_id }, { layout => undef };
};
dance;
