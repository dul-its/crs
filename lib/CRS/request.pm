package CRS::request;

use strict;
use warnings;
use Carp;
use Data::Dumper;

use Dancer2 appname => 'CRS';
use Dancer2::Plugin::DUL::Alma;
use Dancer2::Plugin::Ajax;

require LWP::UserAgent;
use XML::Simple;
use XML::LibXML;
use URI::Escape;
use Cwd qw(cwd);

prefix '/request';

post '/test' => sub {
	my $sysid = body_parameters->get('sysid');
	$sysid =~ s/DUKE//;
	$sysid = sprintf( '%09s', $sysid );
	my $userid = body_parameters->get('userid');
	my $testResponse = "";
	if ($sysid eq '000000000') {
		if (length($userid)) {
			my $data = bor_info($userid);
			$testResponse .= Dumper($data);
		}
	} else {
		my %data = get_item_info($sysid, $userid);
		foreach my $lv1 (sort keys %data) {
			$testResponse .= "$lv1:\n";
			for my $lv2 (sort keys %{$data{$lv1}}) {
				if ($lv1 eq 'bibInfo' && $lv2 eq 'isn') {
					my $isns = "";
					foreach my $isn (@{$data{$lv1}{$lv2}}) {
						$isns .= $isn . ',';
					}
					$isns =~ s/,+$//;
					$testResponse .= "\t$lv2: $isns \n";
				} else {
					$testResponse .= "\t$lv2: $data{$lv1}{$lv2} \n";
				}
			}
		}
		$testResponse .= "Details:\n";
		my $forDetail = {
			multiVol => , 
			borStatus => , 
			userState => ,
			homeLib => 
		};

		my %detailData = get_item_detail(
			$sysid
			, $userid
			, $data{bibInfo}{multiVol}
			, $data{userInfo}{borStatusCode}
			, $data{userInfo}{userState}
			, $data{userInfo}{homeLib}
			, ""
		);

		foreach my $lv1 (sort keys %detailData) {
			$testResponse .= "\t$lv1:\n";
			for my $lv2 (sort keys %{$detailData{$lv1}}) {
				$testResponse .= "\t\t$lv2: $detailData{$lv1}{$lv2} \n";
			}
		}
	}

	delayed {
		content $testResponse;
		done;
	};
};

get '/confirm/:sysid' => sub {
	my $sysid = route_parameters->get('sysid');
	my $reckeyParm = query_parameters->get('reckey');
	my $userHomeLib = query_parameters->get('userHomeLib');
	my $patron_id = query_parameters->get('pid');
	my $mmsid = query_parameters->get('mmsid');
	my $holdid = query_parameters->get('holdid');
	my $reqType = query_parameters->get('type');
	my $description = query_parameters->get('description');
	my $part_of_mmsid = query_parameters->get('partmmsid');

	my @reckeys = split(/,/, $reckeyParm);

	debug sprintf("GET /confirm/%s type: %s userid: %s mmsid: %s itmid: %s homelib %s description: %s partOf: %s holdid: %s", $sysid, $reqType, $patron_id, $mmsid, $reckeyParm, $userHomeLib, $description, $part_of_mmsid, $holdid);

	my $dir = cwd;
	debug "working dir " . $dir;
	# my $pickLocFile = $dir . "/pickup_locations.json";
	my $pickLocFile = $dir . "/" . config->{pickup_locations_path};
	my @pickup_locations = ();
	if (-e $pickLocFile) {
		# debug 'file exists: ' . $pickLocFile;
		open(FH, "<", $pickLocFile);
		my $jsonFile = decode_json <FH>;
        close(FH);
		@pickup_locations = @$jsonFile
	} else {
		debug 'file does not exists: ' . $pickLocFile;
		@pickup_locations = get_hold_shelfs();
		open(FH, ">", $pickLocFile) or die $!;
		my $jsonFile = encode_json(\@pickup_locations);
		debug $jsonFile;
		print FH $jsonFile;
        close FH;
		if (-e $pickLocFile) {
			debug 'file created: ' . $pickLocFile;
		} else {
			debug 'Somthing went wrong, file does not exists: ' . $pickLocFile;
		}
	}
	
	# debug Dumper(@pickup_locations);

	my $urlLD = sprintf("https://open-na.hosted.exlibrisgroup.com/alma/01DUKE_INST/bibs/%s", $mmsid);
	# debug 'bib json-ld: ' . $urlLD;
	my $alma_response = `curl -sS -X GET "${urlLD}"`;
	my $bibLinkedDataRef = decode_json $alma_response;
    # debug "bibLinkedData " . Dumper($bibLinkedDataRef);
	my %bibLinkedData = %$bibLinkedDataRef;

	my $title = $bibLinkedData{title};
	my $creator = $bibLinkedData{creator};
	my $creatorType = ref $creator;
	my $author = "";
	if ($creatorType eq 'ARRAY') {
		foreach my $creatorItm (@$creator) {
			$author .= $creatorItm->{label} . ", "
		}
		$author =~ s/, $//;
	} else {
		$author = $creator->{label}
	}
	
	my $imprint = sprintf("%s %s %s", $bibLinkedData{place_of_publication}, $bibLinkedData{publisher}, $bibLinkedData{date});
	my $partTitle = "";
	if (length($part_of_mmsid)) {
		my $urlLDPart = sprintf("https://open-na.hosted.exlibrisgroup.com/alma/01DUKE_INST/bibs/%s", $part_of_mmsid);
		# debug 'bib part json-ld: ' . $urlLDPart;
		my $alma_response_part = `curl -sS -X GET "${urlLDPart}"`;
		my $bibPartLinkedDataRef = decode_json $alma_response_part;
		# debug "bibPartLinkedData " . Dumper($bibPartLinkedDataRef);
		my %bibPartLinkedData = %$bibPartLinkedDataRef;
		$partTitle = $bibPartLinkedData{title};
	}

	var page_title => 'Get This Title';
	session title => $title;
	session author => $author;
	session imprint => $imprint;
	template 'request/confirm_request', {
		set_pickup_location => 1,
		title => $title,
		description => $description,
		partTitle => $partTitle,
		author => $author,
		imprint => $imprint,
		pickup_locations => \@pickup_locations,
		# last_interest_date => $last_interest_date,
		userHomeLib => $userHomeLib,
		recKey => $reckeyParm,
		patron_id => $patron_id,
		mmsid => $mmsid,
        reqType => $reqType,
		holdid => $holdid,
	}

};

post '/confirm/:sysid' => sub {
	my $pickup_location = body_parameters->get('pickup-location');
	# my $last_interest_date = body_parameters->get('last-interest-date');
	my $item_id = body_parameters->get('recKey');
	my $patron_id = body_parameters->get('patron_id');
	my $sysid = route_parameters->get('sysid');
	my $partTitle = body_parameters->get('partTitle');
	my $mmsid = body_parameters->get('mmsid');
	my $reqType = body_parameters->get('reqType');
	my $description = body_parameters->get('description');
	my $holdid = body_parameters->get('holdid');

	debug sprintf("POST /confirm/%s userid: %s itmid: %s", $sysid, $patron_id, $item_id);

	my %postData = ();
	$postData{request_type} = "HOLD";
	
	$postData{pickup_location_type} = "LIBRARY";
	$postData{pickup_location_library} = $pickup_location;
	if ($reqType eq 'title') {
		$postData{mms_id} = $mmsid;
	}
	if ($reqType eq 'item') {
		$postData{item_id} = $item_id;
	}

	my $postJson = to_json (\%postData);
	debug $postJson;

	my $response = make_hold_request($patron_id, $reqType, $mmsid, $item_id, $postJson, $pickup_location);

	my $urlLD = sprintf("https://open-na.hosted.exlibrisgroup.com/alma/01DUKE_INST/bibs/%s", $mmsid);
	# debug 'bib json-ld: ' . $urlLD;
	my $alma_response = `curl -sS -X GET "${urlLD}"`;
	my $bibLinkedDataRef = decode_json $alma_response;
    # debug "bibLinkedData " . Dumper($bibLinkedDataRef);
	my %bibLinkedData = %$bibLinkedDataRef;

	my $title = $bibLinkedData{title};
	my $creator = $bibLinkedData{creator};
	my $creatorType = ref $creator;
	my $author = "";
	if ($creatorType eq 'ARRAY') {
		foreach my $creatorItm (@$creator) {
			$author .= $creatorItm->{label} . ", "
		}
		$author =~ s/, $//;
	} else {
		$author = $creator->{label}
	}
	my $imprint = sprintf("%s %s %s", $bibLinkedData{place_of_publication}, $bibLinkedData{publisher}, $bibLinkedData{date});


	if ($response->{error} eq 'N') {
		$response->{system_message} = sprintf("Your hold request is complete.  You may pick up your title at %s", $response->{pickup_location});
		my $reply_text = $response->{status};
		debug sprintf("****** REPLY_TEXT = [%s] *******", $reply_text);
		var page_title => 'Confirmation';
		var page_title_class => 'text-success';
		return template 'request/confirm_request', { 
			display_request_confirmation => 1, 
			title => $title,
			description => $description,
			partTitle => $partTitle,
			author => $author,
			imprint => $imprint,
		};
	} else {
		var page_title => 'Error';
		var page_title_class => 'text-warning';
		return template 'request/confirm_request', { 
			display_request_error => 1,
			title => $title,
			description => $description,
			partTitle => $partTitle,
			author => $author,
			imprint => $imprint,
			reqmsg => "Your request has been received, but failed."
		};
	}

};

# ajax ['post'] => '/' => sub {
# 	# it's safe to assume the sysid is sanitized at this stage
# 	my $sysid = body_parameters->get('sysid');
# 	my $bor_id = session('bor_id');
# 	if (!$bor_id) {
# 		# crap, get out of here..
# 		return encode_json { error => 'invalid borrower_id' };
# 	}

# 	#my $response = { 
# 	#	message => 'Hello World in PERL', 
# 	#	author => 'Derrek Croney', 
# 	#	system_message => 'This is a test of the CRS Hold Request System.  Had this been an actual Hold Request, you would have a book :)',
# 	#};
# 	my $response = make_hold_request($sysid, $bor_id);
# 	$response->{system_message} = sprintf("Your hold request is complete.  You may pick up your title at %s", $response->{pickup_location});
# 	encode_json($response)
# };


1;
