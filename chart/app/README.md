# Location Guide on OKD

## Checklist
* Create Helm chart at `helm-chart/app`
* Create database secret (if using mariadb, postgres, etc)
* Copy `issuer.yaml` and `route.yaml` from working projects (e.g. R2T2)
  * Use `sed` for string replacement, ensuring that `app` appears instead of other project names.
* Verify persistent volume claims, requesting needed PVs from OIT (Nate Childers)
* Complete work on templates
* Create appropriate CNAME record
* Integrate `oauth2` and manage groups
*

Add ALMA API key to secret

```bash
kubectl create secret generic alma-<releaseName> \
  --from-literal=alma-api-key=<key-value>
```bash
helm upgrade --install dev . --set-file proxy.shibboleth.attributeMap=attribute-map.xml -f values-dev.yaml --set image.tag=9665beb8 -f memcached.yaml
```

helm upgrade --install dev . --reuse-values --set image.tag=<most-recent-commit-short-sha>
