#! /bin/sh

set -e

cd /srv/web/crs

/usr/local/bin/starman --daemonize -l 0.0.0.0:4000 \
  -E ${STARMAR_ENV:-development} --workers 5 \
  --pid /var/run/crs.pid \
  -I/srv/web/crs \
  bin/app.psgi

# plackup -p 8080 bin/app.psgi
