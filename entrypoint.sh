#!/bin/bash

set -e

cd /srv/web/crs

starman -l 0.0.0.0:4000 -E ${STARMAN_ENV:-development} --workers 5 \
  --pid /var/run/crs.pid -I/srv/web/crs \
  bin/app.psgi
