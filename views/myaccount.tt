<div class="container">
	<% IF settings.maintenance_mode %>
		<div class="alert alert-warning" role="alert"><%settings.maintenance_mode_msg %></div>
	<% END %>
	<% IF (error.defined) %>
		<div class="alert alert-danger" role="alert">
			<h4><% error %></h4>
			<ul>
				<li>If you need assistance, please contact <a href="mailto:perkins-requests@duke.edu">perkins-requests@duke.edu</a></li>
			</ul>
		</div>
	<% END %>
	<div class="row">
		<div class="col-sm-12 col-md-<% settings.ui.myaccount_view.sidebar_columns %>">
			<div class="well well-sm small">
				<% UNLESS settings.maintenance_mode %>
					<strong>About Your Library Account</strong>
					<ul class="list-unstyled">
						<li><a href="#myprofile">Your Profile</a></li>
						<li><a href="#fines">Fines and fees</a></li>
						<li><a href="#loans">Titles on loan</a></li>
						<li><a href="#holdrequests">Hold/recall requests</a></li>
					</ul>
					<hr />
				<% END %>
				<strong><% settings.ui.myaccount_view.related_library_accounts.heading %></strong>
				<ul class="list-unstyled">
				<% FOREACH l IN settings.ui.myaccount_view.related_library_accounts.links %>
					<li><% INCLUDE 'widgets/link-element.tt' link=l %></li>
				<% END %>
				</ul>
			</div>
		</div>
		<div class="col-md-<% settings.ui.myaccount_view.content_columns %>">
			<% UNLESS error.defined %>
				<a name="myprofile"></a>
				<div>
					<div class="h3">Your Profile</div>
					<div>
						<p class="text-muted">Our library system indicates the following information for you:</p>
					</div>
					<div>
						<dl class="dl-horizontal">
							<dt>Name</dt>
							<dd><% bor_name %></dd>
							<% IF bor_address.length %>
							<dt>Address</dt>
							<dd><% bor_address %></dd>
							<% END %>
							<dt>Email Address</dt>
							<dd><% bor_email %></dd>
							<dt>Phone Number</dt>
							<dd><% bor_phone %></dd>
							<dt>Patron Status</dt>
							<dd><% bor_status %></dd>
							<dt>Patron Type</dt>
							<dd><% bor_patron_type %></dd>
						</dl>
					</div>
				</div>
			<% END %>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<% UNLESS error.defined %>
				<% UNLESS settings.maintenance_mode %>
					<a name="fines"></a>
					<div>
						<div class="h3">Fines and fees</div>
						<div id="fineItemsJson"></div>
						<div class="spinner" id="fineItems" data-total-cash="<% total_cash %>"></div>
					</div>
					<a name="loans"></a>
					<div>
						<div class="h3">Titles on loan</div>
						<% IF total_loans == 0 %>
							<div class="text-muted">You do not have any titles on loan or checked-out at this time.</div>
						<% ELSE %>
							<div class="text-muted">You have <% total_loans %> titles on loan or checked-out.</div>
							<p class="small help-block">Need to renew? Titles that can be renewed have a check box on the left hand side. Please check the box for your title(s) and select "Renew Selected Titles."</p>
							<button class="btn btn-primary btn-sm disabled" id="renewItems" data-url="<% request.uri_base %>/account/renew-items" data-reckey="">
								Renew Selected Title(s)
							</button>

							<div id="loanItemsJson"></div>
							<div id="loanItems" class="itm-progress" data-total-loans="<% total_loans %>" data-ctrl-item-count=0 data-renewable-cnt=0>
								<% IF total_loans > 200 %><div class="text-warning" id="loanMsg">This list may take several minutes to load.</div><% END %>
								<progress id="loanItemsProgress" class="progress itm-progress" max=<% total_loans %> value=0></progress>
								<div id="loanItemsProgressMsg">0 of <% total_loans %></div>
							</div>
						<% END %>
					</div>

					<a name="holdrequests"></a>
					<div>
						<div class="h3">Hold/Recall requests</div>
						<% IF total_requests == 0 %>
							<div class="text-muted">You do not have any hold/recall-requests at this time.</div>
						<% ELSE %>
							<div class="text-muted">You have <% total_requests %> hold/recall-requests at this time.</div>
							<button class="btn btn-primary btn-sm disabled" id="cancelHoldItems"  data-url="<% request.uri_base %>/account/cancel-hold-requests" data-reckey="">
								Cancel Selected Hold/Recall Request(s)
							</button>
							<div id="holdItemsJson"></div>
							<div class="spinner" id="holdItems" data-total-holds="<% total_requests %>"></div>
						<% END %>
					</div>
				<% END %>
			<% END %>
		</div>
	</div>
</div>
<% INCLUDE 'modal/pleasewait.tt' %>
