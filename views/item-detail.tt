<% MACRO requestModalButton(holding, label, requestUrl) BLOCK -%>
	<button
		type="button"
		class="btn btn-sm btn-block btn-primary"
		data-available="<% holding.itemAvail %>"
		data-callNumber="<% holding.callNumber %>"
		data-locationcode="<% holding.collCode %>"
		data-request-label="<% label %>"
		data-request-type="<% holding.requestType %>"
		data-request-url="<% requestUrl %>"
		data-scan-url="<% holding.scanUrl %>"
		data-sublibrary="<% holding.subLibCode %>"
		data-target="#requestModal"
		data-toggle="modal"
	>Request</button>
<% END -%>

<% MACRO mapModalButton(holding) BLOCK -%>
	<button
		type="button"
		class="btn btn-block btn-sm btn-warning"
		data-toggle="modal"
		data-target="#mapModal"
		data-sublibrary="<% holding.subLibCode %>"
		data-locationcode="<% holding.collCode %>"
		data-callNumber="<% holding.mapCallNumber %>"
		data-toggle="tooltip"
		data-placement="left"
		title="This item is on the shelf, but you can't request it. Click to display location guide."
	>Where to find it</button>
<% END -%>

<% IF holdings.size %>
	<% IF (recInfo.cItmCnt <= 30) %>
		<div class="table-responsive" role="table" aria-label="item table">
			<table class="table table-hover" id="item-table">
				<thead>
					<tr class="active">
						<% IF (recInfo.itmCntDesc > 0) %>
							<th><% settings.ui.item_view.table_headers.description %></th>
						<% END %>
						<th><% settings.ui.item_view.table_headers.library %></th>
						<th><% settings.ui.item_view.table_headers.collection %></th>
						<th><% settings.ui.item_view.table_headers.location %></th>
						<% IF (isStaff == 'Y') %>
							<th>Barcode</th>
						<% END %>
						<th><% settings.ui.item_view.table_headers.availability %></th>
						<th><% settings.ui.item_view.table_headers.request_action_label %></th>
					</tr>
				</thead>
				<tbody>
	<% END %>
			<% FOREACH h IN holdings %>
				<% IF (h.display == 'Y') %>
					<tr>
						<% IF (recInfo.itmCntDesc > 0) %><td><% h.description %></td><% END %>
						<td><% h.subLibName %></td>
						<td><% h.collName %></td>
						<td><% h.callNumber %></td>
						<% IF (isStaff == 'Y') %>
							<td><% h.barcode %></td>
						<% END %>
						<td>
							<% IF h.availStatus == 'Access Restricted' %>
								<span style="color:red"><% h.availStatus %></span>
							<% ELSE %>
								<% h.availStatus %>
							<% END %>
						</td>
						<td>
							<% IF (recInfo.userState == 'N') %>
								<a href="#" class="btn btn-sm btn-warning">
									<i class="fa fa-sign-in" aria-hidden="true"></i> Please Sign In
								</a>
							<% ELSIF h.requestType == 'none' %>
								&nbsp;
							<% ELSIF (h.requestType == 'aeon') %>
								<a class="btn btn-sm btn-block btn-primary" href="<% settings.aeon_open_url_base %><% h.aeon_query %>" target="_blank">Request</a>
							<% ELSIF (h.requestType == 'alma-request') %>
								<% IF (recInfo.reqType == 'item') %>
									<% requestUrl = request.uri_base _ '/request/confirm/' _ sysid _ '?pid=' _ userid _ '&userHomeLib=' _ homeLib _ '&reckey=' _  h.recKey _ '&holdid=' _  h.holdId _ '&mmsid=' _ recInfo.mmsid _ '&type=' _ recInfo.reqType _ '&description=' _ h.description _ '&partmmsid=' _ part_of_mmsid %>
								<% ELSE %>
									<% requestUrl = request.uri_base _ '/request/confirm/' _ sysid _ '?pid=' _ userid _ '&userHomeLib=' _ homeLib _ '&reckey=' _  h.recKey _ '&holdid=' _  h.holdId _ '&mmsid=' _ recInfo.mmsid _ '&type=' _ recInfo.reqType _ '&partmmsid=' _ part_of_mmsid %>
								<% END %>
								<% requestModalButton(h, "Request", requestUrl) %>
							<% ELSIF (h.requestType == 'alma-recall') %>
								<% IF show_recall %>
									<% IF (recInfo.reqType == 'item') %>
										<% requestUrl = request.uri_base _ '/request/confirm/' _ sysid _ '?pid=' _ userid _ '&userHomeLib=' _ homeLib _ '&reckey=' _  h.recKey _ '&holdid=' _  h.holdId _ '&mmsid=' _ recInfo.mmsid _ '&type=' _ recInfo.reqType _ '&description=' _ h.description _ '&partmmsid=' _ part_of_mmsid %>
									<% ELSE %>
										<% requestUrl = request.uri_base _ '/request/confirm/' _ sysid _ '?pid=' _ userid _ '&userHomeLib=' _ homeLib _ '&reckey=' _  h.recKey _ '&holdid=' _  h.holdId _ '&mmsid=' _ recInfo.mmsid _ '&type=' _ recInfo.reqType _ '&partmmsid=' _ part_of_mmsid %>
									<% END %>
									<% requestModalButton(h, "Recall", requestUrl) %>
								<% ELSIF h.scan == 'Y' %>
									<% requestModalButton(h, "", "") %>
								<% END %>
							<% ELSIF (h.requestType == 'map') %>
								<% IF h.scan == 'Y' %>
									<% requestModalButton(h, "", "") %>
								<% ELSE %>
									<% mapModalButton(h) %>
								<% END %>
							<% ELSIF (h.requestType == 'login') %>
								<a href="/authenticate" class="btn btn-sm btn-block btn-warning">Login to Request</a>
							<% ELSIF (h.requestType == 'ill') %>
								<% IF show_ill %>
									<a href="<% recInfo.illiadLink %>" class="btn btn-sm btn-primary">Interlibrary Loan Request</a>
								<% END %>
							<% ELSE %>
								<a href="#" class"btn btn-sm btn-warning"><% h.requestType %></a>
							<% END %>
						</td>
					</tr>
				<% END %>
			<% END %>
	<% IF (recInfo.hasMore) %>
		<tr id="moreItemsRow" data-ctrl-item-count=<% recInfo.cItmCnt %>></tr>
	<% END %>
	<% IF (recInfo.cItmCnt <= 30) %>
				</tbody>
			</table>
		</div>
		<% IF (recInfo.hasMore) %>
			<div class="spinner" id="moreItemsDiv"></div>
		<% END %>
	<% END %>
<% END %>
<% IF (settings.maintenance_mode) %>
	<div class="alert alert-info" role="alert">
		<h4>Requesting Items Is Unavailable</h4>
		<ul>
			<li>The Duke Requests System is currently unavailable.</li>
			<li>However, Duke University faculty, staff, and students can place an <a href="https://library.duke.edu/using/interlibrary-requests">Interlibrary Loan Request</a> using the button below.</li>
			<li>If you need assistance, please contact <a href="mailto:perkins-requests@duke.edu">perkins-requests@duke.edu</a></li>
		</ul>
	</div>
	<div class="col-sm-4 col-md-4">
		<a href="<% recInfo.illiadLink %>" class="btn btn-primary">Place Interlibrary Loan Request</a>
	</div>
<% ELSE %>
	<% IF show_ill %>
		<div class="well">
			<div class="row">
				<div class="col-sm-8 col-md-8">
					<p>You can place an Interlibrary Request for this title.<br />
					<small><a href="//library.duke.edu/using/interlibrary-requests" target="_blank">Learn More</a></small></p>
				</div>
				<div class="col-sm-4 col-md-4">
					<a href="<% recInfo.illiadLink %>" class="btn btn-primary">Place Interlibrary Loan Request</a>
				</div>
			</div>
		</div>
	<% END %>
	<% IF recInfo.scan == 'Y' %>
		<div class="well">
			<div class="row">
				<div class="col-sm-9">
					<p>We may be able to get a portion of this item scanned and sent to you electronically, but we can't guarantee that we'll be successful. If you'd like us to try to get a scan for you from another library, click here to request a scan.</p>
				</div>
				<div class="col-sm-3">
					<a href="<% illiadScanUrl %>" class="btn btn-primary">Request Scan<a>
				</div>
			</div>
		</div>
	<% END %>
<% END %>



