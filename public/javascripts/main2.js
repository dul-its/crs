(function($) {

	$(document).ready(function(){
		if ($('#itemDetail').length) {
			var cItmCnt = $('#mainItems').data('ctrl-item-count');
			var mmsid = $('#itemDetail').data('mmsid');
			var holdData = [];
			var reqTypeData = undefined;
			// console.log(`mmsid: ${mmsid} cItmCnt: ${cItmCnt}`);
			if (cItmCnt == 0) {
				var data = $('#itemDetail').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 60000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					const show_ill = resp.show_ill;
					$('#mainItemsJson').data("show_ill", show_ill);
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length;
						// console.log("holdData size: " + holdSize);
						// console.log(holdData);
						$('#mainItemsJson').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#mainItemsJson').data("req-type", recInfo.reqType);
						}
						$('#mainItemsJson').data("cnt-desc", recInfo.itmCntDesc);
						if (recInfo.hasMore) {
							const cItmCnt = recInfo.cItmCnt;
							$('#mainItems').data("ctrl-item-count", cItmCnt);
							$('#itemDetailProgress').attr("max", recInfo.totalRecCnt).attr("value", holdSize);
							$('#itemDetailProgressMsg').text(holdSize + " of " + recInfo.totalRecCnt);
							getItemJson(cItmCnt);
						} else {
							$('#mainItems').replaceWith("<div id='mainItemsTab'></div>");
							displayItems('#mainItems', 'none', 'none');
						}
					} else {
						let holdSize = 0;
						if (recInfo.hasMore) {
							const cItmCnt = recInfo.cItmCnt;
							$('#mainItems').data("ctrl-item-count", cItmCnt);
							$('#itemDetailProgress').attr("max", recInfo.totalRecCnt).attr("value", holdSize);
							$('#itemDetailProgressMsg').text(holdSize + " of " + recInfo.totalRecCnt);
							getItemJson(cItmCnt);
						} else {
							$('#mainItems').empty();
						}
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}

		if ($('#partOf1').length) {
			var cItmCnt = $('#partOf1').data('ctrl-item-count');
			var mmsid = $('#partOf1').data('mmsid');
			if (cItmCnt == 0) {
				var data = $('#partOf1').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 180000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length
						console.log("holdData size: " + holdSize);
						console.log(holdData);
						$('#partOf1Json').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#partOf1Json').data("req-type", recInfo.reqType);
						}
						$('#partOf1Json').data("cnt-desc", recInfo.itmCntDesc);
						$('#partOf1').replaceWith("<div id='partOf1Tab'></div>");
						displayItems('#partOf1', 'none', 'none');
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}

		if ($('#partOf2').length) {
			var cItmCnt = $('#partOf2').data('ctrl-item-count');
			var mmsid = $('#partOf2').data('mmsid');
			if (cItmCnt == 0) {
				var data = $('#partOf2').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 180000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length
						console.log("holdData size: " + holdSize);
						console.log(holdData);
						$('#partOf1Json').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#partOf1Json').data("req-type", recInfo.reqType);
						}
						$('#partOf2Json').data("cnt-desc", recInfo.itmCntDesc);
						$('#partOf2').replaceWith("<div id='partOf2Tab'></div>");
						displayItems('#partOf2', 'none', 'none');
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}

		if ($('#partOf3').length) {
			var cItmCnt = $('#partOf3').data('ctrl-item-count');
			var mmsid = $('#partOf3').data('mmsid');
			if (cItmCnt == 0) {
				var data = $('#partOf3').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 180000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length
						console.log("holdData size: " + holdSize);
						console.log(holdData);
						$('#partOf3Json').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#partOf3Json').data("req-type", recInfo.reqType);
						}
						$('#partOf3Json').data("cnt-desc", recInfo.itmCntDesc);
						$('#partOf3').replaceWith("<div id='partOf3Tab'></div>");
						displayItems('#partOf3', 'none', 'none');
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}

		if ($('#partOf4').length) {
			var cItmCnt = $('#partOf4').data('ctrl-item-count');
			var mmsid = $('#partOf4').data('mmsid');
			if (cItmCnt == 0) {
				var data = $('#partOf4').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 180000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length
						console.log("holdData size: " + holdSize);
						console.log(holdData);
						$('#partOf4Json').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#partOf4Json').data("req-type", recInfo.reqType);
						}
						$('#partOf4Json').data("cnt-desc", recInfo.itmCntDesc);
						$('#partOf4').replaceWith("<div id='partOf4Tab'></div>");
						displayItems('#partOf4', 'none', 'none');
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}

		if ($('#partOf5').length) {
			var cItmCnt = $('#partOf5').data('ctrl-item-count');
			var mmsid = $('#partOf5').data('mmsid');
			if (cItmCnt == 0) {
				var data = $('#partOf5').data('for-detail');
				data.cItmCnt = cItmCnt;
				data.mmsid = mmsid;
				// console.log(data);
				var request = {url: "/item-detail", type:"post", data: data, timeout: 180000};
				$.ajax(request).done(function(response) {
					const resp = JSON.parse(response);
					// console.log(resp);
					const holdJson = resp.holdings;
					const recInfo = resp.recInfo;
					if (holdJson.length > 0) {
						holdJson.forEach(obj => {
							holdData.push(obj);
						});
						let holdSize = holdJson.length
						console.log("holdData size: " + holdSize);
						console.log(holdData);
						$('#partOf5Json').data("hold-json", holdData);
						if (reqTypeData === undefined) {
							$('#partOf5Json').data("req-type", recInfo.reqType);
						}
						$('#partOf5Json').data("cnt-desc", recInfo.itmCntDesc);
						$('#partOf5').replaceWith("<div id='partOf3Tab'></div>");
						displayItems('#partOf5', 'none', 'none');
					}
				}).fail (function(jqxhr, textStatus, errorThrown)  {
					console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
					if (cItmCnt === undefined) {
						cItmCnt = 0;
					}
					if (jqxhr.status === 502) {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
					if (jqxhr.status === 0 && textStatus === "timeout") {
						console.log("Retrying " + cItmCnt);
						getItemJson(cItmCnt);
					}
				});
			}
		}
		
		if ($('#fineItems').length) {
			var cash = $('#fineItems').data('total-cash');
			var cashurl = "/account/fines?total=" + cash;
			$.ajax(cashurl).done(function(response) {
				const resp = JSON.parse(response);
				// console.log(resp);
				var fineData = [];
				const fineJson = resp.fines;
				if (fineJson.length > 0) {
					fineJson.forEach(obj => {
						fineData.push(obj);
					});
					// console.log(fineData);
					$('#fineItemsJson').data("fine-json", fineData);
					$('#fineItemsJson').data("fine-total", resp.total_fines);
					$('#fineItems').replaceWith("<div id='fineItemsTab'></div>");
					displayFines('#fineItems', 'none', 'none');
				} else {
					$('#fineItems').removeClass('spinner');
					$('#fineItems').css('transform', 'none');
				}
			});
		}

		var loanCnt = $('#loanItems').data('total-loans');
		var cItmCnt = $('#loanItems').data('ctrl-item-count');
		if (loanCnt > 0) {
			$.ajax('/account/loans?cItmCnt=' + cItmCnt + '&renewableCnt=0').done(function(response) {
				var loanData = [];
				const resp = JSON.parse(response);
				// console.log(resp);
				const loanJson = resp.loanItems;
				if (loanJson.length > 0) {
					loanJson.forEach(obj => {
						loanData.push(obj);
					});
					let loanSize = loanJson.length;
					// console.log("loanData size: " + loanSize);
					// console.log(loanData);
					$('#loanItemsJson').data("loan-json", loanData);
					$('#loanItemsJson').data("renewable-cnt", resp.renewableCnt);
					if (resp.hasMore) {
						const cItmCnt = resp.cItmCnt;
						$('#loanItems').data("ctrl-item-count", cItmCnt);
						$('#loanItemsProgress').attr("max", loanCnt).attr("value", loanSize);
						$('#loanItemsProgressMsg').text(loanSize + " of " + loanCnt);
						getLoanJson(cItmCnt);
					} else {
						$('#loanItems').replaceWith("<div id='loanItemsTab'></div>");
						displayLoans('#loanItems', 'none', 'none', 'none');
					}
				} else {
					let loanSize = 0;
					if (resp.hasMore) {
						const cItmCnt = resp.cItmCnt;
						$('#loanItems').data("ctrl-item-count", cItmCnt);
						$('#loanItemsProgress').attr("max", loanCnt).attr("value", loanSize);
						$('#loanItemsProgressMsg').text(loanSize + " of " + loanCnt);
						getLoanJson(cItmCnt);
					} else {
						$('#loanItems').empty();
					}
				}
			});
		}

		var holdCnt = $('#holdItems').data('total-holds');
		if (holdCnt > 0) {
			$.ajax("/account/holds").done(function(response) {
				const resp = JSON.parse(response);
				// console.log(resp);
				var holdData = [];
				const holdJson = resp.holds;
				if (holdJson.length > 0) {
					holdJson.forEach(obj => {
						holdData.push(obj);
					});
					// console.log(holdData);
					$('#holdItemsJson').data("hold-json", holdData);
					$('#holdItems').replaceWith("<div id='holdItemsTab'></div>");
					displayHolds('#holdItems', 'none', 'none');
				} else {
					$('#holdItems').removeClass('spinner');
					$('#holdItems').css('transform', 'none');
				}
			});
		}

		$('#itemDetail').on('click', 'tr', function(event) {
	        if (event.target.type !== 'checkbox') {
	            $(':checkbox[name=aleph-items]', this).trigger('click');
	        }
	    });

	    $('#holdItems').on('click', 'tr', function(event) {
	        if (event.target.type !== 'checkbox') {
	            $(':checkbox[name=cancel-hold-item]', this).trigger('click');
	        }
	    });

	    $('#holdItems').on('change', ':checkbox[name=cancel-hold-all]', function(event) {
	    	if(this.checked) {
				$(':checkbox[name=cancel-hold-item]').each(function() {
					if (!this.checked) {
						$(this).trigger('click');
					}
		        });
			} else {
				$(':checkbox[name=cancel-hold-item]').each(function() {
		            if (this.checked) {
						$(this).trigger('click');
					}
		        });
			}
	    });

	}); // end of document ready 

	function getItemJson(cItmCnt) {
		var mmsid = $('#itemDetail').data('mmsid');
		var holdData = $('#mainItemsJson').data('hold-json');
		var reqTypeData = $('#mainItemsJson').data('req-type');
		if (holdData === undefined) {
			holdData = [];
        }
		// console.log(`mmsid: ${mmsid} cItmCnt: ${cItmCnt} reqTypeData: ${reqTypeData} holdData: ${holdData.length}`);
		var data = $('#itemDetail').data('for-detail');
		data.cItmCnt = cItmCnt;
		var request = {url: "/item-detail", type:"post", data: data, timeout: 60000};
		$.ajax(request).done(function(response) {
			const resp = JSON.parse(response);
			// console.log(resp);
			const holdJson = resp.holdings;
			const recInfo = resp.recInfo;
			if (reqTypeData === undefined) {
				$('#mainItemsJson').data("req-type", recInfo.reqType);
			}
			let totalDesc = $('#mainItemsJson').data("cnt-desc") + recInfo.itmCntDesc;
			$('#mainItemsJson').data("cnt-desc", totalDesc);
			const show_ill = resp.show_ill;
			$('#mainItemsJson').data("show_ill", show_ill);
			if (holdJson.length > 0) {
				holdJson.forEach(obj => {
					holdData.push(obj);
				});
				let holdSize = holdData.length
				// console.log("holdData size: " + holdSize);
				// console.log(holdData);
				$('#mainItemsJson').data("hold-json", holdData);
				if (recInfo.hasMore) {
					const cItmCnt = recInfo.cItmCnt;
					$('#mainItems').data("ctrl-item-count", cItmCnt);
					$('#itemDetailProgress').attr("max", recInfo.totalRecCnt).attr("value", holdSize);
					$('#itemDetailProgressMsg').text(holdSize + " of " + recInfo.totalRecCnt);
					getItemJson(cItmCnt);
				} else {
					$('#mainItems').replaceWith("<div id='mainItemsTab'></div>");
					displayItems('#mainItems', 'none', 'none');
				}
			} else {
				let holdSize = 0;
				if (recInfo.hasMore) {
					const cItmCnt = recInfo.cItmCnt;
					$('#mainItems').data("ctrl-item-count", cItmCnt);
					$('#itemDetailProgress').attr("max", recInfo.totalRecCnt).attr("value", holdSize);
					$('#itemDetailProgressMsg').text(holdSize + " of " + recInfo.totalRecCnt);
					getItemJson(cItmCnt);
				}
			}
		}).fail (function(jqxhr, textStatus, errorThrown)  {
			console.log("Error " + jqxhr.status + ": " + textStatus + " : " + errorThrown);
			if (jqxhr.status === 502) {
				console.log("Retrying " + cItmCnt);
				getItemJson(cItmCnt);
			}
			if (jqxhr.status === 0 && textStatus === "timeout") {
				console.log("Retrying " + cItmCnt);
				getItemJson(cItmCnt);
			}
		});
	}

	function displayItems(mainDiv, sortBy, groupBy) {
		const jsonDiv = mainDiv + "Json";
		const tabDiv = mainDiv + "Tab";
		const itmJsonOrig = $(jsonDiv).data("hold-json");
		const reqType = $(jsonDiv).data("req-type");
		const forDetail = $('div#itemDetail').data("for-detail");
		const mmsid = $('div#itemDetail').data("mmsid");
		const libraries = $('div#itemDetail').data("libraries");
		const totalDesc = $(jsonDiv).data("cnt-desc");
		const itabClass = "itab itab--collapse table-hover";
        // console.log(`jsonDiv: ${jsonDiv} mmsid: ${mmsid}, totalDesc: ${totalDesc}, reqType: ${reqType}, sortBy: ${sortBy}, groupBy: ${groupBy}`);
		const libCount = Object.keys(libraries).length;
		const isStaff = forDetail.isStaff;
		const show_ill = $(jsonDiv).data("show_ill");
		const title = $('a#title').text();
		// console.log(`show_ill: ${show_ill}`);
		let itmJson = JSON.parse(JSON.stringify(itmJsonOrig));
		// console.log(itmJson);
		let sortBynext = '';
		if (sortBy === 'none') {
            sortBynext = 'descA';
        }
		if (sortBy === 'descA') {
            sortBynext = 'descD';
        }
		if (sortBy === 'descD') {
            sortBynext = 'none';
        }
		let groupBynext = '';
		if (groupBy === 'none') {
            groupBynext = 'library';
        }
		if (groupBy === 'library') {
            groupBynext = 'none';
        }

		if (sortBy === "descA") {
			itmJson.sort((a, b) => {
				const iA = a.description.toUpperCase();
				const iB = b.description.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "descD") {
			itmJson.sort((a, b) => {
				const iA = a.description.toUpperCase();
				const iB = b.description.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		$(tabDiv).empty();
		$(tabDiv).removeClass();

		let sortIcon = "<i class='fa fa-sort'></i>";
		if (sortBynext === "descD") {
			sortIcon = "<i class='fa fa-sort-up'></i>";
		}
		if (sortBynext === "descA") {
            sortIcon = "<i class='fa fa-sort-down'></i>";
        }

		if (groupBy === "library") {
			if (totalDesc > 0) {
				$(tabDiv).append(`<div class='itab-row-header'>
					<div class='itab-cell desc-cell-dl'>
						<div class='itab-cell--content'><button id='sortDescBtn' class='btn btn-sm btn-primary'>Description ${sortIcon}</button></div>
					</div>
					<div class='itab-cell desc-cell-dl'>
						<div class='itab-cell--content'><button id='groupLibBtn' class='btn btn-sm btn-primary'>Library <i class='fa fa-caret-up'></i></button></div>
					</div>
				</div>`);
			} else {
				$(tabDiv).append(`<div class='itab-row-header'>
					<div class='itab-cell desc-cell-dl'>
						<div class='itab-cell--content'><button id='groupLibBtn' class='btn btn-sm btn-primary'>Library <i class='fa fa-caret-up'></i></button></div>
					</div>
				</div>`);
			}
			if (totalDesc > 0) {
				const sortDescBtn = document.getElementById("sortDescBtn");
				sortDescBtn.addEventListener("click", function() {
					displayItems(mainDiv, sortBynext, groupBy);
				});
			}
			const groupLibBtn = document.getElementById("groupLibBtn");
			groupLibBtn.addEventListener("click", function() {
				displayItems(mainDiv, sortBy, groupBynext);
			});
			for (const [libCode, value] of Object.entries(libraries)) {
				const libIdCnt = `lib-${libCode}-cnt`;
				const libIdCollapse = `lib-${libCode}-collapse`;
				let libDiv = $(`<div id='lib-${libCode}' class="accordion-item">
					<h3 class="accordion-header">
						<a role="button" data-bs-toggle="collapse" data-bs-parent="${libIdCollapse}" href="#${libIdCollapse}" aria-expanded="false">
							<i class="fa fa-chevron-down"></i><i class="fa fa-chevron-right"></i>&nbsp;&nbsp;${value.name}&nbsp;&nbsp;(<span id="${libIdCnt}"></span>)
						</a>
					</h3>
				</div>`);
				let libDivCollapse = $(`<div id="${libIdCollapse}" class="accordion-collapse collapse"></div>`);
				let libDivItm = $(`<div class="accordion-body ${itabClass}"></div>`);
				// <div class='itab-cell lib-cell-d'>
				// 		<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
				// 		<div class='itab-cell--content'>Library <button id='groupLibBtn' class='btn btn-sm btn-primary'><i class='fa fa-object-group'></i></button></div>
				// 	</div>
				if (totalDesc > 0) {
					if (isStaff == 'Y') {
						$(libDivItm).append(`<div class='itab-row--head itab-row-header'>
							<div class='itab-cell desc-cell-dbl'>
								<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Description</div>
							</div>
							<div class='itab-cell coll-cell-dbl'>
								<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Collection</div>
							</div>
							<div class='itab-cell loc-cell-dbl'>
								<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Location</div>
							</div>
							<div class='itab-cell bc-cell-dbl'>
								<div class='itab-cell--heading'><i class='fa fa-barcode' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Barcode</div>
							</div>
							<div class='itab-cell status-cell-dbl'>
								<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Item Status</div>
							</div>
							<div class='itab-cell request-cell'>Request</div>
						</div>`);
					} else {
						$(libDivItm).append(`<div class='itab-row--head itab-row-header'>
							<div class='itab-cell desc-cell-dl'>
								<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Description</div>
							</div>
							<div class='itab-cell coll-cell-dl'>
								<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Collection</div>
							</div>
							<div class='itab-cell loc-cell-dl'>
								<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Location</div>
							</div>
							<div class='itab-cell status-cell-dl'>
								<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Item Status</div>
							</div>
							<div class='itab-cell request-cell-dl'>Request</div>
						</div>`);
					}
				} else {
					if (isStaff == 'Y') {
						$(libDivItm).append(`<div class='itab-row--head itab-row-header'>
							<div class='itab-cell coll-cell-bl'>
								<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Collection</div>
							</div>
							<div class='itab-cell loc-cell-bl'>
								<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Location</div>
							</div>
							<div class='itab-cell bc-cell-bl'>
								<div class='itab-cell--heading'><i class='fa fa-barcode' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Barcode</div>
							</div>
							<div class='itab-cell status-cell-bl'>
								<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Item Status</div>
							</div>
							<div class='itab-cell request-cell-bl'>Request</div>
						</div>`);
					} else {
						$(libDivItm).append(`<div class='itab-row--head itab-row-header'>
							<div class='itab-cell coll-cell-l'>
								<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Collection</div>
							</div>
							<div class='itab-cell loc-cell-l'>
								<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Location</div>
							</div>
							<div class='itab-cell status-cell-l'>
								<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
								<div class='itab-cell--content'>Item Status</div>
							</div>
							<div class='itab-cell request-cell-l'>Request</div>
						</div>`);
					}
					// $(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
					// 	<div class='itab-cell desc-cell-d'>
					// 		<div class='itab-cell--content'>Library <button id='groupLibBtnResp' class='btn btn-sm btn-primary'><i class='fa fa-object-group'></i></button></div>
					// 	</div>
					// </div>`);
				}
				
				let itmCntLib = 0;
				itmJson.forEach(itm => {
					if (itm.subLibCode == libCode) {
						if (itm.display == 'Y') {
							let statusHtml = '';
							let requestHtml = '';
							// <div class='itab-cell lib-cell-db'>
							// 		<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library'></i></div>
							// 		<div class='itab-cell--content'>${itm.subLibName}</div>
							// 	</div>
							let item;
							if (totalDesc > 0) {
								if (isStaff == 'Y') {
									statusHtml = getStatusHtml(itm, 'status-cell-dbl');
									requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-dbl');
									item = $(`<div class='itab-row'>
										<div class='itab-cell desc-cell-dbl'>
											<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-label='Description' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Description"></i></div>
											<div class='itab-cell--content'>${itm.description}</div>
										</div>
										<div class='itab-cell coll-cell-dbl'>
											<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
											<div class='itab-cell--content'>${itm.collName}</div>
										</div>
										<div class='itab-cell loc-cell-dbl'>
											<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
											<div class='itab-cell--content'>${itm.callNumber}</div>
										</div>
										<div class='itab-cell bc-cell-dbl'>
											<div class='itab-cell--heading'><i class='fa fa-barcode' aria-label='Barcode' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Barcode"></i></div>
											<div class='itab-cell--content'>${itm.barcode}</div>
										</div>
										${statusHtml}
										${requestHtml}
									</div>`);
								} else {
									statusHtml = getStatusHtml(itm, 'status-cell-dl');
									requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-dl');
									item = $(`<div class='itab-row'>
										<div class='itab-cell desc-cell-dl'>
											<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-label='Description' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Description"></i></div>
											<div class='itab-cell--content'>${itm.description}</div>
										</div>
										<div class='itab-cell coll-cell-dl'>
											<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
											<div class='itab-cell--content'>${itm.collName}</div>
										</div>
										<div class='itab-cell loc-cell-dl'>
											<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
											<div class='itab-cell--content'>${itm.callNumber}</div>
										</div>
										${statusHtml}
										${requestHtml}
									</div>`);
								}
							} else {
								if (isStaff == 'Y') {
									statusHtml = getStatusHtml(itm, 'status-cell-bl');
									requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-bl');
									item = $(`<div class='itab-row'>
										<div class='itab-cell coll-cell-bl'>
											<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
											<div class='itab-cell--content'>${itm.collName}</div>
										</div>
										<div class='itab-cell loc-cell-bl'>
											<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
											<div class='itab-cell--content'>${itm.callNumber}</div>
										</div>
										<div class='itab-cell bc-cell-bl'>
											<div class='itab-cell--heading'><i class='fa fa-barcode' aria-label='Barcode' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Barcode"></i></div>
											<div class='itab-cell--content'>${itm.barcode}</div>
										</div>
										${statusHtml}
										${requestHtml}
									</div>`);
								} else {
									statusHtml = getStatusHtml(itm, 'status-cell-l');
									requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-l');
									item = $(`<div class='itab-row'>
										<div class='itab-cell coll-cell-l'>
											<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
											<div class='itab-cell--content'>${itm.collName}</div>
										</div>
										<div class='itab-cell loc-cell-l'>
											<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
											<div class='itab-cell--content'>${itm.callNumber}</div>
										</div>
										${statusHtml}
										${requestHtml}
									</div>`);
								}
							}
							itmCntLib++;
							$(libDivItm).append(item);
						}
					}
				});
				$(libDivCollapse).append(libDivItm);
				$(libDiv).append(libDivCollapse);
				$(tabDiv).addClass("accordion");
				$(tabDiv).append(libDiv);
				const libSpanCnt = document.getElementById(libIdCnt);
				libSpanCnt.innerText = itmCntLib;
			}
		} else {
			let libraryBtn = "Library";
			let libraryDivResp = ""	
			if (libCount > 1) {
				libraryBtn = "<button id='groupLibBtn' class='btn btn-sm btn-primary'>Library <i class='fa fa-caret-down'></i></button>";
				libraryDivResp = "<div class='itab-cell--content'><button id='groupLibBtnResp' class='btn btn-sm btn-primary'>Library <i class='fa fa-caret-down'></i></button></div>";
            }
			if (totalDesc > 0) {
				if (isStaff == 'Y') {
					$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
						<div class='itab-cell desc-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'><button id='sortDescBtn' class='btn btn-sm btn-primary'>Description ${sortIcon}</button></div>
						</div>
						<div class='itab-cell lib-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>${libraryBtn}</div>
						</div>
						<div class='itab-cell coll-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Collection</div>
						</div>
						<div class='itab-cell loc-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Location</div>
						</div>
						<div class='itab-cell bc-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-barcode' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Barcode</div>
						</div>
						<div class='itab-cell status-cell-db'>
							<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Item Status</div>
						</div>
						<div class='itab-cell request-cell-db'>Request</div>
					</div>`);
				} else {
					$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
						<div class='itab-cell desc-cell-d'>
							<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'><button id='sortDescBtn' class='btn btn-sm btn-primary'>Description ${sortIcon}</button></div>
						</div>
						<div class='itab-cell lib-cell-d'>
							<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>${libraryBtn}</div>
						</div>
						<div class='itab-cell coll-cell-d'>
							<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Collection</div>
						</div>
						<div class='itab-cell loc-cell-d'>
							<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Location</div>
						</div>
						<div class='itab-cell status-cell-d'>
							<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Item Status</div>
						</div>
						<div class='itab-cell request-cell-d'>Request</div>
					</div>`);
				}
				$(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
					<div class='itab-cell desc-cell-d'>
						<div class='itab-cell--content'><button id='sortDescBtnResp' class='btn btn-sm btn-primary'>Description ${sortIcon}</button></div>
						${libraryDivResp}
					</div>
				</div>`);
			
				const sortDescBtn = document.getElementById("sortDescBtn");
				sortDescBtn.addEventListener("click", function() {
					displayItems(mainDiv, sortBynext, groupBy);
				});
				const sortDescBtnResp = document.getElementById("sortDescBtnResp");
				sortDescBtnResp.addEventListener("click", function() {
					displayItems(mainDiv, sortBynext, groupBy);
				});
			} else {
				if (isStaff == 'Y') {
					$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
						<div class='itab-cell lib-cell-b'>
							<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>${libraryBtn}</div>
						</div>
						<div class='itab-cell coll-cell-b'>
							<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Collection</div>
						</div>
						<div class='itab-cell loc-cell-b'>
							<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Location</div>
						</div>
						<div class='itab-cell bc-cell-b'>
							<div class='itab-cell--heading'><i class='fa fa-barcode' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Barcode</div>
						</div>
						<div class='itab-cell status-cell-b'>
							<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Item Status</div>
						</div>
						<div class='itab-cell request-cell-b'>Request</div>
					</div>`);
				} else {
					$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
						<div class='itab-cell lib-cell'>
							<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>${libraryBtn}</div>
						</div>
						<div class='itab-cell coll-cell'>
							<div class='itab-cell--heading'><i class='fa fa-archive' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Collection</div>
						</div>
						<div class='itab-cell loc-cell'>
							<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Location</div>
						</div>
						<div class='itab-cell status-cell'>
							<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
							<div class='itab-cell--content'>Item Status</div>
						</div>
						<div class='itab-cell request-cell'>Request</div>
					</div>`);
				}
				$(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
					<div class='itab-cell desc-cell-d'>
						${libraryDivResp}
					</div>
				</div>`);
			}
			if (libCount > 1) {
				const groupLibBtn = document.getElementById("groupLibBtn");
				groupLibBtn.addEventListener("click", function() {
					displayItems(mainDiv, sortBy, groupBynext);
				});
				const groupLibBtnResp = document.getElementById("groupLibBtnResp");
				groupLibBtnResp.addEventListener("click", function() {
					displayItems(mainDiv, sortBy, groupBynext);
				});
			}

			itmJson.forEach(itm => {
				if (itm.display == 'Y') {
					let statusHtml = '';
					let requestHtml = '';

					let item;
					if (totalDesc > 0) {
						if (isStaff == 'Y') {
							statusHtml = getStatusHtml(itm, 'status-cell-db');
							requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-db');
							item = $(`<div class='itab-row'>
								<div class='itab-cell desc-cell-db'>
									<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-label='Description' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Description"></i></div>
									<div class='itab-cell--content'>${itm.description}</div>
								</div>
								<div class='itab-cell lib-cell-db'>
									<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Library"></i></div>
									<div class='itab-cell--content'>${itm.subLibName}</div>
								</div>
								<div class='itab-cell coll-cell-db'>
									<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
									<div class='itab-cell--content'>${itm.collName}</div>
								</div>
								<div class='itab-cell loc-cell-db'>
									<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
									<div class='itab-cell--content'>${itm.callNumber}</div>
								</div>
								<div class='itab-cell bc-cell-db'>
									<div class='itab-cell--heading'><i class='fa fa-barcode' aria-label='Barcode' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Barcode"></i></div>
									<div class='itab-cell--content'>${itm.barcode}</div>
								</div>
								${statusHtml}
								${requestHtml}
							</div>`);
						} else {
							statusHtml = getStatusHtml(itm, 'status-cell-d');
							requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-d');
							item = $(`<div class='itab-row'>
								<div class='itab-cell desc-cell-d'>
									<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-label='Description' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Description"></i></div>
									<div class='itab-cell--content'>${itm.description}</div>
								</div>
								<div class='itab-cell lib-cell-d'>
									<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Library"></i></div>
									<div class='itab-cell--content'>${itm.subLibName}</div>
								</div>
								<div class='itab-cell coll-cell-d'>
									<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
									<div class='itab-cell--content'>${itm.collName}</div>
								</div>
								<div class='itab-cell loc-cell-d'>
									<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
									<div class='itab-cell--content'>${itm.callNumber}</div>
								</div>
								${statusHtml}
								${requestHtml}
							</div>`);
						}
					} else {
						if (isStaff == 'Y') {
							statusHtml = getStatusHtml(itm, 'status-cell-b');
							requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell-b');
							item = $(`<div class='itab-row'>
								<div class='itab-cell lib-cell-b'>
									<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Library"></i></div>
									<div class='itab-cell--content'>${itm.subLibName}</div>
								</div>
								<div class='itab-cell coll-cell-b'>
									<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
									<div class='itab-cell--content'>${itm.collName}</div>
								</div>
								<div class='itab-cell loc-cell-b'>
									<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
									<div class='itab-cell--content'>${itm.callNumber}</div>
								</div>
								<div class='itab-cell bc-cell-b'>
									<div class='itab-cell--heading'><i class='fa fa-barcode' aria-label='Barcode' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Barcode"></i></div>
									<div class='itab-cell--content'>${itm.barcode}</div>
								</div>
								${statusHtml}
								${requestHtml}
							</div>`);
						} else {
							statusHtml = getStatusHtml(itm, 'status-cell');
							requestHtml = getRequestHtml(itm, forDetail, reqType, mmsid, title, 'request-cell');
							item = $(`<div class='itab-row'>
								<div class='itab-cell lib-cell'>
									<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Library"></i></div>
									<div class='itab-cell--content'>${itm.subLibName}</div>
								</div>
								<div class='itab-cell coll-cell'>
									<div class='itab-cell--heading'><i class='fa fa-archive' aria-label='Collection' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Collection"></i></div>
									<div class='itab-cell--content'>${itm.collName}</div>
								</div>
								<div class='itab-cell loc-cell'>
									<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Location"></i></div>
									<div class='itab-cell--content'>${itm.callNumber}</div>
								</div>
								${statusHtml}
								${requestHtml}
							</div>`);
						}
					}
					$(tabDiv).addClass(itabClass);
					$(tabDiv).append(item);
				}
			})
		}
		const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
		const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
		if (show_ill === 1) {
			const illDiv = $(`<div class="well">
			<div>
				<div class="btn-right">
					<a href="${forDetail.illiadLinkBib}" class="btn btn-primary">Place Interlibrary Loan Request</a>
				</div>
				<div>
					<p>You can place an Interlibrary Request for this title.<br />
					<small><a href="//library.duke.edu/using/interlibrary-requests" target="_blank">Learn More</a></small></p>
				</div>
			</div>
		</div>`);
		$('#itemDetail').append(illDiv);
		}
		// $('div#mainItemsRow').replaceWith("<pre>" + JSON.stringify(itmJson, null, 2) + "</pre>");
	}

	function displayLoans(mainDiv, sortBy, sortDir) {
		const jsonDiv = mainDiv + "Json";
		const tabDiv = mainDiv + "Tab";
		const itmJsonOrig = $(jsonDiv).data("loan-json");
		const itabClass = "itab itab--collapse table-hover";
        // console.log(`jsonDiv: ${jsonDiv}, sortBy: ${sortBy}, sortDir: ${sortDir}`);
		// console.log(forDetail);
		
		let itmJson = JSON.parse(JSON.stringify(itmJsonOrig));
		// console.log(itmJson);
		let sortDirNext = '';
		if (sortDir === 'none') {
            sortDirNext = 'asc';
        }
		if (sortDir === 'asc') {
            sortDirNext = 'desc';
        }
		if (sortDir === 'desc') {
            sortDirNext = 'asc';
        }


		if (sortBy === "author" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.author.toUpperCase();
				const iB = b.author.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "author" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.author.toUpperCase();
				const iB = b.author.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "title" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "title" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "duedate" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const [dayA, monthA, yearA] = a.due_date.split('/').map(Number);
    			const [dayB, monthB, yearB] = b.due_date.split('/').map(Number);
				const iA = new Date(yearA, monthA - 1, dayA);
				const iB = new Date(yearB, monthB - 1, dayB)
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "duedate" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const [dayA, monthA, yearA] = a.due_date.split('/').map(Number);
    			const [dayB, monthB, yearB] = b.due_date.split('/').map(Number);
				const iA = new Date(yearA, monthA - 1, dayA);
				const iB = new Date(yearB, monthB - 1, dayB)
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		$(tabDiv).empty();
		$(tabDiv).removeClass();

		$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
			<div class='itab-cell l-renew-cell'>
				<div class='itab-cell--heading'><i class='fa fa-check' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><input type='checkbox' name='renew-all'></div>
			</div>
			<div class='itab-cell l-author-cell'>
				<div class='itab-cell--heading'><i class='fa fa-user' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='sortAuthorBtn' class='btn btn-sm btn-primary'>Author <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell l-title-cell'>
				<div class='itab-cell--heading'><i class='fa fa-book' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='sortTitleBtn' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell l-year-cell'>
				<div class='itab-cell--heading'><i class='fa fa-calendar-o' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Year</div>
			</div>
			<div class='itab-cell l-lib-cell'>
				<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Library </div>
			</div>
			<div class='itab-cell l-loc-cell'>
				<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Call Number</div>
			</div>
			<div class='itab-cell l-due-cell'>
				<div class='itab-cell--heading'><i class='fa fa-calendar' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='sortDueDateBtn' class='btn btn-sm btn-primary'>Due Date <i class='fa fa-sort'></i></button></div>
			</div>
		</div>`);
		
		$(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='sortAuthorBtnResp' class='btn btn-sm btn-primary'>Author <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='sortTitleBtnResp' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='sortDueDateBtnResp' class='btn btn-sm btn-primary'>Due Date <i class='fa fa-sort'></i></button></div>
			</div>
		</div>`);
		
		const sortAuthorBtn = document.getElementById("sortAuthorBtn");
		sortAuthorBtn.addEventListener("click", function() {
			displayLoans(mainDiv, 'author', sortDirNext);
		});
		const sortTitleBtn = document.getElementById("sortTitleBtn");
		sortTitleBtn.addEventListener("click", function() {
			displayLoans(mainDiv, 'title', sortDirNext);
		});
		const sortDueDateBtn = document.getElementById("sortDueDateBtn");
		sortDueDateBtn.addEventListener("click", function() {
			displayLoans(mainDiv, 'duedate', sortDirNext);
		});
		const sortAuthorBtnResp = document.getElementById("sortAuthorBtnResp");
		sortAuthorBtnResp.addEventListener("click", function() {
			displayLoans(mainDiv, 'author', sortDirNext, );
		});
		const sortTitleBtnResp = document.getElementById("sortTitleBtnResp");
		sortTitleBtnResp.addEventListener("click", function() {
			displayLoans(mainDiv, 'title', sortDirNext);
		});
		const sortDueDateBtnResp = document.getElementById("sortDueDateBtnResp");
		sortDueDateBtnResp.addEventListener("click", function() {
			displayLoans(mainDiv, 'duedate', sortDirNext);
		});

		$('#loanItemsTab').on('click', 'tr', function(event) {
			if (event.target.type !== 'checkbox') {
				$(':checkbox[name=renew-items]', this).trigger('click');
			}
		});

		$('#loanItemsTab').on('change', ':checkbox[name=renew-all]', function(event) {
			if(this.checked) {
				$(':checkbox[name=renew-items]').each(function() {
					if (!this.checked) {
						$(this).trigger('click');
					}
				});
			} else {
				$(':checkbox[name=renew-items]').each(function() {
					if (this.checked) {
						$(this).trigger('click');
					}
				});
			}
		});

		$('#loanItemsTab').on('change', ':checkbox[name=renew-items]', function() {
			var reqa = $('#renewItems');
			var newk = reqa.attr('data-reckey');
			var rkey = $(this).attr('data-reckey');
			if (this.checked) {
				if (newk === '') {
					reqa.attr('data-reckey', rkey)
					reqa.removeClass('disabled');
				} else {
					reqa.attr('data-reckey', newk + ',' + rkey)
					newk = newk.replace(',' + rkey, '');
				}
			} else {
				newk = newk.replace(',' + rkey, '');
				newk = newk.replace(rkey + ',', '');
				newk = newk.replace(rkey, '');
				reqa.attr('data-reckey', newk)
				if (newk === '') {
					reqa.addClass('disabled');
				}
			}
		});

		$('#renewItems').on('click', function() {
			var url = $(this).attr('data-url');
			if (url.length == 0 || url == null) {
				return;
			}
			var recKeys = $(this).attr('data-reckey');
			var data = {rec_keys: recKeys}
			// fire up the request modal popup
			$('#requestProgressModal')
				.find('.modal-title')
					.text( 'Renew Titles' )
				.end()
				.find('.btn-close')
					.hide()
				.end()
				.modal('show');
	
			console.log(url + ' : ' + data);
	
			$.ajax( url, {
				data : data,
				type : 'POST',
				dataType : 'json',
				cache : false,
				success : function(data, txtStat, o) {
	
					$('.btn-close').show();
					$('.btn-close-refresh', '#requestProgressModal')
						.prop('disabled', '').html("Close &amp; Refresh")
						.on('click', function() {
							window.location.reload();
						});
					$('.progress', '#requestProgressModal').hide();
					for (var i = 0; i < data.responses.length; i++) {
						alert_type = data.responses[i].type == 'error' ? 'alert-danger' : 'alert-success';
						$p = $('<p class="alert"></p>').addClass(alert_type).html(data.responses[i].text);
						$('.response-message', '#requestProgressModal').append( $p );
					}
					$('.response-message').show();
					//$('#requestProgressModal').modal('hide');
					//window.location.reload();
				},
				error : function(o, txtStat, err) {
					$('#requestProgressModal').modal('hide');
				}
			});
		});

		itmJson.forEach(itm => {
			let item;
			let renewCheck = "&nbsp;";
			if (itm.renewals === "Y") {
				renewCheck = `<input type="checkbox" name="renew-items" data-reckey="${itm.rec_key}" />`;
			}
			// <div class='itab-cell--heading'><i class='fa fa-check' aria-label='Renewals'></i></div>
			item = $(`<div class='itab-row-cb'>
				<div class='itab-cell l-renew-cell'>
					${renewCheck}
				</div>
				<div class='itab-cell l-author-cell'>
					<div class='itab-cell--heading'><i class='fa fa-user' aria-label='Author'></i></div>
					<div class='itab-cell--content'>${itm.author}</div>
				</div>
				<div class='itab-cell l-title-cell'>
					<div class='itab-cell--heading'><i class='fa fa-book' aria-label='Title'></i></div>
					<div class='itab-cell--content'>${itm.title}</div>
				</div>
				<div class='itab-cell l-year-cell'>
					<div class='itab-cell--heading'><i class='fa fa-calendar-o' aria-label='Publication Year'></i></div>
					<div class='itab-cell--content'>${itm.year}</div>
				</div>
				<div class='itab-cell l-lib-cell'>
					<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library'></i></div>
					<div class='itab-cell--content'>${itm.sublibrary}</div>
				</div>
				<div class='itab-cell l-loc-cell'>
					<div class='itab-cell--heading'><i class='fa fa-location-arrow' aria-label='Location'></i></div>
					<div class='itab-cell--content'>${itm.call_no}</div>
				</div>
				<div class='itab-cell l-due-cell'>
					<div class='itab-cell--heading'><i class='fa fa-calendar' aria-label='Due Date'></i></div>
					<div class='itab-cell--content'>${itm.due_date}</div>
				</div>
			</div>`);
			
			$(tabDiv).addClass(itabClass);
			$(tabDiv).append(item);
		})
		
	}

	function displayFines(mainDiv, sortBy, sortDir) {
		const jsonDiv = mainDiv + "Json";
		const tabDiv = mainDiv + "Tab";
		const itmJsonOrig = $(jsonDiv).data("fine-json");
		const fineTotal = $(jsonDiv).data("fine-total");
		const itabClass = "itab itab--collapse table-hover";
		// console.log(`jsonDiv: ${jsonDiv}, sortBy: ${sortBy}, sortDir: ${sortDir}`);

		let itmJson = JSON.parse(JSON.stringify(itmJsonOrig));
		// console.log(itmJson);
		let sortDirNext = '';
		if (sortDir === 'none') {
            sortDirNext = 'asc';
        }
		if (sortDir === 'asc') {
            sortDirNext = 'desc';
        }
		if (sortDir === 'desc') {
            sortDirNext = 'asc';
        }

		if (sortBy === "title" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "title" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "library" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.sublibrary.toUpperCase();
				const iB = b.sublibrary.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "library" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.sublibrary.toUpperCase();
				const iB = b.sublibrary.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "date" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.transaction_date.toUpperCase();
				const iB = b.transaction_date.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "date" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.transaction_date.toUpperCase();
				const iB = b.transaction_date.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		$(tabDiv).empty();
		$(tabDiv).removeClass();

		$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
			<div class='itab-cell f-lib-cell'>
				<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='fineSortLibraryBtn' class='btn btn-sm btn-primary'>Library <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell f-title-cell'>
				<div class='itab-cell--heading'><i class='fa fa-book' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='fineSortTitleBtn' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell f-date-cell'>
				<div class='itab-cell--heading'><i class='fa fa-calendar' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='fineSortDateBtn' class='btn btn-sm btn-primary'>Date <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell f-desc-cell'>
				<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Description</div>
			</div>
			<div class='itab-cell f-credit-cell'>
				<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Credit</div>
			</div>
			<div class='itab-cell f-debit-cell'>
				<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Debit</div>
			</div>
			<div class='itab-cell f-cost-cell'>
				<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Cost</div>
			</div>
			<div class='itab-cell f-status-cell'>
				<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Status</div>
			</div>
		</div>`);
		$(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='fineSortLibraryBtnResp' class='btn btn-sm btn-primary'>Library <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='fineSortTitleBtnResp' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='fineSortDateBtnResp' class='btn btn-sm btn-primary'>Date <i class='fa fa-sort'></i></button></div>
			</div>
		</div>`);

		itmJson.forEach(itm => {
			let item;
			let credit = "&nbsp;";
			let debit = "&nbsp;";
			let cost = "";
			if (itm.credit_debit === "Debit") {
				debit = itm.fine_net_sum;
				cost = "+ " + itm.fine_net_sum;
			}
			if (itm.credit_debit === "Credit") {
				credit = itm.fine_net_sum;
				cost = "- " + itm.fine_net_sum;
			}

			item = $(`<div class='itab-row'>
				<div class='itab-cell f-lib-cell'>
					<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Library'></i></div>
					<div class='itab-cell--content'>${itm.sublibrary}</div>
				</div>
				<div class='itab-cell f-title-cell'>
					<div class='itab-cell--heading'><i class='fa fa-book' aria-label='Title'></i></div>
					<div class='itab-cell--content'>${itm.title}</div>
				</div>
				<div class='itab-cell f-date-cell'>
					<div class='itab-cell--heading'><i class='fa fa-calendar' aria-label='Date'></i></div>
					<div class='itab-cell--content'>${itm.transaction_date}</div>
				</div>
				<div class='itab-cell f-desc-cell'>
					<div class='itab-cell--heading'><i class='fa fa-info-circle' aria-label='Description'></i></div>
					<div class='itab-cell--content'>${itm.transaction_desc}</div>
				</div>
				<div class='itab-cell f-credit-cell'>
					<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-label='Credit'></i></div>
					<div class='itab-cell--content'>${credit}</div>
				</div>
				<div class='itab-cell f-debit-cell'>
					<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-label='Debit'></i></div>
					<div class='itab-cell--content'>${debit}</div>
				</div>
				<div class='itab-cell f-cost-cell'>
					<div class='itab-cell--heading'><i class='fa fa-dollar-sign' aria-label='Cost'></i></div>
					<div class='itab-cell--content'>${cost}</div>
				</div>
				<div class='itab-cell f-status-cell'>
					<div class='itab-cell--heading'><i class='fa fa-circle' aria-label='Status'></i></div>
					<div class='itab-cell--content'>${itm.status}</div>
				</div>
			</div>`);
			
			$(tabDiv).addClass(itabClass);
			$(tabDiv).append(item);
		})

		// const fineSpanCnt = document.getElementById("fine-total");
		// fineSpanCnt.innerText = "$" + Number(fineTotal).toFixed(2);
		// &nbsp;&nbsp;(<span id="fine-total"></span>)

		const fineSortLibraryBtn = document.getElementById("fineSortLibraryBtn");
		fineSortLibraryBtn.addEventListener("click", function() {
			displayFines(mainDiv, 'library', sortDirNext);
		});
		const fineSortLibraryBtnResp = document.getElementById("fineSortLibraryBtnResp");
		fineSortLibraryBtnResp.addEventListener("click", function() {
			displayFines(mainDiv, 'library', sortDirNext);
		});
		const fineSortTitleBtn = document.getElementById("fineSortTitleBtn");
		fineSortTitleBtn.addEventListener("click", function() {
			displayFines(mainDiv, 'title', sortDirNext);
		});
		const fineSortTitleBtnResp = document.getElementById("fineSortTitleBtnResp");
		fineSortTitleBtnResp.addEventListener("click", function() {
			displayFines(mainDiv, 'title', sortDirNext);
		});
		const fineSortDateBtn = document.getElementById("fineSortDateBtn");
		fineSortDateBtn.addEventListener("click", function() {
			displayFines(mainDiv, 'date', sortDirNext);
		});
		const fineSortDateBtnResp = document.getElementById("fineSortDateBtnResp");
		fineSortDateBtnResp.addEventListener("click", function() {
			displayFines(mainDiv, 'date', sortDirNext);
		});
	}

	function displayHolds(mainDiv, sortBy, sortDir) {
		const jsonDiv = mainDiv + "Json";
		const tabDiv = mainDiv + "Tab";
		const itmJsonOrig = $(jsonDiv).data("hold-json");
		const itabClass = "itab itab--collapse table-hover";
        // console.log(`jsonDiv: ${jsonDiv}, sortBy: ${sortBy}, sortDir: ${sortDir}`);

		let itmJson = JSON.parse(JSON.stringify(itmJsonOrig));
		// console.log(itmJson);
		let sortDirNext = '';
		if (sortDir === 'none') {
            sortDirNext = 'asc';
        }
		if (sortDir === 'asc') {
            sortDirNext = 'desc';
        }
		if (sortDir === 'desc') {
            sortDirNext = 'asc';
        }


		if (sortBy === "author" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.author.toUpperCase();
				const iB = b.author.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "author" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.author.toUpperCase();
				const iB = b.author.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "title" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "title" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const iA = a.title.toUpperCase();
				const iB = b.title.toUpperCase();
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		if (sortBy === "hrdate" && sortDir === "asc") {
			itmJson.sort((a, b) => {
				const [dayA, monthA, yearA] = a.hold_recall_date.split('/').map(Number);
    			const [dayB, monthB, yearB] = b.hold_recall_date.split('/').map(Number);
				const iA = new Date(yearA, monthA - 1, dayA);
				const iB = new Date(yearB, monthB - 1, dayB)
				if (iA < iB) {
				  return -1;
				}
				if (iA > iB) {
				  return 1;
				}
				return 0;
			});
		}

		if (sortBy === "hrdate" && sortDir === "desc") {
			itmJson.sort((a, b) => {
				const [dayA, monthA, yearA] = a.hold_recall_date.split('/').map(Number);
    			const [dayB, monthB, yearB] = b.hold_recall_date.split('/').map(Number);
				const iA = new Date(yearA, monthA - 1, dayA);
				const iB = new Date(yearB, monthB - 1, dayB)
				if (iA < iB) {
				  return 1;
				}
				if (iA > iB) {
				  return -1;
				}
				return 0;
			});
		}

		$(tabDiv).empty();
		$(tabDiv).removeClass();

		$(tabDiv).append(`<div class='itab-row--head itab-row-header'>
			<div class='itab-cell h-cancel-cell'>
				<div class='itab-cell--heading'><i class='fa fa-check' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><input type='checkbox' name='cancel-all'></div>
			</div>
			<div class='itab-cell h-author-cell'>
				<div class='itab-cell--heading'><i class='fa fa-user' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='holdSortAuthorBtn' class='btn btn-sm btn-primary'>Author <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell h-title-cell'>
				<div class='itab-cell--heading'><i class='fa fa-book' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='holdSortTitleBtn' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell h-date-cell'>
				<div class='itab-cell--heading'><i class='fa fa-calendar' aria-hidden='true'></i></div>
				<div class='itab-cell--content'><button id='sortHRDateBtn' class='btn btn-sm btn-primary'>Date <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell h-lib-cell'>
				<div class='itab-cell--heading'><i class='fa fa-university' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Pickup Location</div>
			</div>
			<div class='itab-cell h-status-cell'>
				<div class='itab-cell--heading'><i class='fa fa-circle' aria-hidden='true'></i></div>
				<div class='itab-cell--content'>Status</div>
			</div>
		</div>`);
		
		$(tabDiv).append(`<div class='itab-row--sort itab-row-header'>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='holdSortAuthorBtnResp' class='btn btn-sm btn-primary'>Author <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='holdSortTitleBtnResp' class='btn btn-sm btn-primary'>Title <i class='fa fa-sort'></i></button></div>
			</div>
			<div class='itab-cell desc-cell-d'>
				<div class='itab-cell--content'><button id='sortHRDateBtnResp' class='btn btn-sm btn-primary'>Hold/Recall Date <i class='fa fa-sort'></i></button></div>
			</div>
		</div>`);
		
		const holdSortAuthorBtn = document.getElementById("holdSortAuthorBtn");
		holdSortAuthorBtn.addEventListener("click", function() {
			displayHolds(mainDiv, 'author', sortDirNext);
		});
		const holdSortTitleBtn = document.getElementById("holdSortTitleBtn");
		holdSortTitleBtn.addEventListener("click", function() {
			displayHolds(mainDiv, 'title', sortDirNext);
		});
		const sortHRDateBtn = document.getElementById("sortHRDateBtn");
		sortHRDateBtn.addEventListener("click", function() {
			displayHolds(mainDiv, 'hrdate', sortDirNext);
		});
		const holdSortAuthorBtnResp = document.getElementById("holdSortAuthorBtnResp");
		holdSortAuthorBtnResp.addEventListener("click", function() {
			displayHolds(mainDiv, 'author', sortDirNext, );
		});
		const holdSortTitleBtnResp = document.getElementById("holdSortTitleBtnResp");
		holdSortTitleBtnResp.addEventListener("click", function() {
			displayHolds(mainDiv, 'title', sortDirNext);
		});
		const sortHRDateBtnResp = document.getElementById("sortHRDateBtnResp");
		sortHRDateBtnResp.addEventListener("click", function() {
			displayHolds(mainDiv, 'hrdate', sortDirNext);
		});

		$('#holdItemsTab').on('click', 'tr', function(event) {
			if (event.target.type !== 'checkbox') {
				$(':checkbox[name=cancel-items]', this).trigger('click');
			}
		});

		$('#holdItemsTab').on('change', ':checkbox[name=cancel-all]', function(event) {
			if(this.checked) {
				$(':checkbox[name=cancel-items]').each(function() {
					if (!this.checked) {
						$(this).trigger('click');
					}
				});
			} else {
				$(':checkbox[name=cancel-items]').each(function() {
					if (this.checked) {
						$(this).trigger('click');
					}
				});
			}
		});

		$('#holdItemsTab').on('change', ':checkbox[name=cancel-items]', function() {
			var reqa = $('#cancelHoldItems');
			var newk = reqa.attr('data-reckey');
			var rkey = $(this).attr('data-reckey');
			if (this.checked) {
				if (newk === '') {
					reqa.attr('data-reckey', rkey)
					reqa.removeClass('disabled');
				} else {
					reqa.attr('data-reckey', newk + ',' + rkey)
					newk = newk.replace(',' + rkey, '');
				}
			} else {
				newk = newk.replace(',' + rkey, '');
				newk = newk.replace(rkey + ',', '');
				newk = newk.replace(rkey, '');
				reqa.attr('data-reckey', newk)
				if (newk === '') {
					reqa.addClass('disabled');
				}
			}
		});

		$('#cancelHoldItems').on('click', function() {
			var url = $(this).attr('data-url');
			if (url.length == 0 || url == null) {
				return;
			}
			var recKeys = $(this).attr('data-reckey');
			var data = {rec_keys: recKeys}
			// fire up the request modal popup
			$('#requestProgressModal')
				.find('.modal-title')
					.text( 'Cancel Titles' )
				.end()
				.find('.btn-close')
					.hide()
				.end()
				.modal('show');
	
			console.log(url + ' : ' + data);
	
			$.ajax( url, {
				data : data,
				type : 'POST',
				dataType : 'json',
				cache : false,
				success : function(data, txtStat, o) {
	
					$('.btn-close').show();
					$('.btn-close-refresh', '#requestProgressModal')
						.prop('disabled', '').html("Close &amp; Refresh")
						.on('click', function() {
							window.location.reload();
						});
					$('.progress', '#requestProgressModal').hide();
					for (var i = 0; i < data.responses.length; i++) {
						alert_type = data.responses[i].type == 'error' ? 'alert-danger' : 'alert-success';
						$p = $('<p class="alert"></p>').addClass(alert_type).html(data.responses[i].text);
						$('.response-message', '#requestProgressModal').append( $p );
					}
					$('.response-message').show();
					//$('#requestProgressModal').modal('hide');
					//window.location.reload();
				},
				error : function(o, txtStat, err) {
					$('#requestProgressModal').modal('hide');
				}
			});
		});

		itmJson.forEach(itm => {
			let item;
			item = $(`<div class='itab-row-cb'>
				<div class='itab-cell h-cancel-cell'>
					<input type="checkbox" name="cancel-items" data-reckey="${itm.rec_key}" />
				</div>
				<div class='itab-cell h-author-cell'>
					<div class='itab-cell--heading'><i class='fa fa-user' aria-label='Author'></i></div>
					<div class='itab-cell--content'>${itm.author}</div>
				</div>
				<div class='itab-cell h-title-cell'>
					<div class='itab-cell--heading'><i class='fa fa-book' aria-label='Title'></i></div>
					<div class='itab-cell--content'>${itm.title}</div>
				</div>
				<div class='itab-cell h-date-cell'>
					<div class='itab-cell--heading'><i class='fa fa-calendar' aria-label='Hold/Recall Date'></i></div>
					<div class='itab-cell--content'>${itm.hold_recall_date}</div>
				</div>
				<div class='itab-cell h-lib-cell'>
					<div class='itab-cell--heading'><i class='fa fa-university' aria-label='Pickup Location'></i></div>
					<div class='itab-cell--content'>${itm.sublibrary}</div>
				</div>
				<div class='itab-cell h-status-cell'>
					<div class='itab-cell--heading'><i class='fa fa-circle' aria-label='Status'></i></div>
					<div class='itab-cell--content'>${itm.hold_status}</div>
				</div>
			</div>`);
			
			$(tabDiv).addClass(itabClass);
			$(tabDiv).append(item);
		})

	}

	function getStatusHtml(itm, className) {
		let status = itm.availStatus || '';
		if (itm.availStatus == 'Access Restricted') {
			status = `<span style="color:red">${itm.availStatus}</span>`;
		}
		let statusHtml = '';
		if (status.startsWith('Available')) {
			statusHtml = `<div class='itab-cell ${className}'>
						<div class='itab-cell--heading'><span style="color:green"><i class='fa fa-circle' aria-label='Item Status' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Item Status"></i></span></div>
						<div class='itab-cell--content'>${status}</div>
					</div>`;
		} else {
			statusHtml = `<div class='itab-cell ${className}'>
						<div class='itab-cell--heading'><span style="color:red"><i class='fa fa-stop-circle' aria-label='Item Status' data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Item Status"></i></span></div>
						<div class='itab-cell--content'>${status}</div>
					</div>`;
		}
		return statusHtml;
	}

	function getRequestHtml(itm, forDetail, reqType, mmsid, title, className) {
		let requestHtml = '';
		let requestUrl = '';
		if (forDetail.userState == 'N') {
			$('div#itemDetail').append(`<a href="#" class="btn btn-sm btn-warning"><i class="fa fa-sign-in" aria-hidden="true"></i> Please Sign In</a>`);
		} else if (itm.requestType == 'none') {
			requestHtml = '&nbsp;';
		} else if (itm.requestType == 'aeon') {
			requestHtml = `<a class="btn btn-sm btn-block btn-primary" href="https://duke.aeon.atlas-sys.com/logon/?Action=10&Form=30&${itm.aeon_query}" target="_blank">Request</a>`;
		} else if (itm.requestType == 'alma-request') {
			// TODO check part_of_mmsid &partmmsid=' _ part_of_mmsid
			if(reqType == 'item') {
				requestUrl = `/request/confirm/${forDetail.sysid}?pid=${forDetail.userid}&userHomeLib=${forDetail.homeLib}&reckey=${itm.recKey}&holdid=${itm.holdId}&mmsid=${mmsid}&type=${reqType}&description=${itm.description}`;
			} else {
				requestUrl = `/request/confirm/${forDetail.sysid}?pid=${forDetail.userid}&userHomeLib=${forDetail.homeLib}&reckey=${itm.recKey}&holdid=${itm.holdId}&mmsid=${mmsid}&type=${reqType}`;
			}
			requestHtml = requestModalButton(itm, "Request", requestUrl, forDetail.illiadScanUrl, title);
		} else if (itm.requestType == 'alma-recall') {
			const show_recall = true;
			if (show_recall) {
				if (reqType == 'item') {
					requestUrl = `/request/confirm/${forDetail.sysid}?pid=${forDetail.userid}&userHomeLib=${forDetail.homeLib}&reckey=${itm.recKey}&holdid=${itm.holdId}&mmsid=${mmsid}&type=${reqType}&description=${itm.description}`;
				} else {
					requestUrl = `/request/confirm/${forDetail.sysid}?pid=${forDetail.userid}&userHomeLib=${forDetail.homeLib}&reckey=${itm.recKey}&holdid=${itm.holdId}&mmsid=${mmsid}&type=${reqType}/request/confirm/${forDetail.sysid}?pid=${forDetail.userid}&userHomeLib=${forDetail.homeLib}&reckey=${itm.recKey}&holdid=${itm.holdId}&mmsid=${mmsid}&type=${reqType}`;
				}
				requestHtml = requestModalButton(itm, "Recall", requestUrl, forDetail.illiadScanUrl, title);
			} else if (itm.scan == 'Y') {
				requestHtml = requestModalButton(itm, "", "", forDetail.illiadScanUrl, title);
			}
		} else if (itm.requestType == 'map') {
			if (itm.scan == 'Y') {
				requestHtml = requestModalButton(itm, "", "", forDetail.illiadScanUrl, title);
			} else {
				requestHtml = `<button type="button" class="btn btn-block btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#mapModal" data-sublibrary="${itm.subLibCode}" data-locationcode="${itm.collCode}" data-callNumber="${itm.mapCallNumber}" data-titlestring="${title}" data-bs-toggle="tooltip" data-placement="left" title="This item is on the shelf, but you can't request it. Click to display location guide.">Where to find it</button>`;
			}
		} else if (itm.requestType == 'login') {
			requestHtml = `<a href="/authenticate" class="btn btn-sm btn-block btn-warning">Login to Request</a>`;
		} else if (itm.requestType == 'ill') {
			if (show_ill) {
				requestHtml = `<a href="${forDetail.illiadLink}" class="btn btn-sm btn-primary">Interlibrary Loan Request</a>`;
			}
		} else {
			requestHtml = `<a href="#" class"btn btn-sm btn-warning">${itm.requestType}</a>`;
		}
		if (requestHtml == '&nbsp;') {
			requestHtml = `<div class='itab-hidden ${className}'>${requestHtml}</div>`;
		} else {
			requestHtml = `<div class='itab-cell ${className}'>${requestHtml}</div>`;
		}
		
		return requestHtml;
	}

	function requestModalButton(holding, label, requestUrl, scanUrl, title) {
		const reqBtn = `<button type="button" class="btn btn-sm btn-block btn-primary" data-available="${holding.itemAvail}" data-callNumber="${holding.callNumber}" data-locationcode="${holding.collCode}" data-request-label="${label}" data-request-type="${holding.requestType}" data-request-url="${requestUrl}" data-scan-url="${scanUrl}" data-sublibrary="${holding.subLibCode}" data-titlestring="${title}" data-bs-target="#requestModal" data-bs-toggle="modal">Request</button>`;
		return reqBtn;
	}

	function getLoanJson(cItmCnt) {
		var loanCnt = $('#loanItems').data('total-loans');
		var cItmCnt = $('#loanItems').data('ctrl-item-count');
		var renewableCnt = $('#loanItems').data('renewable-cnt');
		var loanData = $('#loanItemsJson').data('loan-json');
		if (loanData === undefined) {
			loanData = [];
        }
		$.ajax('/account/loans?cItmCnt=' + cItmCnt + '&renewableCnt=' + renewableCnt).done(function(response) {
			const resp = JSON.parse(response);
			// console.log(resp);
			const loanJson = resp.loanItems;
			if (loanJson.length > 0) {
				loanJson.forEach(obj => {
					loanData.push(obj);
				});
				let loanSize = loanData.length;
				// console.log("loanData size: " + loanSize);
				// console.log(loanData);
				$('#loanItemsJson').data("loan-json", loanData);
				$('#loanItemsJson').data("renewable-cnt", resp.renewableCnt);
				if (resp.hasMore) {
					const cItmCnt = resp.cItmCnt;
					$('#loanItems').data("ctrl-item-count", cItmCnt);
					$('#loanItemsProgress').attr("max", loanCnt).attr("value", loanSize);
					$('#loanItemsProgressMsg').text(loanSize + " of " + loanCnt);
					getLoanJson(cItmCnt);
				} else {
					$('#loanItems').replaceWith("<div id='loanItemsTab'></div>");
					displayLoans('#loanItems', 'none', 'none', 'none');
				}
			} else {
				let loanSize = 0;
				if (resp.hasMore) {
					const cItmCnt = resp.cItmCnt;
					$('#loanItems').data("ctrl-item-count", cItmCnt);
					$('#loanItemsProgress').attr("max", loanCnt).attr("value", loanSize);
					$('#loanItemsProgressMsg').text(loanSize + " of " + loanCnt);
					getLoanJson(cItmCnt);
				} else {
					$('#loanItems').empty();
				}
			}
		});
	}

	$('#triggerDiscoverSearch').on('click', function() {
		$('#requestProgressModal').modal('show');
	});

	function updateModalMap(selector) {
		return function(data, txtstatus, o) {
			var modal  = $(selector).empty();

			if (data.theme == 'area-map') {
				modal.append('<div align="center"><img class="area-map-img" src="//' + data.server_name + data.media_path + data.image + '"/>');
				modal.append('<div><span><strong>Building:</strong>&nbsp;&nbsp;' + data.building + '</span></div>');
				modal.append('<div><span><strong>Area/Floor:</strong>&nbsp;&nbsp;' + data.area + '</span></div></div>');
				if (data.sublibrary == 'LAW') {
					modal.find('.modal-dialog').addClass('modal-lg');
				}

			} else if (data.theme == 'external-link') {
				modal.append('<div><h2>' + data.extlink_title + '</h2>');
				modal.append('<p>' + data.generic_msg_content + '</p>');
			}
		}
	}

	$('#requestModal').on('show.bs.modal', function(evt) {
		var button = $(evt.relatedTarget);

		var titlestring = button.data('titlestring');
		var title = decodeURIComponent(titlestring);
		var callno = button.data('callnumber');
		if (callno.length > 0) {
			title += ' // ' + callno;
		}
		$('#requestModal').find('.modal-title').text(title);

		var requestType = button.data('request-type');

		if (requestType == 'map') {
			$('#request-onsite').removeClass('hidden');
			$('#request-onsite-map').removeClass('hidden');
			$('#request-onsite-takeout').addClass('hidden');

			var collectioncode = button.data('locationcode');
			var library = button.data('sublibrary');

			$.ajax({
				//url					: '//locationguide-stage.lib.duke.edu/api/mapinfo',
                url                 : '//library.duke.edu/mapinfo',
				data				: {
					collection_code	: collectioncode,
					callno					: callno,
					sublibrary			: library,
					titlestring			: titlestring,
					ajax						: 'yes'
				},
				type 				: 'POST',
				dataType        : 'json',
				cache 			: false,
				success 		: updateModalMap('#request-onsite-map'),
				error : function(o, txtstatus, err) {}
			});
		} else if (button.data('request-url')) {
			$('#request-onsite').removeClass('hidden');
			$('#request-onsite-takeout').removeClass('hidden');
			$('#request-onsite-map').addClass('hidden');

			$('#request-onsite-takeout-btn').attr('href', button.data('request-url'))
			$('#request-onsite-takeout-btn').text(button.data('request-label'));
		} else {
			$('#request-onsite').addClass('hidden');
		}

		var scanUrl = button.data('scan-url');
		if (scanUrl) {
			$('#request-scan').removeClass('hidden');
			$('#request-scan-btn').attr('href', scanUrl);

			if (button.data('available') === 'Y') {
				$('#request-scan-instructions-avail').removeClass('hidden');
				$('#request-scan-instructions-unavail').addClass('hidden');
			} else {
				$('#request-scan-instructions-avail').addClass('hidden');
				$('#request-scan-instructions-unavail').removeClass('hidden');
			}
		} else {
			$('#request-scan').addClass('hidden');
		}
	});

	// button for location map
	// <a href="" class="item-location-info">Stacks</a>
	$('#mapModal').on('show.bs.modal', function(evt) {
		var button = $(evt.relatedTarget);

		var titlestring = button.data('titlestring');
		var callno = button.data('callnumber');

		var title = decodeURIComponent(titlestring);
		if (callno.length > 0) {
			title += ' // ' + callno;
		}

		$('#mapModal').find('.modal-title').text(title);

		$.ajax({
			//url					: '//locationguide-stage.lib.duke.edu/api/mapinfo',
            url                 : '//library.duke.edu/mapinfo',
			data				: {
				callno					: callno,
				collection_code	: button.data('locationcode'),
				sublibrary			: button.data('sublibrary'),
				titlestring			: titlestring,
				ajax						: 'yes'
			},
			type 				: 'POST',
            dataType        : 'json',
			cache 			: false,
			success 		: updateModalMap('#mapModal .modal-body'),
			error : function(o, txtstatus, err) {
			}
		});
	});

	$('#holdItems').on('change', ':checkbox[name=cancel-hold-item]', function() {
		var reqa = $('#cancelHoldItems');
		var newk = reqa.attr('data-reckey');
		var rkey = $(this).attr('data-reckey');
		if (this.checked) {
			if (newk === '') {
				reqa.attr('data-reckey', rkey)
				reqa.removeClass('disabled');
			} else {
				reqa.attr('data-reckey', newk + ',' + rkey)
				newk = newk.replace(',' + rkey, '');
			}
		} else {
			newk = newk.replace(',' + rkey, '');
			newk = newk.replace(rkey + ',', '');
			newk = newk.replace(rkey, '');
			reqa.attr('data-reckey', newk)
			if (newk === '') {
				reqa.addClass('disabled');
			}
		}
	});

	$('#holdItems').on('click', '#cancelHoldItems', function() {
		var url = $(this).attr('data-url');
		if (url.length == 0 || url == null) {
			return;
		}
		var recKeys = $(this).attr('data-reckey');
		var data = {rec_keys: recKeys}

		// fire up the request modal popup
		$('#requestProgressModal')
			.find('.modal-title')
				.text( 'Cancel Request' )
			.end()
			.find('.btn-close')
				.hide()
			.end()
			.modal('show');

		console.log(url + ' : ' + data);

		$.ajax( url, {
			data : data,
			type : 'POST',
			dataType : 'json',
			cache : false,
			success : function(data, txtStat, o) {

				$('.btn-close').show();
				$('.btn-close-refresh', '#requestProgressModal')
					.prop('disabled', '').html("Close &amp; Refresh")
					.on('click', function() {
						window.location.reload();
					});
				$('.progress', '#requestProgressModal').hide();
				for (var i = 0; i < data.responses.length; i++) {
					alert_type = data.responses[i].type == 'error' ? 'alert-danger' : 'alert-success';
					$p = $('<p class="alert"></p>').addClass(alert_type).html(data.responses[i].text);
					$('.response-message', '#requestProgressModal').append( $p );
				}
				$('.response-message').show();
				//$('#requestProgressModal').modal('hide');
				//window.location.reload();
			},
			error : function(o, txtStat, err) {
				$('#requestProgressModal').modal('hide');
			}
		});
	});

})(jQuery);

