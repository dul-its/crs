$(function() {

	/**
	 * REQUEST ('Discover') BUTTON
	 */
	$('.btn-confirm-request').on('click', function(evt) {
		// capture the URL
		var request_url = $(this).attr('href');
		evt.preventDefault();

		// now verify that the ALEPH REST(ful) API is available
		$.ajax( '/verify-api', {
			cache : false,
			type : 'GET',
			dataType : 'json',
			success : function(data, txtStat, o) {
				if (data['reply-text'] == 'OK' ) {
					$('#requestProgressModal')
						.find('.modal-title')
							.text('Preparing Request')
							.end()
						.modal('show');
					window.location.assign( request_url );
				} else {
					if ($('#apiErrorModal').length > 0 ) {
						$('#apiErrorModal').modal('show');
					} else {
						alert( "Sorry, the system is unable to reach the layer that handles the request." );
					}
				}
			},
			error : function(o, txtStat, err) {
				// assume 500-ish error
				if ($('#apiErrorModal').length > 0 ) {
					$('#apiErrorModal').modal('show');
				} else {
					alert( "Sorry, the system is unable to reach the layer that handles the request." );
				}
			}
		});
		return false;
	});

	$('[type="submit"]', '#titleSearchForm').on('click', function(evt) {
		// display progress bar modal
		if ($('#searchTermText').val() == '') {
			$('#searchTermText')
				.parent('.form-group')
					.addClass('has-error')
					.end()
				.on('shown.bs.popover', function() {
					var me = this;
					var myWait = setTimeout(function() {
						$(me)
							.popover('hide');

						$(me).parent().removeClass('has-error');
					}, 3000);
				})
				.popover('show');
			return evt.preventDefault();
		}
	});

	$('#modalRequest').
		on('show.bs.modal', function(e) {
			var relTarget = e.relatedTarget;
			var $modal = $(this);
			if (!$(relTarget)[0].hasAttribute('data-barcode')) {
				// bail out because we don't have the info
				return evt.preventDefault();
			}
			var sysId = $(relTarget).attr('data-barcode');

			$('.info-alert', this).hide();
			$('.progress', this).hide();
			$('.confirm-alert', this).show();
			$('.confirm-alert > .confirm-yes').on('click', function(e) {
				debugger;
				
				$('.confirm-alert', this).show();

				// the meat of the request is done here..
				// TODO: explore creating a function for this.
				var requestData = { sysid : sysId }
				$.ajax('/request/', {
					type : 'POST',
					data : requestData,
					cache : false,
					dataType : 'json',
					success : function (data, txtStatus, o) {
						var e = 'Hello E';
						$('.confirm-alert', $modal).hide();
						$('.progress', $modal).hide();
						$('.info-alert', $modal)
							.find('.system-message')
								.text(data.system_message)
								.end()
							.show();
					},
					error : function(o, txtStatus, err) {
						var e = 'hello error';
						debugger;
					}
				})
			});
		})
		.on('shown.bs.modal', function(e) {
			// ajax call to place hold request.
			// note -- user info should reside in the backend session
			//
			// ON SUCCESS:
			// - hide the progress bar
			// - transfer data from ajax request to info-alert view
			// - show info-alert view
			/**
			 * SEE 'confirm-alert' section above
			var barcode = $(e.relatedTarget).attr('data-barcode');
			if (barcode.length == 0) {
				alert("Unable to determine the barcode for this item.");
				return evt.preventDefault();
			}
			$.ajax('/request/', {
				type : 'POST',
				data : { sysid: barcode },
				cache : false,
				error : function(o, txtstat, err) {
					var d = 'hi';
				},
				success : function(data, txtstat, o) {
					var e = 'hi';
				}
			})
			*/
		});

	$('#apiErrorModal').modal({
		backdrop: '',
		keyboard: false,
		show: false
	});

	$('#requestProgressModal').modal({
		backdrop: '',
		keyboard: false,
		show: false
	})
	.on('show.bs.modal', function(e) {
		var relTarget = e.relatedTarget;
		if ($('.modal-requirement').val() == '') {
			return false;
		}
		$('.btn-close').hide();
	});

	/* Show a modal popup while the "Request" request is in progress... */
	$('button[data-toggle="request"]').on('click', function(evt) {
		$('#requestProgressModal').modal('show');
		myWait = setTimeout(function() {
			$('#requestProgressModal').modal('hide');
		}, 5000);
		
		// return evt.preventDefault();
	});

	$('#pickupLocation').change(function() {
		$('#pickup-label').removeClass('validate-warning');
	});
});

function validateRequest() {
	let pickupLocation = $('#pickupLocation option:selected').text();
	// console.log('validateRequest pickupLocation: ' + pickupLocation);
	if (pickupLocation == '--- SELECT PICKUP/DELIVERY LOCATION ---') {
		$('#pickup-label').addClass('validate-warning');
		alert('Please choose a pickup/delivery location.');
		return false;
	}
	return true;
}