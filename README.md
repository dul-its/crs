# CRS = Catalog Request System

## Web URLs

**Production**: https://requests.library.duke.edu

**Development**: https://circulation-sandbox.lib.duke.edu

## Purpose
This project represents the development of a web application that will replace the Libraries' current catalog request system.

Where as the legacy application was built using the Django (Python) framework, this new application will be built on an emerging new framework -- Dancer2 (a PERL framework).

## Deployment Strategy
GitLab CI/CD is used to facilitate auto-deployment for the web application.  

Commits to the "main" branch are automatically deployed to `requests.library.duke.edu`.  
Commits to the "sandbox" branch are automatically deployed to `circulation-sandbox.lib.duke.edu`.  
Commits to any `ui-*` prefixed feature branch are deployed to `auxs-requests.lib.duke.edu`.  

## Confluence Documentation
For detailed documentation, visit:  
[Catalog Request System (CRS) - Developer Notes](https://duldev.atlassian.net/wiki/spaces/DOC/pages/3890511874/Catalog+Request+System+CRS+-+Developer+Notes)  

